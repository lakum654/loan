<?php

use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\LoginController as AdminLoginController;
use App\Http\Controllers\Admin\UsersController as AppUsersController;
use App\Http\Controllers\Admin\LoanApplicationController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\Admin\DocumentController as Document;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('login');

Route::get('career', function () {
        return view('career');
});

Route::any('catch/webhook', [PaymentController::class, 'catchWebhook']);

Route::post('sendOtp', [LoginController::class, 'sendOtp'])->name('login.sendOtp');
Route::post('verifyOtp', [LoginController::class, 'verifyOtp'])->name('login.verifyOtp');

Route::view('company','company');
Route::view('premium-membership-card','premium-membership-card');
Route::view('platinum-membership-card','platinum-membership-card');
Route::view('channel-partner-code','channel-partner-code');
Route::view('membership-card-benefits','membership-card-benefits');
Route::view('business-loan','businessLoan');
Route::view('personal-loan','personalLoan');
Route::view('channel-partner','channel-partner');
Route::view('partner/channel','partner.channel');
Route::view('channel-login','channel-login');
Route::view('faq','faqs');
Route::view('contactus','contactus');
Route::view('important-update','important-update');
Route::view('sitemap','sitemap');
Route::view('privacy-policy','privacy-policy');
Route::view('refund-policy','refund-policy');
Route::view('disclaimer','disclaimer');
Route::view('terms-conditions','terms-conditions');
Route::view('career/form-1','career.form-1');
Route::view('career/form-2','career.form-2');
Route::view('career/form-3','career.form-3');

Route::get('customer-login',[AuthController::class,'index']);
Route::view('forgotpassword','auth.forgotpassword');

Route::view('digital/personal-loan','digital.personalLoan')->middleware('logged_in_check:process.loan.continue');
// Route::view('digital/otp-verify','digital.otp-verify')->name('otp.verify.scr')->middleware('logged_in_check:process.loan.continue');
Route::view('digital/business-loan','digital.businessLoan');

Route::group(['middleware' => 'auth'], function() {
    Route::group(['middleware' => 'payment_check'], function() {
        Route::view('digital/loan-amount','digital.loan-amount')->name('process.loan.continue')->middleware('logged_in_check:process.loan.checkeligibility');
        Route::post('digital/loan-amount', [LoginController::class, 'emailLoanAmountSave'])->name('process.loan.continue.post');
        Route::view('digital/checkeligibility','digital.checkeligibility')->name('process.loan.checkeligibility')->middleware('logged_in_check:email.loan.amt.custom.check');
        Route::view('digital/preapproval','digital.preapproval')->middleware('logged_in_check:email.loan.amt.custom.check');
        Route::view('digital/membershiporder','digital.membershiporder')->middleware('logged_in_check:email.loan.amt.custom.check');
        Route::post('payment/success', [PaymentController::class, 'paymentSuccess'])->name('loan.payment.success');
    });
    Route::view('digital/document-upload','digital.documentupload')->name('loan.step.document.upload')->middleware('logged_in_check:senction.custom.check');
    Route::post('digital/document-submit',[DocumentController::class,'store'])->name('loan.step.document.submit');
    Route::view('digital/senction-letter','digital.senctionletter')->name('loan.step.senction.letter');/* ->middleware('logged_in_check:final.not.remain.check') */;
});
/**  Topics */
Route::view('personal-loan-for-self-employed','topics.personal-loan-for-self-employed');
Route::view('personal-loan-for-cibil-defaulters', 'topics.personal-loan-for-cibil-defaulters');
Route::view('personal-loan-balance-transfer', 'topics.personal-loan-balance-transfer');
Route::view('personal-loan-without-salary-slip', 'topics.personal-loan-without-salary-slip');
Route::view('personal-loan-private-finance', 'topics.personal-loan-private-finance');
Route::view('personal-loan-for-nri', 'topics.personal-loan-for-nri');
Route::view('pre-approved-personal-loan', 'topics.pre-approved-personal-loan');
Route::view('documents-required-for-personal-loan', 'topics.documents-required-for-personal-loan');
Route::view('required-cibil-score-for-personal-loan', 'topics.required-cibil-score-for-personal-loan');
Route::view('top-up-personal-loan', 'topics.top-up-personal-loan');
Route::view('business-loan-eligibility', 'topics.business-loan-eligibility');
Route::view('loan-agency-in-india', 'topics.loan-agency-in-india');
Route::view('long-term-loans', 'topics.long-term-loans');

/* Personal Loan by City */
Route::view('personal-loan-in-delhi-ncr', 'personal_loan_city.personal-loan-in-delhi-ncr');
Route::view('personal-loan-in-bangalore', 'personal_loan_city.personal-loan-in-bangalore');
Route::view('personal-loan-in-kerala', 'personal_loan_city.personal-loan-in-kerala');
Route::view('online-personal-loan-mumbai', 'personal_loan_city.online-personal-loan-mumbai');
Route::view('personal-loan-in-pune', 'personal_loan_city.personal-loan-in-pune');
Route::view('personal-loan-in-coimbatore', 'personal_loan_city.personal-loan-in-coimbatore');
Route::view('personal-loan-in-kolkata', 'personal_loan_city.personal-loan-in-kolkata');
Route::view('personal-loan-in-jaipur', 'personal_loan_city.personal-loan-in-jaipur');

/* Business Loan by City  */
Route::view('business-loan-in-delhi-ncr', 'business_loan_city.business-loan-in-delhi-ncr');
Route::view('business-loan-in-bangalore', 'business_loan_city.business-loan-in-bangalore');
Route::view('business-loan-in-kerala', 'business_loan_city.business-loan-in-kerala');
Route::view('business-loan-in-bihar', 'business_loan_city.business-loan-in-bihar');

/** Admin Routes */
Route::group(["prefix" => "admin"], function () {
    Route::get('login', [AdminLoginController::class, 'index'])->name('admin.login');
    Route::post('login', [AdminLoginController::class, 'login_check'])->name('admin.login.check');
    Route::group(["middleware" => "is_admin"], function () {
        Route::get('/', [AdminHomeController::class, 'index'])->name('admin.dashboard');
        Route::post('logout', [AdminLoginController::class, 'logout'])->name('admin.logout');

        /** Registered Users Routes */
        Route::get('users',[AppUsersController::class,'index'])->name('users');
        Route::get('users/getUserData',[AppUsersController::class,'getUsers'])->name('users.getUserData');
        Route::get('users/delete',[AppUsersController::class,'index'])->name('users.delete');

        /** User Loan Application Routes */
        Route::get('applications',[LoanApplicationController::class,'index'])->name('applications');
        Route::get('applications/getData',[LoanApplicationController::class,'getData'])->name('applications.getData');

        /* Document Route */
        Route::get('document/{id}',[Document::class,'index'])->name('index.document');
    });
});
