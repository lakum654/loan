<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            "email" => "admin@gmail.com"
        ], [
            "name" => "Super Admin",
            "email" => "admin@gmail.com",
            "mobile" => "0000000000",
            "password" => Hash::make('123456'),
            "is_admin" => 1
        ]);
    }
}
