<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_applications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("user_id")->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string("razorpay_payment_id");
            $table->string("cibil_score")->nullable();
            $table->string("monthly_income")->nullable();
            $table->string("current_monthly_emi")->nullable();
            $table->string("loan_purpose")->nullable();
            $table->string("city")->nullable();
            $table->string("state")->nullable();
            $table->string("eligibility_amount")->nullable();
            $table->string("pre_approval_offer")->nullable();
            $table->string("amount_paid")->nullable();
            $table->string("payment_status")->default('pending')->comment("pending, success, failed");
            $table->text("full_hook")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applications');
    }
}
