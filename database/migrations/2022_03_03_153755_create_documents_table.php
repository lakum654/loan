<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("user_id")->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('profile_photo')->nullable();
            $table->text('aadhar_card')->nullable();
            $table->text('pan_card')->nullable();
            $table->text('aadhar_proof')->nullable();
            $table->text('cancel_cheque')->nullable();
            $table->text('address_proof')->nullable()->comment('light_bill');
            $table->text('form_16')->nullable();
            $table->text('bank_statement')->nullable()->comment('last 6 monts');
            $table->text('salary_slip')->nullable();
            $table->longText('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
