<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanApplication extends Model
{
    use HasFactory;

    protected $table = "loan_applications";
    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id')->withDefault(['name' => '-']);
    }
}
