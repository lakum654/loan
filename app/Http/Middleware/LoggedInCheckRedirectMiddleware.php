<?php

namespace App\Http\Middleware;

use App\Models\Document;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoggedInCheckRedirectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $route)
    {
        if(Auth::check()) {
            if($route == 'process.loan.checkeligibility') {
                if(!empty(auth()->user()->email) && !empty(auth()->user()->loan_amount)) {
                    return redirect()->route($route);
                } else {
                    return $next($request);
                }
            } elseif($route == 'email.loan.amt.custom.check') {
                if(empty(auth()->user()->email) || empty(auth()->user()->loan_amount)) {
                    return redirect()->route('process.loan.continue');
                } else {
                    return $next($request);
                }
            } elseif($route == 'senction.custom.check') {
                $doccheck = Document::where('user_id', auth()->user()->id)->first();
                if(!empty($doccheck)) {
                    return redirect()->route('loan.step.senction.letter');
                } else {
                    return $next($request);
                }
            } else {
                return redirect()->route($route);
            }
        } else {
            return $next($request);
        }

    }
}
