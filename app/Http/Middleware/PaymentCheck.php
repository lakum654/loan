<?php

namespace App\Http\Middleware;

use App\Models\LoanApplication;
use Closure;
use Illuminate\Http\Request;

class PaymentCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $checkLoanApp = LoanApplication::where('user_id', auth()->user()->id)->first();
        if(!empty($checkLoanApp)) {
            return redirect()->route('loan.step.document.upload');
        }
        return $next($request);
    }
}
