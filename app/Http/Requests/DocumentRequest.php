<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_photo' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'aadhar_card' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'pan_card' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'aadhar_proof' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'cancel_cheque' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'address_proof' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'form_16' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'bank_statement' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
            'salary_slip' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
        ];
    }

    public function messages()
    {
        return[
            'profile_photo.required' => 'Profile Photo Is Required.',
            'aadhar_card.required' => 'Aadhar Card Photo Is Required.',
            'pan_card.required' => 'Pan Card Photo Is Required.',
            'aadhar_proof.required' => 'Aadhar Proof Photo Is Required.',
            'cancel_cheque.required' => 'Cancel Cheque Photo Is Required.',
            'address_proof.required' => 'Address Proof Is Required.',
            'form_16.required' => 'Form 14 Is Required.',
            'bank_statement.required' => 'Bank Statement Is Required.',
            'salary_slip.required' => 'Salary Slip Is Required.',
            'remarks.required' => 'Remarks Is Required.',
        ];
    }
}
