<?php

namespace App\Http\Controllers;

use App\Models\LoanApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function paymentSuccess(Request $request)
    {

        $this->validate($request, [
            "paymentid" => "required"
        ]);

        $step2Data = (isset($request->step2) && !empty($request->step2)) ? json_decode($request->step2, true) : [];

        LoanApplication::updateOrCreate([
            "razorpay_payment_id" => $request->paymentid
        ], [
            "user_id" => auth()->user()->id,
            "razorpay_payment_id" => $request->paymentid,
            "cibil_score" => $step2Data['cibilscore'] ?? '',
            "monthly_income" => $step2Data['monthlyincome'] ?? '',
            "current_monthly_emi" => $step2Data['monthlyemi'] ?? '',
            "loan_purpose" => $step2Data['loanpurpose'] ?? '',
            "city" => $step2Data['city'] ?? '',
            "state" => $step2Data['state'] ?? '',
            "eligibility_amount" => $request->eligible_amount ?? '',
            "pre_approval_offer" => $request->step3 ?? '',
            "amount_paid" => $request->orderamount + ($request->orderamount * 18) / 100 ?? ''
        ]);

        return redirect()->route('loan.step.document.upload');
    }

    public function catchWebhook()
    {
        $post = file_get_contents('php://input');
        $data = json_decode($post, true);

        if(isset($data["event"]) && $data["event"] == "payment.authorized") {
            if(isset($data["payload"]) && isset($data["payload"]["payment"]) && isset($data["payload"]["payment"]["entity"]) && isset($data["payload"]["payment"]["entity"]["status"]) && $data["payload"]["payment"]["entity"]["status"] == "authorized") {
                LoanApplication::updateOrCreate([
                    "razorpay_payment_id" => $data["payload"]["payment"]["entity"]["id"]
                ], [
                    "razorpay_payment_id" => $data["payload"]["payment"]["entity"]["id"],
                    "payment_status" => 'success',
                    "full_hook" => json_encode($data)
                ]);
            }
        } elseif(isset($data["event"]) && $data["event"] == "payment.failed") {
            if(isset($data["payload"]) && isset($data["payload"]["payment"]) && isset($data["payload"]["payment"]["entity"]) && isset($data["payload"]["payment"]["entity"]["id"])) {
                LoanApplication::updateOrCreate([
                    "razorpay_payment_id" => $data["payload"]["payment"]["entity"]["id"]
                ], [
                    "razorpay_payment_id" => $data["payload"]["payment"]["entity"]["id"],
                    "payment_status" => 'failed',
                    "full_hook" => json_encode($data)
                ]);
            }
        }
    }
}
