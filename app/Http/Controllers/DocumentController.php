<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentRequest;
use Illuminate\Http\Request;
use App\Models\Document;

class DocumentController extends Controller
{
    public function store(Request $request){

        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 0);
        $authId = auth()->user()->id;
        $path = storage_path('userdocuments');

        if($request->has('profile_photo')){
            $profile_photo = mt_rand(111119,999999).'_profile_'.$authId.'.'.$request->profile_photo->extension();
            $request->profile_photo->move($path, $profile_photo);
            $profile_photo = url('storage/userdocuments/'.$profile_photo);
        }

        if($request->has('aadhar_card')){
            $aadhar_card = mt_rand(111119,999999).'_aadhar_card_'.$authId.'.'.$request->aadhar_card->extension();
            $request->aadhar_card->move($path, $aadhar_card);
            $aadhar_card = url('storage/userdocuments/'.$aadhar_card);
        }


        if($request->has('pan_card')){
            $pan_card = mt_rand(111119,999999).'_pan_card_'.$authId.'.'.$request->pan_card->extension();
            $request->pan_card->move($path, $pan_card);
            $pan_card = url('storage/userdocuments/'.$pan_card);
        }

        if($request->has('aadhar_proof')){
            $aadhar_proof = mt_rand(111119,999999).'_aadhar_proof_'.$authId.'.'.$request->aadhar_proof->extension();
            $request->aadhar_proof->move($path, $aadhar_proof);
            $aadhar_proof = url('storage/userdocuments/'.$aadhar_proof);
        }


        if($request->has('cancel_cheque')){
            $cancel_cheque = mt_rand(111119,999999).'_cancel_cheque_'.$authId.'.'.$request->cancel_cheque->extension();
            $request->cancel_cheque->move($path, $cancel_cheque);
            $cancel_cheque = url('storage/userdocuments/'.$cancel_cheque);
        }


        if($request->has('address_proof')){
            $address_proof = mt_rand(111119,999999).'_address_proof_'.$authId.'.'.$request->address_proof->extension();
            $request->address_proof->move($path, $address_proof);
            $address_proof = url('storage/userdocuments/'.$address_proof);
        }

        if($request->has('form_16')){
            $form_16 = mt_rand(111119,999999).'_form_16_'.$authId.'.'.$request->form_16->extension();
            $request->form_16->move($path, $form_16);
            $form_16 = url('storage/userdocuments/'.$form_16);
        }


        if($request->has('bank_statement')){
            $bank_statement = mt_rand(111119,999999).'_bank_statement_'.$authId.'.'.$request->bank_statement->extension();
            $request->bank_statement->move($path, $bank_statement);
            $bank_statement = url('storage/userdocuments/'.$bank_statement);
        }

        if($request->has('salary_slip')){
            $salary_slip = mt_rand(111119,999999).'_salary_slip_'.$authId.'.'.$request->salary_slip->extension();
            $request->salary_slip->move($path, $salary_slip);
            $salary_slip = url('storage/userdocuments/'.$salary_slip);
        }


        Document::updateOrCreate(['user_id' => $authId],['user_id' => $authId,'profile_photo' => ($profile_photo) ?? null,'aadhar_card' => ($aadhar_card) ?? null,'pan_card' => ($pan_card) ?? null,'cancel_cheque' => ($cancel_cheque) ?? null,'address_proof' => ($address_proof) ?? null,'form_16' => ($form_16) ?? null,'bank_statement' => ($bank_statement) ?? null,'salary_slip' => ($salary_slip) ?? null,'remarks' => ($request->remarks) ?? null]);
        return redirect()->route('loan.step.senction.letter');
    }
}
