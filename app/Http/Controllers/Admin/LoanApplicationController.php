<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LoanApplication;
use DataTables;

class LoanApplicationController extends Controller
{
    public $moduleName = "Loan Applications";
    public $route = 'applications';
    public $view = 'admin.admin.loan_application';

    public function index() {
        $moduleName = $this->moduleName;
        return view($this->view.'.index',compact('moduleName'));
    }

    public function getData() {
        $applications = LoanApplication::with('user');

        return DataTables()->eloquent($applications)
        ->editColumn('current_monthly_emi',function($row){
            return number_format($row->current_monthly_emi,2);
        })

        ->editColumn('monthly_income',function($row){
            return number_format($row->monthly_income,2);
        })

        ->editColumn('amount_paid',function($row){
            return number_format($row->amount_paid,2);
        })
        ->editColumn('action',function($row){
            $url = route('index.document',encrypt($row->user_id));
            return "<a href='$url' class='edit btn btn-primary btn-xs'>View</a>";
        })
        ->addIndexColumn()
        ->make(true);

    }
}
