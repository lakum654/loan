<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        if(auth()->check()) {
            return redirect()->route('admin.dashboard');
        } else {
            return view('admin.login');
        }
    }

    public function login_check(Request $request)
    {
        if(auth()->check()) {
            return redirect('admin.dashboard');
        } else {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $credentials = $request->only('email', 'password');

            if (Auth::attempt($credentials)) {
                if(auth()->user()->is_admin == 1) {
                    return redirect()->route('admin.dashboard');
                } else {
                    Auth::logout();
                    return redirect()->route('admin.login')->with('error', 'Invalid Email address or Password');
                }
            } else {
                return redirect()->route('admin.login')->with('error', 'Invalid Email address or Password');
            }
        }
    }

    public function logout()
    {
        if(auth()->check()) {
            Auth::logout();
            return redirect()->route('admin.login');
        } else {
            return redirect()->route('admin.login');
        }
    }
}
