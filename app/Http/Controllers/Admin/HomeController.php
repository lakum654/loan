<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LoanApplication;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $user = User::where('is_admin',0)->count();
        $todayUser = User::whereDate('created_at', date('Y-m-d'))->where('is_admin',0)->count();
        $successPay = LoanApplication::where('payment_status','success')->get()->pluck('user_id')->toArray();
        $pendingPay = User::where('is_admin',0)->whereNotIn('id',$successPay)->count();
        return view('admin.admin.home',compact('user','todayUser','successPay','pendingPay'));
    }
}
