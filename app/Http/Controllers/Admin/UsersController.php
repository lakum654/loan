<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use DataTables;

class UsersController extends Controller
{
    public $moduleName = "App Users";
    public $route = 'users';
    public $view = 'admin.admin.users';

    public function index() {
        $moduleName = $this->moduleName;
        return view($this->view.'.index',compact('moduleName'));
    }

    public function getUsers(Request $request) {

        $users = User::where('is_admin',0);
        if($request->fromdate != null && $request->todate != null){
           $users->whereDate('created_at', '>=', $request->fromdate)->whereDate('created_at', '<=', $request->todate);
        }

        return DataTables()->eloquent($users)
        ->editColumn('loan_amount',function($row){
            return number_format($row->loan_amount,2);
        })

        ->editColumn('created_at',function($row){
            return date('d-m-Y h:i:s A',strtotime($row->created_at));
        })
        ->addIndexColumn()
        ->make(true);

    }
}
