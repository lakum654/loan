<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    private $moduleName = "User Document";

    public function index($id)
    {
        $id = decrypt($id);
        $moduleName = $this->moduleName;
        $document = Document::where('user_id',$id)->first();
        return view('admin.admin.document.index',compact('moduleName','id','document'));
    }
}
