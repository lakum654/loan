<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Otp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function sendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'mobile' => 'required|numeric|digits:10'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => "Something went wrong while varifying details try again."], 200);
        }

        $otp = mt_rand(111111, 999999);
        $mobile_no = $request->mobile;

        Otp::updateOrCreate([
            'mobile_no' => $mobile_no
        ], [
            'mobile_no' => $mobile_no,
            'otp' => '123456'
        ]);

        // return response()->json(["success" => true, "message" => "Otp has been sent to your registered mobile number.", "redirect_url" => route('otp.verify.scr')], 200);
        return response()->json(["success" => true, "message" => "Otp has been sent to your registered mobile number.", "redirect_url" => route('process.loan.continue')], 200);
    }

    // public function verifyOtp(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'fullname' => 'required',
    //         'mobile' => 'required|numeric|digits:10',
    //         'otp' => 'required|numeric|digits:6'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(["success" => false, "message" => "Something went wrong while varifying details try again."], 200);
    //     }

    //     $otpdata = Otp::where(["mobile_no" => $request->mobile, "otp" => $request->otp])->first();
    //     if(!empty($otpdata)) {
    //         $user = User::updateOrCreate([
    //             "mobile" => $request->mobile
    //         ],[
    //             "name" => $request->fullname,
    //             "mobile" => $request->mobile
    //         ]);

    //         Auth::loginUsingId($user->id);
    //         Otp::where(["mobile_no" => $request->mobile, "otp" => $request->otp])->delete();

    //         return response()->json(["success" => true, "message" => "Logged into the system successfully.", "redirect_url" => route('process.loan.continue')], 200);
    //     } else {
    //         return response()->json(["success" => false, "message" => "Otp is wrong, Please try again."], 200);
    //     }
    // }

    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'mobile' => 'required|numeric|digits:10'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => "Something went wrong while varifying details try again."], 200);
        }

        $user = User::updateOrCreate([
            "mobile" => $request->mobile
        ],[
            "name" => $request->fullname,
            "mobile" => $request->mobile
        ]);

        Auth::loginUsingId($user->id);
        return response()->json(["success" => true, "message" => "Logged into the system successfully.", "redirect_url" => route('process.loan.continue')], 200);
    }

    public function emailLoanAmountSave(Request $request)
    {
        $this->validate($request, [
            'usertype' => 'required|in:0,1',
            'useremail' => 'required|email',
            'loanamount' => 'required|numeric'
        ]);

        User::where(["id" => auth()->user()->id])->update([
            "person" => $request->usertype,
            "email" => $request->useremail,
            "loan_amount" => $request->loanamount
        ]);

        return redirect()->back();
    }
}
