<html class="loading" lang="en" data-textdirection="ltr">
<title>Get online personal loan approval in 30 minutes | moneyupfinance </title>
@include('layouts.headerscript')
  <body class="breakpoint-xl b--desktop">
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
      <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
        <div class="header-inner">
          <div class="container">

            <div id="logo">
              <a href="{{ url('/') }}">
                <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
                <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
              </a>
            </div>

            <div class="header-extras">
              <div class="p-dropdown">
                  <a class="x"><span class="lines"></span></a>
                  <ul class="p-dropdown-content">
                      <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a></li>
                      <li><a href="mailto:info@moneyupfinance.com"><i class="icon-mail"></i>info@moneyupfinance.com</a></li>
                  </ul>
              </div>
            </div>

          </div>
        </div>
      </header>
  <section class="background-theme bg-loaded" data-bg-image="https://nowofloan.com/assets/images/slider/digital-process-bg.jpg" style="background-image: url(&quot;https://nowofloan.com/assets/images/slider/digital-process-bg.jpg&quot;);">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 pb-4 text-center">
                  <h2 class="text-light">Digital Personal Loan Application Process</h2>
                  <p class="text-light m-b-30">Get offer for instant disbursement</p>
              </div>
          </div>
      </div>
  </section>

  <section class="reservation-form-over no-padding">
      <div class="container">
          <div class="row">
              <div class="col-lg-3 col-md-3 col-12 sm-hidden">
                  <div class="p-cb">
                      <div class="wizard clearfix" data-style="1" data-orientation="vertical" role="application">
                          <div class="steps clearfix m-0 p-r-0">
                              <ul role="tablist">
                                  <li role="tab" class="current"><a href="#"><span class="number">1</span><span class="title">Quick Registration</span></a></li>

                                  <li role="tab" class="current"><a href="#"><span class="number">2</span><span class="title">Check Eligibility</span></a></li>

                                  <li role="tab" class="current"><a href="#"><span class="number">3</span><span class="title">Get Pre-Approval Offer</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">4</span><span class="title">Buy Membership Card</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">5</span><span class="title">Submit Documents</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">6</span><span class="title">Get Sanctioned</span></a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

              <!-- START : GET PRE-APPROVAL -->
              <form action="#" id="submitForm2" class="col-lg-9 col-md-9 col-12" novalidate="novalidate" method="post" accept-charset="utf-8">
                  <div class="row p-cb">
                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="fullname"><strong>Name :</strong> <span id="username">{{ auth()->user()->name }}</span></h5>
                      </div>

                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="mobile"><strong>Mobile no. :</strong> <span id="mobile">{{ auth()->user()->mobile }}</span></h5>
                      </div>

                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="loanamount"><strong>Loan Amount :</strong> <span id="amount">{{ number_format(auth()->user()->loan_amount,2) }}</span></h5>
                          <input type="hidden" value="{{ auth()->user()->loan_amount }}" id="loan_amount">
                      </div>

                      <div class="line m-10 p-b-20"></div>

                      <?php
                            $final_amount = 0;
                            $loan_amount = auth()->user()->loan_amount;
                            $loan_amount = $loan_amount - ($loan_amount * 10.75) / 100;
                            $final_amount = $loan_amount - ($loan_amount * 5.35) / 100;
                      ?>

                      <div class="form-group col-md-12 text-center">
                          <h4 class="text-success p-b-20">Congratulations! Your pre-approval <span class="text-lowercase">Personal Loan</span> eligibility offer is Rs. <span id="eligibilityamt">{{ number_format($final_amount,2) }}</span></h4>

                          <h5 class="p-b-20">As per your required loan amount - Rs. <strong>{{ auth()->user()->loan_amount }}</strong>. Your monthly EMI are as below. Kindly select any option:</h5>
                      </div>
                        <?php
                            $month36 =36;
                            $month48 = 48;
                            $month60 = 60;
                            // $loanAmount = auth()->user()->loan_amount;
                            $loanAmount = $final_amount;

                            $year3 = (($loanAmount * 10)/100)/36;
                            $year4 = (($loanAmount * 10)/100)/48;
                            $year5 = (($loanAmount * 10)/100)/60;
                        ?>
                      <div class="form-group col-sm-12 col-md-12 text-center text-dark">
                          <div class="form-check">
                              <label class="form-check-label m-l-15 text-left">
                                  <strong>Tenure</strong> <i class="fa fa-long-arrow-alt-right m-r-20 m-l-20"></i> <strong>EMI</strong>
                              </label>
                          </div>

                          <div class="form-check">
                              <input type="radio" class="form-check-input" name="tenure" id="foryear3" value="36" checked="">
                              <label class="form-check-label m-l-10" for="foryear3">
                                  36 Months <i class="fa fa-long-arrow-alt-right m-r-10 m-l-10"></i></label><span id="year3">{{ number_format($year3,2) }}</span></div>

                          <div class="form-check">
                              <input type="radio" class="form-check-input" name="tenure" id="foryear4" value="48">
                              <label class="form-check-label m-l-10" for="foryear4">
                                  48 Months <i class="fa fa-long-arrow-alt-right m-r-10 m-l-10"></i><span id="year4">{{ number_format($year4,2) }}</span></label>
                          </div>

                          <div class="form-check">
                              <input type="radio" class="form-check-input" name="tenure" id="foryear5" value="60">
                              <label class="form-check-label m-l-10" for="foryear5">
                                  60 Months <i class="fa fa-long-arrow-alt-right m-r-10 m-l-10"></i><span id="year5">{{ number_format($year5,2) }}</span></label>
                          </div>
                      </div>

                      <div class="form-group col-md-12 text-center text-uppercase js-confetti">
                          <button type="submit" id="form-submit2" class="btn btn-secondary">GET OFFER</button>
                      </div>

                      <div class="form-group col-md-12 text-center">
                          <hr>
                          <p class="m-b-0"><small>Need to know about eligibility criteria, <a class="text-primary" data-target="#modal" data-toggle="modal" href="#">Click here.</a></small></p>
                      </div>
                  </div>
              </form>			<!-- END : GET PRE-APPROVAL -->
          </div>
      </div>
  </section>

  <section class="p-b-30">
      <div class="container">
          <div class="heading-text heading-line text-center m-b-10">
              <h4 class="text-medium font-weight-500">Your Pre-Approved Loan Offers From Various Banks</h4>
          </div>
      </div>
  </section>


  <div class="modal fade" id="modal" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="modal-label">Eligibility Criteria : </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="col-md-12 text-justify">
                          <p>Your eligibility offer amount and interest rate cited is on the basis of the information provided by you. It is tentative and for informational purposes only. Actual interest rates and loan eligibility amount will vary. To verify eligibility and appropriate amount for a personal loan or business loan, the user needs to provide complete and accurate information / documents required. Further which the bank will verify the details provided and approve the eligibility for the same.</p>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>

  <footer id="footer">
      <div class="copyright-content background-dark">
          <div class="container">
            <div class="copyright-text text-center text-light">
                2022 © moneyupfinance Service India Pvt. Ltd. All rights reserved.
            </div>
          </div>
      </div>
  </footer>


    </div>

    <a id="scrollTop" style="bottom: 16px; opacity: 0;"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

  @include('layouts.footerscript')
  <script type="text/javascript">
      $(function(){
          $('#submitForm2').on('submit', function(e) {
              e.preventDefault();
              $('#form-submit2').attr('disabled', true);
              $('#form-submit2').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> PROCESS...');

              var tenureEmi = $("input[type='radio'][name='tenure']:checked").attr("value");

              if(tenureEmi == 36) {
                 tenureEMI = {month:tenureEmi,amount:$('#year3').text()}
              } else if(tenureEmi == 48) {
                 tenureEMI = {month:tenureEmi,amount:$('#year4').text()}
              } else {
                 tenureEMI = {month:tenureEmi,amount:$('#year5').text()}
              }

              localStorage.setItem('eligibilityamt',$('#eligibilityamt').text());

              localStorage.setItem('tenureEMI',JSON.stringify(tenureEMI));
              console.log(localStorage.getItem('eligibilityamt'))
              console.log(JSON.parse(localStorage.getItem('loanData')))
              console.log(JSON.parse(localStorage.getItem('tenureEMI')))
              window.location.replace("membershiporder");
          });
      });
  </script>
  </body></html>
