<html class="loading" lang="en" data-textdirection="ltr">

@include('layouts.headerscript')

<body class="breakpoint-xl b--desktop"><br>
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
        <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
            <div class="header-inner">
                <div class="container">

                    <div id="logo">
                        <a href="{{ url('/') }}">
                            <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                            <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                        </a>
                    </div>

                    <div class="header-extras">
                        <div class="p-dropdown">
                            <a class="x"><span class="lines"></span></a>
                            <ul class="p-dropdown-content">
                                <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a>
                                </li>
                                <li><a href="mailto:info@moneyupfinance.com"><i
                                            class="icon-mail"></i>info@moneyupfinance.com</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <section class="background-theme bg-loaded"
            data-bg-image="https://nowofloan.com/assets/images/slider/digital-process-bg.jpg"
            style="background-image: url(&quot;https://nowofloan.com/assets/images/slider/digital-process-bg.jpg&quot;);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 pb-4 text-center">
                        <h2 class="text-light">Digital Personal Loan Application Process</h2>
                        <p class="text-light m-b-30">Buy Membership Card &amp; Get Pre-Approved Loan Offer</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="reservation-form-over no-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-12 sm-hidden">
                        <div class="p-cb">
                            <div class="wizard clearfix" data-style="1" data-orientation="vertical" role="application">
                                <div class="steps clearfix m-0 p-r-0">
                                    <ul role="tablist">
                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">1</span><span class="title">Quick
                                                    Registration</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">2</span><span class="title">Check
                                                    Eligibility</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">3</span><span class="title">Get
                                                    Pre-Approval Offer</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">4</span><span class="title">Buy
                                                    Membership Card</span></a></li>

                                        <li role="tab" class="disabled"><a href="#"><span
                                                    class="number">5</span><span class="title">Submit
                                                    Documents</span></a></li>

                                        <li role="tab" class="disabled"><a href="#"><span
                                                    class="number">6</span><span class="title">Get
                                                    Sanctioned</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- START : GET MEMBERSHIP CARD -->
                    <form action="{{ route('loan.payment.success') }}" id="submitForm3"
                        class="col-lg-9 col-md-9 col-12" novalidate="novalidate" method="post" accept-charset="utf-8">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="step2" id="step2savedData" value="">
                        <input type="hidden" name="step3" id="step3savedData" value="">
                        <input type="hidden" name="eligible_amount" id="eligible_amount" value="">
                        <div class="row p-cb">

                            <div class="col-lg-12 col-md-12 col-sm-12 p-0">
                                <div class="row">
                                    <div class="col-md-7 col-12 text-center">
                                        <p><span class="badge badge-success">LOAN OFFER</span></p>
                                        <p class="m-b-20">You're Eligible For Pre-Approved Personal Loan Offer -
                                            <strong>Rs. <span id="eligibilityamt"></span></strong>
                                        </p>
                                    </div>

                                    <div class="col-md-5 col-12 text-center">
                                        <p class="m-b-10"><small>Offer Ending Soon</small></p>
                                        <div class="countdown small" data-countdown="2022/03/01 24:00:00">
                                            <div class="countdown-container">
                                                <div class="countdown-box">
                                                    <div class="number">1</div><span>Day</span>
                                                </div>
                                                <div class="countdown-box">
                                                    <div class="number">00</div><span>Hours</span>
                                                </div>
                                                <div class="countdown-box">
                                                    <div class="number">30</div><span>Minutes</span>
                                                </div>
                                                <div class="countdown-box">
                                                    <div class="number">10</div><span>Seconds</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="line m-t-10"></div>
                            </div>

                            <div class="col-lg-7 col-md-7 col-sm-12 p-0">
                                <input type="hidden" name="orderamount" id="orderamount" value="999"
                                    class="form-control">
                                <input type="hidden" name="paymentid" id="paymentid" value="" class="form-control">
                                <div class="text-center">
                                    <div class="credit">
                                        <div class="credit__front credit__part premium-card">
                                            <div class="credit__head">
                                            </div>
                                            <p class="credit_numer text-left">**** **** **** 4637</p>
                                            <div class="credit__space-full text-left">
                                                <span class="credit__label">VALID FROM {{ date('d/m/Y') }} VALID TO
                                                    {{ date('d/m/Y', strtotime('+10 years')) }}</span>
                                                <p class="credit__info">{{ auth()->user()->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 p-0 text-center">
                                <div class="form-group text-center m-t-20">
                                    <h4>Buy Membership Card Now!</h4>

                                    <h3>Rs. <del class="text-danger">3,999.00</del> <span
                                            class="text-success">999.00</span> only <span class="text-xxs">(GST
                                            Additional)</span></h3>
                                </div>

                                <div class="form-group text-center text-uppercase">
                                    <button type="submit" id="form-submit3" class="btn btn-secondary">BUY NOW</button>
                                    <div id="resmessage"></div>
                                </div>
                            </div>

                            <!-- END : GET MEMBERSHIP CARD -->

                        </div>
                    </form>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="heading-text heading-line text-center p-b-10">
                    <h4 class="text-medium font-weight-500">Our Customers Testimonials</h4>
                </div>

                <div class="carousel equalize testimonial testimonial-box" data-margin="20" data-arrows="false"
                    data-items="3" data-items-sm="2" data-items-xxs="1" data-equalize-item=".testimonial-item">
                    <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/manoj-makwana.png') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
                        <p>I faced no problems in getting personal loan from moneyupfinance. All I had to do is get their Membership Card.</p>
                        <span class="p-b-20">Manoj Chudiwal</span>
                    </div>

                    <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/abdul-khan.png') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="5.00"></div>
                        <p>Membership card has benefited me with so many things. Besides getting quick loan, I started earning 40% through easy referrals.</p>
                        <span class="p-b-20">Ashish Amant</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/amrit-giri.jpg') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
                        <p>In just few clicks, I received loan money in my bank. moneyupfinance gave best services & support to me. Really nice experience!</p>
                        <span class="p-b-20">Satish Kumar</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/anil-bharti.png') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="5.00"></div>
                        <p>I got a personal loan with a very low interest rate & easy EMI options via Membership Card. Thank you moneyupfinance!</p>
                        <span class="p-b-20">Parvej Ansari</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/satendra-singh.jpg') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.00"></div>
                        <p>Best part of moneyupfinance Membership Card is that I didn’t have to go to banks. Entire loan process was digital & easy.</p>
                        <span class="p-b-20">Bhim Singh</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/dhaval-bhatt.jpg') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
                        <p>moneyupfinance provided me with pre-approved loan offers from many banks. I received money very quickly. Best loan service for sure!</p>
                        <span class="p-b-20">Anshul Asati</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/chirag-kukadiya.png') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.00"></div>
                        <p>My experience with moneyupfinance membership card is great. Money was credited in just 30 minutes with online loan process.</p>
                        <span class="p-b-20">Manish Kumar</span>
                    </div>
                                <div class="testimonial-item">
                        <img src="{{ asset('public/assets/images/customers/suresh-lalvani.jpg') }}" alt="customer img">
                        <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
                        <p>Thanks to moneyupfinance for providing a personal loan on urgent basis. I got loan offers from multiple banks through Membership Card.</p>
                        <span class="p-b-20">Balaji Bocakari</span>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer">
            <div class="copyright-content background-dark">
                <div class="container">
                    <div class="copyright-text text-center text-light">
                        2022 © moneyupfinance Service India Pvt. Ltd. All rights reserved.
                    </div>
                </div>
            </div>
        </footer>


    </div>

    <a id="scrollTop" style="bottom: 16px; opacity: 0; z-index: 199;"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>


    @include('layouts.footerscript')

    <div class="razorpay-container" style="z-index: 1000000000; position: fixed; top: 0px; display: none; left: 0px; height: 100%; width: 100%; backface-visibility: hidden; overflow-y: visible;">
        <style>
            @keyframes rzp-rot {
                to {
                    transform: rotate(360deg);
                }
            }

            @-webkit-keyframes rzp-rot {
                to {
                    -webkit-transform: rotate(360deg);
                }
            }

        </style>
        <div class="razorpay-backdrop"
            style="min-height: 100%; transition: all 0.3s ease-out 0s; position: fixed; top: 0px; left: 0px; width: 100%; height: 100%;">
            <span
                style="text-decoration: none; background: rgb(214, 68, 68); border: 1px dashed white; padding: 3px; opacity: 0; transform: rotate(45deg); transition: opacity 0.3s ease-in 0s; font-family: lato, ubuntu, helvetica, sans-serif; color: white; position: absolute; width: 200px; text-align: center; right: -50px; top: 50px;">Test
                Mode</span>
        </div><iframe
            style="opacity: 1; height: 100%; position: relative; background: none; display: block; border: 0 none transparent; margin: 0px; padding: 0px; z-index: 2;"
            allowtransparency="true" frameborder="0" width="100%" height="100%" allowpaymentrequest="true"
            src="https://api.razorpay.com/v1/checkout/public" class="razorpay-checkout-frame"></iframe>
    </div>


    <script type="text/javascript">
        $(function() {
            $("#step2savedData").val(localStorage.getItem('loanData'))
            $("#step3savedData").val(localStorage.getItem('tenureEMI'))
            $('#eligibilityamt').text(localStorage.getItem('eligibilityamt'));
            $('#eligible_amount').val(localStorage.getItem('eligibilityamt'));

            $('#submitForm3').on('submit', function(e) {
                e.preventDefault();
                $('#form-submit3').attr('disabled', true);
                $('#form-submit3').html(
                    '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> PROCESSING...'
                );

                const amount = 999 + (999 * 18) / 100; //add 18 % GST

                var options = {
                    "key": "{{ env('RAZORPAY_PUBLIC_KEY') }}",
                    "amount": (amount * 100).toFixed(),
                    "currency": "INR",
                    "name": "Money Up Finance",
                    "description": "Membership Card Purchase",
                    "prefill": {
                        "name": '{{ auth()->user()->name }}',
                        "email": '{{ auth()->user()->email }}',
                        "contact": '{{ auth()->user()->mobile }}'
                    },
                    "notify": {
                        "sms": true,
                        "email": true
                    },
                    "modal": {
                        "ondismiss": function() {
                            location.reload();
                        }
                    },
                    "handler": function(response) {
                        if (response.razorpay_payment_id != "") {
                            document.getElementById('paymentid').value = response.razorpay_payment_id;
                            document.getElementById('submitForm3').submit();
                        } else {
                            setTimeout(function() {
                                location.reload();
                            }, 1500);
                        }
                    }
                };

                var rzp1 = new Razorpay(options);
                rzp1.open();
            });
        });
    </script>
</body>

</html>
