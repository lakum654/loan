<html class="loading" lang="en" data-textdirection="ltr">

@include('layouts.headerscript')

<body class="breakpoint-xl b--desktop"><br>
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
        <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
            <div class="header-inner">
                <div class="container">

                    <div id="logo">
                        <a href="{{ url('/') }}">
                            <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="Nowof Loan" width="120"></span>
                            <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="Nowof Loan" width="120"></span>
                        </a>
                    </div>

                    <div class="header-extras">
                        <div class="p-dropdown">
                            <a class="x"><span class="lines"></span></a>
                            <ul class="p-dropdown-content">
                                <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a>
                                </li>
                                <li><a href="mailto:info@nowofloan.com"><i
                                            class="icon-mail"></i>info@nowofloan.com</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <section class="background-theme bg-loaded"
            data-bg-image="https://nowofloan.com/assets/images/slider/digital-process-bg.jpg"
            style="background-image: url(&quot;https://nowofloan.com/assets/images/slider/digital-process-bg.jpg&quot;);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 pb-4 text-center">
                        <h2 class="text-light">Digital Personal Loan Application Process</h2>
                        <p class="text-light m-b-30">Buy Membership Card &amp; Get Pre-Approved Loan Offer</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="reservation-form-over no-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-12 sm-hidden">
                        <div class="p-cb">
                            <div class="wizard clearfix" data-style="1" data-orientation="vertical" role="application">
                                <div class="steps clearfix m-0 p-r-0">
                                    <ul role="tablist">
                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">1</span><span class="title">Quick
                                                    Registration</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">2</span><span class="title">Check
                                                    Eligibility</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">3</span><span class="title">Get
                                                    Pre-Approval Offer</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">4</span><span class="title">Buy
                                                    Membership Card</span></a></li>

                                        <li role="tab" class="current"><a href="#"><span
                                                    class="number">5</span><span class="title">Submit
                                                    Documents</span></a></li>

                                        <li role="tab" class="disabled"><a href="#"><span
                                                    class="number">6</span><span class="title">Get
                                                    Sanctioned</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <form action="{{ route('loan.step.document.submit') }}" id="submitForm13" class="col-lg-9 col-md-9 col-12"
                        method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <div class="row p-cb">

                            <div class="col-lg-12 col-md-12 col-sm-12 p-0">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Profile Photo</label>
                                                <div class="input-group">
                                                    <input type="file" name="profile_photo" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('profile_photo') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="Button" class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('profile_photo')}}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Aadhar Card</label>
                                                <div class="input-group">
                                                    <input type="file" name="aadhar_card" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('aadhar_card') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('aadhar_card')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Pan Card</label>
                                                <div class="input-group">
                                                    <input type="file" name="pan_card" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('pan_card') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('pan_card')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Address Proof - Light Bill</label>
                                                <div class="input-group">
                                                    <input type="file" name="address_proof" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('address_proof') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('address_proof')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Cancel Cheque</label>
                                                <div class="input-group">
                                                    <input type="file" name="cancel_cheque" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('cancel_cheque') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('cancel_cheque')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Form 16</label>
                                                <div class="input-group">
                                                    <input type="file" name="form_16" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('form_16') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button"
                                                                class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('form_16')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Bank Statement - Last 6 Months</label>
                                                <div class="input-group">
                                                    <input type="file" name="bank_statement" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('bank_statement') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button"
                                                                class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('bank_statement')}}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="p-cb process border-top-tomato">
                                            <div class="form-group">
                                                <label class="form-control-label">Salary Slip</label>
                                                <div class="input-group">
                                                    <input type="file" name="salary_slip" id="user_photo"
                                                        class="form-control" aria-required="true" required=""
                                                        accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.pdf" value="{{ old('salary_slip') }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-btn">
                                                            <button type="button"
                                                                class="btn btn-light">UPLOAD</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="help-block font-small-3 text-danger">{{ $errors->first('salary_slip')}}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-12">
                                        <div class="form-group">
                                            <label class="form-control-label">Remarks</label>
                                            <div class="input-group">
                                                <textarea name="remarks" id="remarks" rows="5"
                                                    class="form-control"></textarea>
                                            </div>
                                            <div class="help-block font-small-3 text-danger">{{ $errors->first('remarks')}}</div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" id="submit-submit1"
                                                class="btn btn-outline btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="line m-t-10"></div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <script>

    </script>

    @include('layouts.footerscript')
</body>

</html>
