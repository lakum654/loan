<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<!-- Mirrored from nowofloan.com/digital/businessLoan by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Feb 2022 08:04:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Get online Business loan approval in 30 minutes | moneyupfinance </title>
  <meta name="description" content="Get easy Instant Business loan approval within a minute without leaving your home, We let you apply at multiple banks for a faster loan process. Click for more." />
  <meta name="keywords" content="Apply for Business Loan, Business Loan online, Instant Business Loan" />
  <meta name="author" content="vw-team">
  <link rel="canonical" href="businessLoan.html" />
  <meta name="robots" content="index, follow" />
  <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
  <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
  <meta property="og:image" content="../assets/images/nowofloanlogo.jpg" />

  @include('layouts.headerscript')
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-161757010-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 418840398 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-418840398"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-418840398');
</script>

<!-- Facebook Domain + Pixel Code -->
<meta name="facebook-domain-verification" content="qlipicd3au8ys6aao97gqu0y15p3k2" /><script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  '../../connect.facebook.net/en_US/fbevents.js');
  fbq('init', '490578892301965');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=490578892301965&amp;ev=PageView&amp;noscript=1" /></noscript>
<!-- End Facebook Domain + Pixel Code -->

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "moneyupfinance.com",
  "alternateName": "moneyupfinance Service India Pvt. Ltd.",
  "url": "https://nowofloan.com/",
  "logo": "https://nowofloan.com/assets/images/logo-large.png",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+919081545972",
    "contactType": "customer service",
    "areaServed": "IN",
    "availableLanguage": ["en","Hindi","Gujarati"]
  },
  "sameAs": [
    "https://www.facebook.com/nowofloan.in/",
    "https://twitter.com/nowofloan/",
    "https://www.instagram.com/nowofloan.in/",
    "https://www.linkedin.com/company/nowofloan/",
    "https://in.pinterest.com/nowofloan/",
    "https://www.youtube.com/channel/UC-YuwjTXkBlhazMqhqS-m2w"
  ]
}
</script>

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","../../chimpstatic.com/mcjs-connected/js/users/c7b98423fac62ff5c0187aa80/68ff10d3e4181a0f8dee72111.js");</script>
</head>
<body>
  <div class="body-inner">
    <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
      <div class="header-inner">
        <div class="container">

          <div id="logo">
            <a href="{{ url('/') }}">
              <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
              <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
            </a>
          </div>

          <div class="header-extras">
            <div class="p-dropdown">
                <a class="x"><span class="lines"></span></a>
                <ul class="p-dropdown-content">
                    <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a></li>
                    <li><a href="mailto:info@moneyupfinance.com"><i class="icon-mail"></i>info@moneyupfinance.com</a></li>
                </ul>
            </div>
          </div>

        </div>
      </div>
    </header>

<section class="profile-content">
	<div class="profile-image" style="background-color:#F4F6F9;">
		<div class="profile-name">
			<h2>Deal with all your business financial needs with quick business loan from moneyupfinance</h2>
			<ul class="list-icon list-icon-check list-icon-colored">
				<li>Business loan upto Rs. 1 Cr. in 48 hours*</li>
				<li>Get Instant business loan offer multiple banks*</li>
				<li>Low annual percentage rate starting from 11.5%*</li>
			</ul>

			<div class="line"></div>
			<h4>Our Lending Bank Partners</h4>

			<div class="carousel client-logos" data-items="4" data-dots="false" data-arrows="false">
			<div><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></div><div><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></div><div><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></div><div><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></div><div><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></div><div><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></div><div><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></div><div><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></div>			</div>
		</div>
	</div>

	<div class="profile-bio">
	<section class="fullscreen p-t-40">
		<div class="container m-0 p-10">
				<div class="text-middle">
			<h5 class="p-b-10">Get ₹10 Lakhs Business Loan in Just 3 Steps - <strong class="text-theme">Apply Now!</strong></h5>

			<form action="https://nowofloan.com/digital/sendotpCode" id="submitForm1" class="col-md-12 col-sm-12 p-0" novalidate="novalidate" method="post" accept-charset="utf-8">
				<input type="hidden" name="loantype" value="12" required>

				<div class="form-group">
					<label class="text-dark" for="fullname">Full name</label>
					<input type="text" aria-required="true" name="fullname" id="fullname" class="form-control" placeholder="Bank registerd name" required data-validation-regex-regex="^[a-zA-Z ]*$">
					<div class="help-block font-small-3"></div>
				</div>

				<div class="form-group">
					<label class="text-dark" for="mobile">Mobile no.</label>
					<input type="text" aria-required="true" name="mobile" id="mobileno" class="form-control" placeholder="Bank registerd number" required maxlength="10" data-validation-regex-regex="^[6789]\d{9}$">
					<div class="help-block font-small-3"></div>
				</div>

				<div class="form-group">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" aria-required="true" name="conditions" id="conditions" class="custom-control-input" value="1" required>
						<label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the <a href="../terms-conditions.html" target="_blank">Terms of Use</a> and <a href="../privacy-policy.html" target="_blank">Privacy Policy</a> of moneyupfinance.com</small></label>
						<div class="help-block font-small-3"></div>
					</div>
				</div>

				<div class="custom-error" id="mobilenoError"></div>

				<div class="form-group m-b-0">
					<button type="submit" id="form-submit1" class="btn btn-block btn-secondary">APPLY NOW</button>
				</div>
			</form>		</div>


		<div class="line"></div>

		<div class="text-center">
			<p class="text-theme">Already have an account?
				<a href="https://nowofloan.com/customer/login" class="btn btn-outline btn-sm text-uppercase">Login</a>
			</p>
		</div>

		</div>
	</section>

	</div>
</section>

<section class="border-top">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-12">
				<h6 class="text-theme">Small Business Person Eligibility Criteria</h6>
				<p><small>(1) Minimum age 21 years (2) Minimum 1 year IT return (3) 1 year business stability</small></p>

				<h6 class="text-theme">Audited Report Person Eligibility Criteria</h6>
				<p><small>(1) Minimum age 21 years (2) 1 crore plus yearly turnover (3) Minimum 2 year audited Report</small></p>

				<h6 class="text-theme">How to works?</h6>
				<p><small>(1) Quick registration (2) Check eligibility (3) Buy membership card (4) Submit documents (5) Bank Verification (6) Bank Sanction</small></p>

				<h6 class="text-theme">Why moneyupfinance.com?</h6>
				<p><small>(1) Per month 1000+ happy customers (2) Minimum documents - depend customer profile (3) Compare to multiple banks (4) Flexible repayment terms 1 to 6 years (5) Processing fees & insurance depand banks</small></p>

				<h6 class="text-theme">Membership Card Benefits</h6>
				<p><small>(1) Let you apply for business loan in multiple banks (2) You get 10 years free loan consultancy (3) Get 40% referral payout bonus (4) Get loan offers from multiple banks anytime-anywhere (5) No effect on CIBIL of multiple bank verification (6) On-call Assistance on all your doubts</small></p>

				<div class="profile-bio-footer">
					<h6 class="text-theme">Contact Us</h6>
					<address>
						<small>
							<strong>Registered Office Address:</strong>
							<br/>4001,<sup></sup> Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009<br/><i class='fa fa-gavel m-r-5'></i> CIN No.: U31101GJ2016PTC813691<br/><i class='fa fa-phone-square m-r-5'></i>+91 97237-97077<br/><i class='fa fa-envelope m-r-5'></i>info@moneyupfinance.com<br/><i class='fa fa-clock m-r-5'></i>10 AM to 6 PM (Monday to Saturday)						</small>
					</address>
				</div>
			</div>

			<div class="col-md-5 col-12">
				<h5 class="text-theme">Overview Business Loan</h5>

				<p class="text-justify"><small>Being a business owner, you are much aware of the financial need that may arise at any point or stage of your business flow. So, to ensure that your business does not face any interruptions, you must have some backup money – and the smartest option to do so is getting an Instant Business Loan from moneyupfinance.com. Be it for making supplier payments, or clearing any pending debts, or hiring new resources to boost your business, or even for the makeover of your office for gaining a good first impression – you can easily avail of Quick Business Loan Offers from moneyupfinance.com. Collaborated with multiple banks and NBFCs, moneyupfinance provides you with Business Loan offers for you to choose the best one according to your necessity.</small></p>

				<ul class="text-dark">
					<li><small>Get Business Loan - up to ₹1 Crore</small></li>
					<li><small>ReasonabReasonable Interest Rate starting at 11%</small></li>
					<li><small>Processing Charge of 1.5% + GST (18%)</small></li>
				</ul>

				<p class="text-justify"><small>With an example, we'll try to understand the specifics of a Business Loan. If a businessman/businesswoman takes a business loan of ₹1 Lakh at the annual interest rate of 11% for a repayment tenure of 6 years, the loan processing fees charged by the banks or any lending firm would be 1.5% + GST (18%), the EMI would be ₹1904 per month, and the loan insurance amount would depend on the applicant's age.</small></p>
			</div>
		</div>
	</div>
</section>

<footer id="footer" style="z-index:10;">
	<div class="copyright-content background-dark">
	    <div class="container">
	      <div class="copyright-text text-center text-light">
	          2022 &copy; moneyupfinance Service India Pvt. Ltd. All rights reserved.
	      </div>
	    </div>
	</div>
</footer>


  </div>

  <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

  @include('layouts.footerscript')
</body>
</html>

<script type="text/javascript">
    function resendotp() {
        fullname = document.getElementById('usernm').value;
        mobile = document.getElementById('otpmobile').value;

        $.ajax({
          url : 'https://nowofloan.com/digital/resendotpCode',
          type: "POST",
          data: 'mobile=' + mobile + '&fullname=' + fullname,
          dataType: "JSON",
          cache: false,
          processData: false,
          success: function (response) {
            if(response['success'] == true) {
                $('#resend-message').html(response['message']);
                $.notify({ message: response['message'] },{ type: 'success' });
            }
            else {
                $.notify({ message: response['message'] },{ type: 'danger' });
            }
          },
          error: function (jXHR, textStatus, errorThrown) {
              $.notify({ message: errorThrown },{ type: 'danger' });
          }
      });
    }

    $(function(){
        $('#submitForm1').on('submit', function(e) {
          e.preventDefault();

          $.ajax({
              url : $(this).attr('action') || window.location.pathname,
              type: "POST",
              data: $(this).serialize(),
              dataType: "JSON",
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('#form-submit1').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> APPLYING...');
                  $('#form-submit1').attr('disabled', true);
              },
              success: function (response) {
                if(response['success'] == true) {
                        if(response['redirect_url'] != "") {
                            window.location.href = response['redirect_url'];
                            $('#form-submit1').html('APPLY NOW');
                        }
                        else {
                            window.location = "./businessLoan/s2/" + response['mobile'];
                        }
                }
                else {
                      $('#mobilenoError').html(response['message']);
                      $.notify({ message: response['message'] },{ type: 'danger' });
                }

                $('#form-submit1').html('APPLY NOW');
                $('#form-submit1').attr('disabled', false);
              },
              error: function (jXHR, textStatus, errorThrown) {
                  $('#form-submit1').html('APPLY NOW');
                  $('#form-submit1').attr('disabled', false);
                  $.notify({ message: errorThrown },{ type: 'danger' });
              }
          });
      });

      $('#submitForm2').on('submit', function(e) {
          e.preventDefault();
             $('#resend-message').html('');
             $('#otpcodeError').html('');

          $.ajax({
              url : $(this).attr('action') || window.location.pathname,
              type: "POST",
              data: $(this).serialize(),
              dataType: "JSON",
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('#form-submit2').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> VERIFYING...');
                  $('#form-submit2').attr('disabled', true);
              },
              success: function (response) {
                if(response['success'] == true) {
                        window.location = "../../businessLoan/s3/" + response['mobile'];
                }
                else {
                      $('#otpcodeError').html(response['message']);
                      $.notify({ message: response['message'] },{ type: 'danger' });
                }

                $('#form-submit2').html('VERIFY');
                $('#form-submit2').attr('disabled', false);
              },
              error: function (jXHR, textStatus, errorThrown) {
                  $('#form-submit2').html('VERIFY');
                  $('#form-submit2').attr('disabled', false);
                  $.notify({ message: errorThrown },{ type: 'danger' });
              }
          });
      });

      $('#submitForm3').on('submit', function(e) {
          e.preventDefault();
             $('#resend-message').html('');
             $('#otpcodeError').html('');

          $.ajax({
              url : $(this).attr('action') || window.location.pathname,
              type: "POST",
              data: $(this).serialize(),
              dataType: "JSON",
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('#form-submit3').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> PROCESSING...');
                  $('#form-submit3').attr('disabled', true);
              },
              success: function (response) {
                if(response['success'] == true) {
                        window.location.href = response['redirect_url'];
                }
                else {
                      $('#otpcodeError').html(response['message']);
                      $.notify({ message: response['message'] },{ type: 'danger' });
                }

                $('#form-submit3').html('PROCESS');
                $('#form-submit3').attr('disabled', false);
              },
              error: function (jXHR, textStatus, errorThrown) {
                  $('#form-submit3').html('PROCESS');
                  $('#form-submit3').attr('disabled', false);
                  $.notify({ message: errorThrown },{ type: 'danger' });
              }
          });
      });
  });
</script>
