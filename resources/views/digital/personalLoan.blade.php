<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<!-- Mirrored from nowofloan.com/digital/personalLoan by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Feb 2022 08:04:34 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Get online personal loan approval in 30 minutes | moneyupfinance </title>
  <meta name="description" content="Get easy Instant personal loan approval within a minute, that too without leaving your home, We let you apply at multiple banks for a faster loan process. " />
  <meta name="keywords" content="Apply for personal loan, Personal loan online, Instant personal loan" />
  <meta name="author" content="vw-team">
  <link rel="canonical" href="personalLoan.html" />
  <meta name="robots" content="index, follow" />
  <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
  <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
@include('layouts.headerscript')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161757010-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-161757010-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 418840398 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-418840398"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-418840398');
</script>

<!-- Facebook Domain + Pixel Code -->
<meta name="facebook-domain-verification" content="qlipicd3au8ys6aao97gqu0y15p3k2" /><script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  '../../connect.facebook.net/en_US/fbevents.js');
  fbq('init', '490578892301965');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=490578892301965&amp;ev=PageView&amp;noscript=1" /></noscript>
<!-- End Facebook Domain + Pixel Code -->

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Nowofloan.com",
  "alternateName": "moneyupfinance Service India Pvt. Ltd.",
  "url": "https://nowofloan.com/",
  "logo": "https://nowofloan.com/assets/images/logo-large.png",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+919081545972",
    "contactType": "customer service",
    "areaServed": "IN",
    "availableLanguage": ["en","Hindi","Gujarati"]
  },
  "sameAs": [
    "https://www.facebook.com/nowofloan.in/",
    "https://twitter.com/nowofloan/",
    "https://www.instagram.com/nowofloan.in/",
    "https://www.linkedin.com/company/nowofloan/",
    "https://in.pinterest.com/nowofloan/",
    "https://www.youtube.com/channel/UC-YuwjTXkBlhazMqhqS-m2w"
  ]
}
</script>

</head>
<body>
  <div class="body-inner">
    <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
      <div class="header-inner">
        <div class="container">

          <div id="logo">
            <a href="{{ url('/') }}">
              <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
              <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
            </a>
          </div>

          <div class="header-extras">
            <div class="p-dropdown">
                <a class="x"><span class="lines"></span></a>
                <ul class="p-dropdown-content">
                    <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a></li>
                    <li><a href="mailto:info@moneyupfinance.com"><i class="icon-mail"></i>info@moneyupfinance.com</a></li>
                </ul>
            </div>
          </div>

        </div>
      </div>
    </header>

<section class="profile-content">
	<div class="profile-image" style="background-color:#F4F6F9;">
		<div class="profile-name">
			<h2>Attain Hassle-Free Personal Loan and Meet Your Monetary Needs</h2>
			<ul class="list-icon list-icon-check list-icon-colored">
				<li>Personal loan upto Rs. 15 lakhs in 30 minutes*</li>
				<li>Get Instant personal loan offer multiple banks*</li>
				<li>Low annual percentage rate starting from 12.5%*</li>
			</ul>

			<div class="line"></div>
			<h4>Our Lending Bank Partners</h4>

			<div class="carousel client-logos" data-items="4" data-dots="false" data-arrows="false">
			<div><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></div><div><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></div><div><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></div><div><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></div><div><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></div><div><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></div><div><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></div><div><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></div>			</div>
		</div>
	</div>

	<div class="profile-bio">
	<section class="fullscreen p-t-40">
		<div class="container m-0 p-10">

				<div class="text-middle">
			<h5 class="p-b-10">Get ₹5 Lakhs Personal Loan in Just 3 Steps - <strong class="text-theme">Apply Now!</strong></h5>

			{{-- <form action="{{ route('login.sendOtp') }}" id="submitForm1" class="col-md-12 col-sm-12 p-0" novalidate="novalidate" method="post" accept-charset="utf-8"> --}}
			<form action="{{ route('login.verifyOtp') }}" id="submitForm1" class="col-md-12 col-sm-12 p-0" novalidate="novalidate" method="post" accept-charset="utf-8">
                <input type="hidden" name="loantype" value="11" required>
				@csrf
				<div class="form-group">
					<label class="text-dark" for="fullname">Full name</label>
					<input type="text" aria-required="true" name="fullname" id="fullname" class="form-control" placeholder="Bank registerd name" required data-validation-regex-regex="^[a-zA-z]+([\s][a-zA-Z]+)*$">
					<div class="help-block font-small-3"></div>
				</div>

				<div class="form-group">
					<label class="text-dark" for="mobile">Mobile no.</label>
					<input type="text" aria-required="true" name="mobile" id="mobileno" class="form-control" placeholder="Bank registerd number" required maxlength="10" data-validation-regex-regex="[0-9]+">
					<div class="help-block font-small-3"></div>
				</div>

				<div class="form-group">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" aria-required="true" name="conditions" id="conditions" class="custom-control-input" value="1" required>
						<label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the <a href="terms-conditions" target="_blank">Terms of Use</a> and <a href="../privacy-policy.html" target="_blank">Privacy Policy</a> of moneyupfinance.com</small></label>
						<div class="help-block font-small-3"></div>
					</div>
				</div>

				<div class="custom-error" id="mobilenoError"></div>

				<div class="form-group m-b-0">
					<button type="submit" id="form-submit1" class="btn btn-block btn-secondary">APPLY NOW</button>
				</div>
			</form>		</div>


		<div class="line"></div>

		<div class="text-center">
			<p class="text-theme">Already have an account?
			<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-sm text-uppercase">Login</a>
			</p>
		</div>

		</div>
	</section>

	</div>
</section>

<section class="border-top">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-12">
				<h6 class="text-theme">Salaried Person Eligibility Criteria</h6>
				<p><small>(1) Minimum salary Rs. 15,000/- per month (2) 1 year job stability (3) Minimum age 21 years</small></p>

				<h6 class="text-theme">Self-Employed Person Eligibility Criteria</h6>
				<p><small>(1) Minimum 1 year IT return (2) 1 year business stability (3) Minimum age 21 years</small></p>

				<h6 class="text-theme">How to works?</h6>
				<p><small>(1) Quick registration (2) Check eligibility (3) Buy membership card (4) Submit documents (5) Bank Verification (6) Bank Sanction</small></p>

				<h6 class="text-theme">Why moneyupfinance.com?</h6>
				<p><small>(1) Per month 1000+ happy customers (2) Minimum documents - depend customer profile (3) Compare to multiple banks - loan up to Rs. 15,00,000/-* (4) Annual percentage rate - minimum 12.5% and maximum 24%* (5) Flexible repayment terms - 1 to 6 years (6) Processing fees - depand on customer profile & banks</small></p>

				<h6 class="text-theme">Membership Card Benefits</h6>
				<p><small>(1) Let you apply for personal loan in multiple banks (2) You get 10 years free loan consultancy (3) Get 40% referral payout bonus (4) Get loan offers from multiple banks anytime-anywhere (5) No effect on CIBIL of multiple bank verification (6) On-call Assistance on all your doubts</small></p>

				<div class="profile-bio-footer">
					<h6 class="text-theme">Contact Us</h6>
					<address>
						<small>
							<strong>Registered Office Address:</strong>
							<br/>4001,<sup></sup> Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009<br/><i class='fa fa-gavel m-r-5'></i> CIN No.: U31101GJ2016PTC813691<br/><i class='fa fa-phone-square m-r-5'></i>+91 97237-97077<br/><i class='fa fa-envelope m-r-5'></i>info@moneyupfinance.com<br/><i class='fa fa-clock m-r-5'></i>10 AM to 6 PM (Monday to Saturday)						</small>
					</address>
				</div>
			</div>

			<div class="col-md-5 col-12">
				<h5 class="text-theme">Overview Personal Loan</h5>

				<p class="text-justify"><small>Money requirements can arise at any time and at any specific instance of life. If you are planning for an exciting vacation at an enthralling destination, or any of your family members is hit with a medical emergency, or you want the dull interiors of your home to be renovated, or wedding bells are about to ring in your home – the most convenient option to meet your money needs is by availing an Instant Personal Loan from moneyupfinance.com. Partnered with multiple leading banks and NBFCs, moneyupfinance facilitates you with easy and convenient personal loan offers with a 100% online process, that too utmost quickly!</small></p>

				<ul class="text-dark">
					<li><small>Get Personal Loan - up to ₹15 Lakhs</small></li>
					<li><small>Reasonable Interest Rate starting at 12.5%</small></li>
					<li><small>Processing Charge of 2%</small></li>
				</ul>

				<p class="text-justify"><small>Let's consider an example of an individual who is in pursuit of a personal loan of ₹1 Lakh at the annual interest rate of 12.5% for a repayment tenure of 6 years. As the lending firms charge the processing fees, this individual would have to pay 2% of the loan amount as processing fees. So, the EMI that this individual would have to pay is ₹1981 per month. Additionally, the loan insurance amount has to be paid; this amount is usually 2% but it also depends on the applicant's age.</small></p>
			</div>
		</div>
	</div>
</section>

<footer id="footer" style="z-index:10;">
	<div class="copyright-content background-dark">
	    <div class="container">
	      <div class="copyright-text text-center text-light">
	          2022 &copy; moneyupfinance Service India Pvt. Ltd. All rights reserved.
	      </div>
	    </div>
	</div>
</footer>


  </div>

  <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

  @include('layouts.footerscript')
</body>

<!-- Mirrored from nowofloan.com/digital/personalLoan by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Feb 2022 08:04:35 GMT -->
</html>
<script type="text/javascript">
  	$(function(){
        $token = '{{ csrf_token() }}';
	  	// $('#submitForm1').on('submit', function(e) {
	    //     e.preventDefault();
	    //     $.ajax({
	    //         url : $(this).attr('action'),
	    //         type: "POST",
	    //         data: $(this).serialize(),
	    //         dataType: "JSON",
	    //         cache: false,
	    //         processData:false,
	    //         beforeSend: function(){
	    //             $('#form-submit1').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> APPLYING...');
		// 			$('#form-submit1').attr('disabled', true);
	    //         },
	    //         success: function (response) {
	    //           if(response['success'] == true) {
	    //           		if(response['redirect_url'] != "") {
		// 					$.notify({ message: response['message'] },{ type: 'success' });
		// 					localStorage.setItem("fullname", document.getElementById('fullname').value);
		// 					localStorage.setItem("mobile", document.getElementById('mobileno').value);
		// 					setTimeout(() => {
		// 						window.location.href = response['redirect_url'];
		// 					}, 1000);
	    //           		}
	    //           		else {
	    //           			window.location.reload();
	    //           		}
	    //           }
	    //           else {
	    //             	$('#mobilenoError').html(response['message']);
		// 				$.notify({ message: response['message'] },{ type: 'danger' });
	    //           }

	    //           $('#form-submit1').html('APPLY NOW');
	    //           $('#form-submit1').attr('disabled', false);
	    //         },
	    //         error: function (jXHR, textStatus, errorThrown) {
	    //             $('#form-submit1').html('APPLY NOW');
		// 			$('#form-submit1').attr('disabled', false);
		// 			$.notify({ message: errorThrown },{ type: 'danger' });
	    //         }
	    //     });
	    // });

        $('#submitForm1').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $token
                },
                data: {
                    fullname: document.getElementById('fullname').value,
                    mobile: document.getElementById('mobileno').value
                },
                dataType: "JSON",
                cache: false,
                beforeSend: function() {
                    $('#form-submit1').html(
                        '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> VERIFYING...'
                        );
                    $('#form-submit1').attr('disabled', true);
                },
                success: function(response) {
                    if (response['success'] == true) {
                        $.notify({
                            message: response['message']
                        }, {
                            type: 'success'
                        });
                        setTimeout(() => {
                            window.location.href = response['redirect_url'];
                        }, 1000);
                    } else {
                        $('#otpcodeError').html(response['message']);
                        $.notify({
                            message: response['message']
                        }, {
                            type: 'danger'
                        });
                    }

                    $('#form-submit1').html('VERIFY');
                    $('#form-submit1').attr('disabled', false);
                },
                error: function(jXHR, textStatus, errorThrown) {
                    $('#form-submit1').html('VERIFY');
                    $('#form-submit1').attr('disabled', false);
                    $.notify({
                        message: errorThrown
                    }, {
                        type: 'danger'
                    });
                }
            });
        });
	});
</script>
