<html class="loading" lang="en" data-textdirection="ltr">
<title>Get online personal loan approval in 30 minutes | moneyupfinance </title>

@include('layouts.headerscript')

<body class="breakpoint-xl b--desktop">
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
        <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
            <div class="header-inner">
                <div class="container">

                    <div id="logo">
                        <a href="#">
                            <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                            <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                        </a>
                    </div>

                    <div class="header-extras">
                        <div class="p-dropdown">
                            <a class="x"><span class="lines"></span></a>
                            <ul class="p-dropdown-content">
                                <li><a href="tel:+91-90815-45972"><i class="icon-phone-call"></i>+91-90815-45972</a>
                                </li>
                                <li><a href="mailto:info@nowofloan.com"><i
                                            class="icon-mail"></i>info@moneyupfinance.com</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <section class="profile-content">
            <div class="profile-image" style="background-color:#F4F6F9;">
                <div class="profile-name">
                    <h2>Attain Hassle-Free Personal Loan and Meet Your Monetary Needs</h2>
                    <ul class="list-icon list-icon-check list-icon-colored">
                        <li>Personal loan upto Rs. 15 lakhs in 30 minutes*</li>
                        <li>Get Instant personal loan offer multiple banks*</li>
                        <li>Low annual percentage rate starting from 12.5%*</li>
                    </ul>

                    <div class="line"></div>
                    <h4>Our Lending Bank Partners</h4>

                    <div class="carousel client-logos flickity-enabled is-draggable carousel-loaded" data-items="4"
                        data-dots="false" data-arrows="false">
                        <div class="flickity-viewport" style="height: 124.969px; touch-action: pan-y;">
                            <div class="flickity-slider"
                                style="left: 0px; transform: translateX(-101.2%); margin-right: -2.5px;">
                                <div class="polo-carousel-item"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 0%;"
                                    aria-hidden="true">
                                    <div><img src="https://nowofloan.com/assets/images/bank/006.png" alt="TATA Capital">
                                    </div>
                                </div>
                                <div class="polo-carousel-item"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 25.3%;"
                                    aria-hidden="true">
                                    <div><img src="https://nowofloan.com/assets/images/bank/015.png" alt="Faircent.com">
                                    </div>
                                </div>
                                <div class="polo-carousel-item"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 50.6%;"
                                    aria-hidden="true">
                                    <div><img src="https://nowofloan.com/assets/images/bank/016.png"
                                            alt="Fullertor India"></div>
                                </div>
                                <div class="polo-carousel-item"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 75.9%;"
                                    aria-hidden="true">
                                    <div><img src="https://nowofloan.com/assets/images/bank/023.png" alt="Lendingkart">
                                    </div>
                                </div>
                                <div class="polo-carousel-item is-selected"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 101.2%;">
                                    <div><img src="https://nowofloan.com/assets/images/bank/021.png" alt="Indifi"></div>
                                </div>
                                <div class="polo-carousel-item" aria-hidden="true"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 126.5%;">
                                    <div><img src="https://nowofloan.com/assets/images/bank/022.png" alt="Money View">
                                    </div>
                                </div>
                                <div class="polo-carousel-item" aria-hidden="true"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 151.8%;">
                                    <div><img src="https://nowofloan.com/assets/images/bank/024.png" alt="Moneytap">
                                    </div>
                                </div>
                                <div class="polo-carousel-item" aria-hidden="true"
                                    style="width: 209.949px; padding-right: 10px; position: absolute; left: 177.1%;">
                                    <div><img src="https://nowofloan.com/assets/images/bank/025.png"
                                            alt="IDFC First Bank"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="profile-bio">
                <section class="fullscreen p-t-40">
                    <div class="container m-0 p-10">

                        <div class="text-middle text-center">
                            <h5 class="p-b-10">Please enter the OTP you get on this number.</h5>

                            <form action="{{ route('login.verifyOtp') }}" id="submitForm2" class="col-md-12 col-sm-12 p-0"
                                novalidate="novalidate" method="post" accept-charset="utf-8">
                                <div class="form-group">
                                    <img src="https://nowofloan.com/assets/images/icons/mobile-otp.png" alt="Mobile OTP"
                                        class="m-b-20">
                                    <label class="text-dark" for="mobileno">Mobile No. :
                                        <strong id="my_mobile_no"></strong></label>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="otpcode" id="otpcode"
                                        class="form-control optnumber text-center" required="" maxlength="6"
                                        data-validation-regex-regex="[0-9]+">
                                    <div class="help-block font-small-3"></div>
                                </div>

                                <div class="p-countdown" data-delay="5">
                                    <div class="p-countdown-count" style="display: none;">
                                        <code>New OTP code will generate in <span class="count-number">1</span>
                                            Sec</code>
                                    </div>
                                    <div class="p-countdown-show" style="display: block;"><code>Don't received OTP? <a
                                                href="javascript:resendotp()">Resend OTP</a></code></div>
                                    <code id="resend-message"></code>
                                </div>

                                <div class="custom-error" id="otpcodeError"></div>

                                <div class="form-group m-b-0">
                                    <button type="submit" id="form-submit2"
                                        class="btn btn-block btn-secondary">VERIFY</button>
                                </div>
                            </form>
                        </div>

                        <div class="line"></div>

                        <div class="text-center">
                            <p class="text-theme">Already have an account?
                                <a href="{{ url('customer-login') }}"
                                    class="btn btn-outline btn-sm text-uppercase">Login</a>
                            </p>
                        </div>

                    </div>
                </section>

            </div>
        </section>

        <section class="border-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-12">
                        <h6 class="text-theme">Salaried Person Eligibility Criteria</h6>
                        <p><small>(1) Minimum salary Rs. 15,000/- per month (2) 1 year job stability (3) Minimum age 21
                                years</small></p>

                        <h6 class="text-theme">Self-Employed Person Eligibility Criteria</h6>
                        <p><small>(1) Minimum 1 year IT return (2) 1 year business stability (3) Minimum age 21
                                years</small></p>

                        <h6 class="text-theme">How to works?</h6>
                        <p><small>(1) Quick registration (2) Check eligibility (3) Buy membership card (4) Submit
                                documents (5) Bank Verification (6) Bank Sanction</small></p>

                        <h6 class="text-theme">Why moneyupfinance.com?</h6>
                        <p><small>(1) Per month 1000+ happy customers (2) Minimum documents - depend customer profile
                                (3) Compare to multiple banks - loan up to Rs. 15,00,000/-* (4) Annual percentage rate -
                                minimum 12.5% and maximum 24%* (5) Flexible repayment terms - 1 to 6 years (6)
                                Processing fees - depand on customer profile &amp; banks</small></p>

                        <h6 class="text-theme">Membership Card Benefits</h6>
                        <p><small>(1) Let you apply for personal loan in multiple banks (2) You get 10 years free loan
                                consultancy (3) Get 40% referral payout bonus (4) Get loan offers from multiple banks
                                anytime-anywhere (5) No effect on CIBIL of multiple bank verification (6) On-call
                                Assistance on all your doubts</small></p>

                        <div class="profile-bio-footer">
                            <h6 class="text-theme">Contact Us</h6>
                            <address>
                                <small>
                                    <strong>Registered Office Address:</strong>
                                    <br>130, Green Elina, 1<sup>st</sup> Floor, Anand Mahal Road, Adajan, Surat,
                                    Gujarat, India - 395009<br><i class="fa fa-gavel m-r-5"></i> CIN No.:
                                    U93090GJ2019PTC109257<br><i
                                        class="fa fa-phone-square m-r-5"></i>+91-90815-45972<br><i
                                        class="fa fa-envelope m-r-5"></i>info@moneyupfinance.com<br><i
                                        class="fa fa-clock m-r-5"></i>10 AM to 6 PM (Monday to Saturday) </small>
                            </address>
                        </div>
                    </div>

                    <div class="col-md-5 col-12">
                        <h5 class="text-theme">Overview Personal Loan</h5>

                        <p class="text-justify"><small>Money requirements can arise at any time and at any specific
                                instance of life. If you are planning for an exciting vacation at an enthralling
                                destination, or any of your family members is hit with a medical emergency, or you want
                                the dull interiors of your home to be renovated, or wedding bells are about to ring in
                                your home – the most convenient option to meet your money needs is by availing an
                                Instant Personal Loan from moneyupfinance.com. Partnered with multiple leading banks and
                                NBFCs, moneyupfinance facilitates you with easy and convenient personal loan offers with a
                                100% online process, that too utmost quickly!</small></p>

                        <ul class="text-dark">
                            <li><small>Get Personal Loan - up to ₹15 Lakhs</small></li>
                            <li><small>Reasonable Interest Rate starting at 12.5%</small></li>
                            <li><small>Processing Charge of 2%</small></li>
                        </ul>

                        <p class="text-justify"><small>Let's consider an example of an individual who is in pursuit of
                                a personal loan of ₹1 Lakh at the annual interest rate of 12.5% for a repayment tenure
                                of 6 years. As the lending firms charge the processing fees, this individual would have
                                to pay 2% of the loan amount as processing fees. So, the EMI that this individual would
                                have to pay is ₹1981 per month. Additionally, the loan insurance amount has to be paid;
                                this amount is usually 2% but it also depends on the applicant's age.</small></p>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer" style="z-index:10;">
            <div class="copyright-content background-dark">
                <div class="container">
                    <div class="copyright-text text-center text-light">
                        2022 © moneyupfinance Service India Pvt. Ltd. All rights reserved.
                    </div>
                </div>
            </div>
        </footer>


    </div>

    <a id="scrollTop" style="bottom: 16px; opacity: 0;"><i class="icon-chevron-up"></i><i
            class="icon-chevron-up"></i></a>

    @include('layouts.footerscript')


<script type="text/javascript">
    $token = '{{ csrf_token() }}';
    function resendotp() {
        $.ajax({
            url: '{{ route('login.sendOtp') }}',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $token
            },
            data: {
                fullname: localStorage.getItem("fullname"),
                mobile: localStorage.getItem("mobile"),
            },
            dataType: "JSON",
            cache: false,
            success: function(response) {
                if (response['success'] == true) {
                    $('#resend-message').html(response['message']);
                    $.notify({
                        message: response['message']
                    }, {
                        type: 'success'
                    });
                } else {
                    $.notify({
                        message: response['message']
                    }, {
                        type: 'danger'
                    });
                }
            },
            error: function(jXHR, textStatus, errorThrown) {
                $.notify({
                    message: errorThrown
                }, {
                    type: 'danger'
                });
            }
        });
    }

    $(function() {
        $('#my_mobile_no').text(localStorage.getItem("mobile"));

        $('#submitForm2').on('submit', function(e) {
            e.preventDefault();
            $('#resend-message').html('');
            $('#otpcodeError').html('');

            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $token
                },
                data: {
                    fullname: localStorage.getItem("fullname"),
                    mobile: localStorage.getItem("mobile"),
                    otp: document.getElementById('otpcode').value
                },
                dataType: "JSON",
                cache: false,
                beforeSend: function() {
                    $('#form-submit2').html(
                        '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> VERIFYING...'
                        );
                    $('#form-submit2').attr('disabled', true);
                },
                success: function(response) {
                    if (response['success'] == true) {
                        $.notify({
                            message: response['message']
                        }, {
                            type: 'success'
                        });
                        setTimeout(() => {
                            window.location.href = response['redirect_url'];
                        }, 1000);
                    } else {
                        $('#otpcodeError').html(response['message']);
                        $.notify({
                            message: response['message']
                        }, {
                            type: 'danger'
                        });
                    }

                    $('#form-submit2').html('VERIFY');
                    $('#form-submit2').attr('disabled', false);
                },
                error: function(jXHR, textStatus, errorThrown) {
                    $('#form-submit2').html('VERIFY');
                    $('#form-submit2').attr('disabled', false);
                    $.notify({
                        message: errorThrown
                    }, {
                        type: 'danger'
                    });
                }
            });
        });
    });
</script>
</body>

</html>
