<html class="loading" lang="en" data-textdirection="ltr">

@include('layouts.headerscript')

<body class="breakpoint-xl b--desktop">
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
        <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
            <div class="header-inner">
                <div class="container">

                    <div id="logo">
                        <a href="{{ url('/') }}">
                            <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                            <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                                    alt="moneyupfinance Loan" width="120"></span>
                        </a>
                    </div>

                    <div class="header-extras">
                        <div class="p-dropdown">
                            <a class="x"><span class="lines"></span></a>
                            <ul class="p-dropdown-content">
                                <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a>
                                </li>
                                <li><a href="mailto:info@moneyupfinance.com"><i
                                            class="icon-mail"></i>info@moneyupfinance.com</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <section class="profile-content">
            <div class="profile-image" style="background-color:#F4F6F9;">
                <div class="profile-name">
                    <h2>Attain Hassle-Free Personal Loan and Meet Your Monetary Needs</h2>
                    <ul class="list-icon list-icon-check list-icon-colored">
                        <li>Personal loan upto Rs. 15 lakhs in 30 minutes*</li>
                        <li>Get Instant personal loan offer multiple banks*</li>
                        <li>Low annual percentage rate starting from 12.5%*</li>
                    </ul>

                    <div class="line"></div>
                    <h4>Our Lending Bank Partners</h4>

                    <div class="carousel client-logos" data-items="4" data-dots="false" data-arrows="false">
                        <div><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></div><div><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></div><div><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></div><div><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></div><div><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></div><div><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></div><div><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></div><div><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></div>			</div>
                </div>
            </div>

            <div class="profile-bio">
                <section class="fullscreen p-t-40">
                    <div class="container m-0 p-10">

                        <div class="text-middle">
                            <h5 class="p-b-10">Select Your Profile &amp; Enter Details.</h5>

                            <form action="{{ route('process.loan.continue.post') }}" id="submitForm3"
                                class="col-md-12 col-sm-12 p-0" novalidate="novalidate" method="post"
                                accept-charset="utf-8">
                                @csrf
                                <div class="form-group">
                                    <label class="text-dark" for="username">Full name: <strong><span
                                                id="username">{{ auth()->user()->name }}</span></strong></label>
                                </div>

                                <div class="form-group">
                                    <label class="text-dark" for="usermobile">Mobile No.: <strong><span
                                                id="mobile">{{ auth()->user()->mobile }}</span></strong></label>
                                </div>

                                <div class="form-group btn-group-toggle" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-light active">
                                            <input type="radio" name="usertype" value="0" autocomplete="off" checked><i
                                                class="icon-briefcase"></i> Salaried Person
                                        </label>
                                        <label class="btn btn-light">
                                            <input type="radio" name="usertype" value="1" autocomplete="off"><i
                                                class="icon-flag"></i> Self Employed Person
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="text-dark" for="useremail">Email id</label>
                                    <input type="email" aria-required="true" name="useremail" id="useremail"
                                        class="form-control" placeholder="As per your bank records" required="">
                                    <div class="help-block font-small-3"></div>
                                    @error('useremail')
                                        <ul role="alert">
                                            <li>{{ $message }}</li>
                                        </ul>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="text-dark" for="loanamount">Required loan amount </label>
                                    <input type="text" aria-required="true" name="loanamount" id="loanamount"
                                        class="form-control" placeholder="As per your requirement" required=""
                                        min="10000" data-validation-regex-regex="[0-9]+">
                                    <div class="help-block font-small-3"></div>
                                    @error('loanamount')
                                        <ul role="alert">
                                            <li>{{ $message }}</li>
                                        </ul>
                                    @enderror
                                </div>

                                <div class="form-group m-b-0">
                                    <button type="submit" id="form-submit3"
                                        class="btn btn-block btn-secondary">PROCESS</button>
                                </div>
                            </form>
                        </div>

                        <div class="line"></div>

                        <div class="text-center">
                            <p class="text-theme">Already have an account?
                                <a href="digital/customer-login" class="btn btn-outline btn-sm text-uppercase">Login</a>
                            </p>
                        </div>

                    </div>
                </section>

            </div>
        </section>

        <section class="border-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-12">
                        <h6 class="text-theme">Salaried Person Eligibility Criteria</h6>
                        <p><small>(1) Minimum salary Rs. 15,000/- per month (2) 1 year job stability (3) Minimum age 21
                                years</small></p>

                        <h6 class="text-theme">Self-Employed Person Eligibility Criteria</h6>
                        <p><small>(1) Minimum 1 year IT return (2) 1 year business stability (3) Minimum age 21
                                years</small></p>

                        <h6 class="text-theme">How to works?</h6>
                        <p><small>(1) Quick registration (2) Check eligibility (3) Buy membership card (4) Submit
                                documents (5) Bank Verification (6) Bank Sanction</small></p>

                        <h6 class="text-theme">Why moneyupfinance.com?</h6>
                        <p><small>(1) Per month 1000+ happy customers (2) Minimum documents - depend customer profile
                                (3) Compare to multiple banks - loan up to Rs. 15,00,000/-* (4) Annual percentage rate -
                                minimum 12.5% and maximum 24%* (5) Flexible repayment terms - 1 to 6 years (6)
                                Processing fees - depand on customer profile &amp; banks</small></p>

                        <h6 class="text-theme">Membership Card Benefits</h6>
                        <p><small>(1) Let you apply for personal loan in multiple banks (2) You get 10 years free loan
                                consultancy (3) Get 40% referral payout bonus (4) Get loan offers from multiple banks
                                anytime-anywhere (5) No effect on CIBIL of multiple bank verification (6) On-call
                                Assistance on all your doubts</small></p>

                        <div class="profile-bio-footer">
                            <h6 class="text-theme">Contact Us</h6>
                            <address>
                                <small>
                                    <strong>Registered Office Address:</strong>
                                    <br>4001,<sup></sup> Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan,
                                    Surat, Gujarat. 395009<br><i class="fa fa-gavel m-r-5"></i> CIN No.:
                                    U31101GJ2016PTC813691<br><i class="fa fa-phone-square m-r-5"></i>+91
                                    97237-97077<br><i class="fa fa-envelope m-r-5"></i>info@moneyupfinance.com<br><i
                                        class="fa fa-clock m-r-5"></i>10 AM to 6 PM (Monday to Saturday) </small>
                            </address>
                        </div>
                    </div>

                    <div class="col-md-5 col-12">
                        <h5 class="text-theme">Overview Personal Loan</h5>

                        <p class="text-justify"><small>Money requirements can arise at any time and at any specific
                                instance of life. If you are planning for an exciting vacation at an enthralling
                                destination, or any of your family members is hit with a medical emergency, or you want
                                the dull interiors of your home to be renovated, or wedding bells are about to ring in
                                your home – the most convenient option to meet your money needs is by availing an
                                Instant Personal Loan from moneyupfinance.com. Partnered with multiple leading banks and
                                NBFCs, moneyupfinance facilitates you with easy and convenient personal loan offers with
                                a 100% online process, that too utmost quickly!</small></p>

                        <ul class="text-dark">
                            <li><small>Get Personal Loan - up to ₹15 Lakhs</small></li>
                            <li><small>Reasonable Interest Rate starting at 12.5%</small></li>
                            <li><small>Processing Charge of 2%</small></li>
                        </ul>

                        <p class="text-justify"><small>Let's consider an example of an individual who is in pursuit of
                                a personal loan of ₹1 Lakh at the annual interest rate of 12.5% for a repayment tenure
                                of 6 years. As the lending firms charge the processing fees, this individual would have
                                to pay 2% of the loan amount as processing fees. So, the EMI that this individual would
                                have to pay is ₹1981 per month. Additionally, the loan insurance amount has to be paid;
                                this amount is usually 2% but it also depends on the applicant's age.</small></p>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer" style="z-index:10;">
            <div class="copyright-content background-dark">
                <div class="container">
                    <div class="copyright-text text-center text-light">
                        2022 © moneyupfinance Service India Pvt. Ltd. All rights reserved.
                    </div>
                </div>
            </div>
        </footer>


    </div>

    <a id="scrollTop" style="bottom: 16px; opacity: 0;"><i class="icon-chevron-up"></i><i
            class="icon-chevron-up"></i></a>

    @include('layouts.footerscript')

    <script type="text/javascript">
    </script>
</body>

</html>
