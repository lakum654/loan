<html class="loading" lang="en" data-textdirection="ltr">
  @include('layouts.headerscript')
  <body class="breakpoint-xl b--desktop"><br>
    <div class="body-inner" style="animation-duration: 300ms; opacity: 1;">
      <header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
        <div class="header-inner">
          <div class="container">

            <div id="logo">
              <a href="{{ url('/') }}">
                <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
                <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan" width="120"></span>
              </a>
            </div>

            <div class="header-extras">
              <div class="p-dropdown">
                  <a class="x"><span class="lines"></span></a>
                  <ul class="p-dropdown-content">
                      <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a></li>
                      <li><a href="mailto:info@moneyupfinance.com"><i class="icon-mail"></i>info@moneyupfinance.com</a></li>
                  </ul>
              </div>
            </div>

          </div>
        </div>
      </header>
  <section class="background-theme bg-loaded" data-bg-image="https://nowofloan.com/assets/images/slider/digital-process-bg.jpg" style="background-image: url(&quot;https://nowofloan.com/assets/images/slider/digital-process-bg.jpg&quot;);">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 pb-4 text-center">
                  <h2 class="text-light">Digital Personal Loan Application Process</h2>
                  <p class="text-light m-b-30">Just a few more details to get instant loan offer</p>
              </div>
          </div>
      </div>
  </section>

  <section class="reservation-form-over no-padding">
      <div class="container m-b-40">
          <div class="row">
              <div class="col-lg-3 col-md-3 col-12 sm-hidden">
                  <div class="p-cb">
                      <div class="wizard clearfix" data-style="1" data-orientation="vertical" role="application">
                          <div class="steps clearfix m-0 p-r-0">
                              <ul role="tablist">
                                  <li role="tab" class="current"><a href="#"><span class="number">1</span><span class="title">Quick Registration</span></a></li>

                                  <li role="tab" class="current"><a href="#"><span class="number">2</span><span class="title">Check Eligibility</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">3</span><span class="title">Get Pre-Approval Offer</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">4</span><span class="title">Buy Membership Card</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">5</span><span class="title">Submit Documents</span></a></li>

                                  <li role="tab" class="disabled"><a href="#"><span class="number">6</span><span class="title">Get Sanctioned</span></a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

              <!-- START : CHECK ELIGIBLITY -->
              <form action="#" id="submitForm1" class="col-lg-9 col-md-9 col-12" novalidate="novalidate" method="POST" accept-charset="utf-8">
                  <div class="row p-cb">
                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="fullname"><strong>Name : <span id="username">{{ auth()->user()->name }}</span></strong> </h5>
                      </div>

                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="mobile"><strong>Mobile no. : <span id="mobile">{{ auth()->user()->mobile }}</span></strong> </h5>
                      </div>

                      <div class="form-group col-md-4">
                          <h5 class="text-dark" for="loanamount"><strong>Loan Amount :</strong> <span id="amount">{{ auth()->user()->loan_amount }}</span></h5>
                      </div>

                      <div class="line m-10 p-b-20"></div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="cibilscore">Cibil Score</label>
                          <select name="cibilscore" aria-required="true" id="cibilscore" class="form-control" required="">
                              <option value="">Select Score</option>
                              <option value="Below 650">Below 650</option>
                              <option value="650 - 700">650 - 700</option>
                              <option value="700 - 750">700 - 750</option>
                              <option value="750 - 800">750 - 800</option>
                              <option value="800 - 850">800 - 850</option>
                              <option value="850 - 900">850 - 900</option>
                          </select>
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="monincome">Monthly Income</label>
                          <input type="text" aria-required="true" name="monincome" id="monincome" class="form-control" required="" data-validation-regex-regex="[0-9]+">
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="monemi">Current Monthly EMI</label>
                          <input type="text" aria-required="true" name="monemi" id="monemi" class="form-control" required="" data-validation-regex-regex="[0-9]+">
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="loanpurpose">Loan Purpose</label>
                          <select name="loanpurpose" aria-required="true" id="loanpurpose" class="form-control" required="">
                              <option value="">Select Loan Purpose</option>
                              <option value="Personal Use">Personal Use</option>
                              <option value="Property Renovation">Property Renovation</option>
                              <option value="Marriage Purpose">Marriage Purpose</option>
                              <option value="Education Purpose">Education Purpose</option>
                              <option value="Medical Emergency">Medical Emergency</option>
                              <option value="Other">Other</option>
                          </select>
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="city">City</label>
                          <input type="text" aria-required="true" name="city" id="city" class="form-control" required="">
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-4">
                          <label class="text-dark" for="state">State</label>
                          <select name="state" aria-required="true" id="state" class="form-control" required="">
                              <option value="">Select State</option>
                              <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Ladakh">Ladakh</option><option value="Lakshadweep">Lakshadweep</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Odisha">Odisha</option><option value="Puducherry">Puducherry</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="Uttarakhand">Uttarakhand</option><option value="West Bengal">West Bengal</option>						</select>
                          <div class="help-block font-small-3"></div>
                      </div>

                      <div class="form-group col-md-12 text-center text-uppercase">
                          <button type="submit" id="form-submit1" class="btn btn-secondary">CHECK ELIGIBILITY</button>
                      </div>
                  </div>
              </form>			<!-- END : CHECK ELIGIBLITY -->

          </div>
      </div>
  </section>

  <footer id="footer">
      <div class="copyright-content background-dark">
          <div class="container">
            <div class="copyright-text text-center text-light">
                2022 © moneyupfinance Service India Pvt. Ltd. All rights reserved.
            </div>
          </div>
      </div>
  </footer>


    </div>

    <a id="scrollTop" style="bottom: 26px; opacity: 1; z-index: 199;"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

    @include('layouts.footerscript')

  <script type="text/javascript">
      $(function(){
          $('#submitForm1').on('submit', function(e) {
            e.preventDefault();
              $('#form-submit1').attr('disabled', true);
              $('#form-submit1').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> PROCESS...');

              let loanData = {
                cibilscore:$('#cibilscore').val(),
                monthlyincome:$('#monincome').val(),
                monthlyemi:$('#monemi').val(),
                loanpurpose:$('#loanpurpose').val(),
                city:$('#city').val(),
                state:$('#state').val(),
              }
              localStorage.setItem('loanData',JSON.stringify(loanData))


              setTimeout(() => {
                window.location.replace("preapproval");
              }, 2000);
          });


        const setData = () => {
            let data = JSON.parse(localStorage.getItem('loanData'));
                  if(!data.isEmpty) {
                    $('#cibilscore').val(data.cibilscore).change();
                    $('#monincome').val(data.monthlyincome)
                    $('#monemi').val(data.monthlyemi)
                    $('#loanpurpose').val(data.loanpurpose).change()
                    $('#city').val(data.city)
                    $('#state').val(data.state).change()
             }
        }

        setData();
      });
  </script>
  </body></html>
