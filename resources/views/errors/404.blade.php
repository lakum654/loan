@extends('layouts.master')

@section('main')

<section class="fullscreen" data-bg-image="https://nowofloan.com/assets/images/pages/bw-business-bg.jpg">
    <div class="container">
      <div class="container-fullscreen">
        <div class="row m-t-80">
          <div class="col-lg-6">
            <div class="page-error-404 text-secondary">404</div>
          </div>
          <div class="col-lg-6">
            <div class="text-left text-dark">
              <h1 class="text-medium">Ooops, This Page Could Not Be Found!</h1>
              <p class="lead">The page you are looking for might have been removed, or is temporarily unavailable.</p>
              <a href="{{ url('/') }}" class="btn btn-light btn-sm">Go to Homepage</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
