@extends('layouts.master')


@section('main')

<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
    <div class="slide"
        style="background-image:url(assets/images/slider/slider-background-1.jpg') }});">
        <div class="container">
            <div class="slide-captions row">
                <div class="col-lg-6 col-md-6 col-12 align-self-center">
                    <h2 class="text-dark">moneyupfinance - Faster and Simpler Way to get Loan Approvals</h2>
                    <p class="text-dark">Applying for a loan was never such easy</p>
                    <a href="{{ url('digital/personal-loan') }}"
                        class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply for Personal
                            Loan</span><i class="fa fa-arrow-right"></i></a>
                    <a href="{{ url('digital/personal-loan') }}"
                        class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply for Business
                            Loan</span><i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="{{ asset('public/assets/images/slider/model-1.png') }}"
                        alt="premium membership card" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="slide"
        style="background-image:url(assets/images/slider/slider-background-1.jpg') }});">
        <div class="container">
            <div class="slide-captions row">
                <div class="col-lg-6 col-md-6 col-12 align-self-center">
                    <h2 class="text-dark">Apply for a loan from multiple banks just by sitting on a couch
                    </h2>
                    <p class="text-dark">Just sit back and relax; We are on the run for your loans</p>
                    <a href="digital/personalLoan.html"
                        class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply for Personal
                            Loan</span><i class="fa fa-arrow-right"></i></a>
                    <a href="digital/businessLoan.html"
                        class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply for Business
                            Loan</span><i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="{{ asset('public/assets/images/slider/model-2.png') }}"
                        alt="platinum membership card" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>

<section class="">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-12">
                <div class="heading-text heading-line m-b-0">
                    <h4 class="text-medium font-weight-500">Loans from multiple banks on a single platform</h4>
                </div>

                <div class="text-justify">
                    <p>Having served 55,000+ customers and partners with specialized lending, moneyupfinance Service
                        India Private Limited is India's most trusted & reliable loan service provider, which
                        offers Business loans and Personal loans through membership. moneyupfinance.com is an online
                        platform that allows customers to apply for a loan through multiple banks and NBFCs by
                        purchasing a membership card - that too without impacting your CIBIL or credit score.
                        Also, our membership cardholder can apply for a free loan (without application charges)
                        through the validity of 10 years. We also have a Loan DSA (aka Channel Partner) program
                        where a user can refer us for a loan to earn commission on every successful purchase or
                        loan approval. In pursuit of transforming people's lives and bringing positive changes,
                        rapid response and transparency remain our primary goals. We aid in financial planning
                        and support our customers by providing the loan anytime, anywhere.</p>
                </div>
            </div>

            <div class="col-lg-5 col-md-5 col-12">
                <div class="row">
                    <div class="col-lg-6 text-center">
                        <div class="icon-box effect medium center process m-t-10 m-b-10 p-10 col-12">
                            <div><img src="{{ asset('public/assets/images/icons/customer.png') }}"
                                    alt="customers"></div>
                            <div class="counter small">
                                <span data-speed="1500" data-refresh-interval="150" data-to="55000"
                                    data-from="29490"></span>
                            </div>
                            <h6>SATISFIED CUSTOMERS</h6>
                        </div>
                    </div>

                    <div class="col-lg-6 text-center">
                        <div class="icon-box effect medium center process m-t-10 m-b-10 p-10 col-12">
                            <div><img src="{{ asset('public/assets/images/icons/partner.png') }}"
                                    alt="partners"></div>
                            <div class="counter small">
                                <span data-speed="1500" data-refresh-interval="50" data-to="949"
                                    data-from="481"></span>
                            </div>
                            <h6>SATISFIED PARTNERS</h6>
                        </div>
                    </div>

                    <div class="col-lg-6 text-center">
                        <div class="icon-box effect medium center process m-t-10 m-b-10 p-10 col-12">
                            <div><img src="{{ asset('public/assets/images/icons/bank.png') }}" alt="banks">
                            </div>
                            <div class="counter small">
                                <span data-speed="1500" data-refresh-interval="5" data-to="30"
                                    data-from="5"></span>
                            </div>
                            <h6>BANK PARTNERS</h6>
                        </div>
                    </div>

                    <div class="col-lg-6 text-center">
                        <div class="icon-box effect medium center process m-t-10 m-b-10 p-10 col-12">
                            <div><img src="{{ asset('public/assets/images/icons/earning.png') }}"
                                    alt="earning"></div>
                            <div class="counter small">
                                <span data-speed="1500" data-refresh-interval="1000" data-to="300000"
                                    data-from="56816"></span>
                            </div>
                            <h6>EARNING OPPORTUNITY</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="background-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-12">
                <div class="heading-text heading-line">
                    <h4 class="text-medium font-weight-500">Our Core Products</h4>
                </div>

                <div class="accordion accordion-simple">
                    <div class="ac-item ac-active">
                        <h3 class="ac-title"><i class="fa fa-credit-credit"></i> Premium Membership Card
                            to apply for Personal loan</h3>
                        <div class="ac-content">
                            <ul class="list-icon list-icon-arrow">
                                <li>Loans for salaried and self-employed</li>
                                <li>Instant loan offers up to INR 15 lakhs</li>
                                <li>Attractive Interest Rates</li>
                                <li>Quick approval within 30 minutes</li>
                            </ul>
                            <a href="{{ url('premium-membership-card') }}" class="btn btn-primary btn-sm">Apply Now</a>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h3 class="ac-title"><i class="fa fa-credit-credit"></i> Platinum Membership Card
                            to apply for Business loan</h3>
                        <div class="ac-content">
                            <ul class="list-icon list-icon-arrow">
                                <li>Loans for startups, business owners, and young entrepreneurs</li>
                                <li>Instant loan offers up to INR 1 cr without any collaterals</li>
                                <li>Flexible EMI options - Minimum Documents Required</li>
                                <li>Quick approval within 48 hours</li>
                            </ul>
                            <a href="{{ url('platinum-membership-card') }}" class="btn btn-primary btn-sm">Apply
                                Now</a>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h3 class="ac-title"><i class="fa fa-credit-credit"></i> Earn with US - Become
                            Loan DSA or Channel Partner</h3>
                        <div class="ac-content">
                            <ul class="list-icon list-icon-arrow">
                                <li>Earning opportunity up to INR 3,00,000/-</li>
                                <li>Lifetime career opportunity</li>
                                <li>Free marketing support</li>
                            </ul>
                            <a href="{{ url('channel-partner-code') }}" class="btn btn-primary btn-sm">Apply Now</a>
                        </div>
                    </div>
                </div>

                <hr />
                <h4 class="text-small font-weight-500 m-t-20">Whether you're a salaried person, self-employed,
                    or business owner; We help everyone for quick loan approval.</h4>
            </div>

            <div class="col-lg-5 col-md-5 col-12 text-center">
                <img src="{{ asset('public/assets/images/slider/model-3.png') }}" alt="our products"
                    class="img-fluid">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="heading-text heading-line text-center p-b-5">
            <h4 class="text-medium font-weight-500">All Our Membership Card Benefits @ ₹999</h4>
        </div>

        <div class="row icon-boxes">
            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-clock"></i>
                <div class="icon-box-content">
                    <h3>Save Your Time, Money & Effort</h3>
                    <p>You don’t need to waste time & money in visiting several banks for loans. Apply in
                        multiple banks easily @ just ₹999</p>
                </div>
            </div>

            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-calendar"></i>
                <div class="icon-box-content">
                    <h3>10 Years of Free Expert Consultancy</h3>
                    <p>We'll help you out in improving your loan application and suggest the best steps for easy
                        loan approval through on-call assistance</p>
                </div>
            </div>

            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-server"></i>
                <div class="icon-box-content">
                    <h3>Apply For Loans in Multiple Banks @ Single Platform</h3>
                    <p>Instead of getting 1 bank/agent support at a time, you get multiple banks' support with
                        us – letting you get loan offers from multiple banks</p>
                </div>
            </div>

            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-file-text"></i>
                <div class="icon-box-content">
                    <h3>No Effect On Your CIBIL Score</h3>
                    <p>We submit your loan application only to those banks whose loan eligibility criteria
                        matches your application, not impacting your CIBIL</p>
                </div>
            </div>

            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-grid"></i>
                <div class="icon-box-content">
                    <h3>Get Your Personalized Portal</h3>
                    <p>You can track your loan status and notifications easily while sitting at your home and
                        get timely updates</p>
                </div>
            </div>

            <div class="icon-boxx col-md-4 col-12">
                <i class="icon-percent"></i>
                <div class="icon-box-content">
                    <h3>Easy Refer & Earn Up To 40% Payout</h3>
                    <p>Start earning easy income through our refer and earn program. Get up to 40% commission
                        for every card sold through your referral link</p>
                </div>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-md-12 col-12 text-center m-t-20">
                <a href="{{ url('membership-card-benefits') }}" class="btn btn-primary btn-sm">Know Detailed
                    Benefits</a>
            </div>
        </div>
    </div>
</section>

<section class="background-grey">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading-text heading-line text-center">
                    <h4 class="text-medium font-weight-500">Our NBFC Bank Partners</h4>
                </div>

                <div class="carousel client-logos" data-items="5" data-arrows="true" data-dots="false">
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India">
                    </div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank">
                    </div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/026.png') }}" alt="Bajaj Finserv"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/028.png') }}" alt="Ziploan"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/030.png') }}" alt="Hero Fincorp"></div>
                    <div class="icon-box box-type effect center process"><img
                            src="{{ asset('public/assets/images/bank/031.png') }}" alt="Monexo"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="heading-text heading-line text-center p-b-10">
            <h4 class="text-medium font-weight-500">Our Customers Testimonials</h4>
        </div>

        <div class="carousel equalize testimonial testimonial-box" data-margin="20" data-arrows="false"
            data-items="3" data-items-sm="2" data-items-xxs="1" data-equalize-item=".testimonial-item">
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/abdul-khan.png') }}" alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="4.50"></div>
                <p>I'm much thankful for your quick support in getting an instant personal loan. I highly
                    recommend moneyupfinance.com for loans.</p>
                <span class="p-b-20">Abdul Kedar Khan</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/khushi-patel.png') }}" alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="5.00"></div>
                <p>moneyupfinance is one of the best trusted multiple bank loan service providers to assist with
                    loans through membership. They have provided me with a great earning opportunity to become a
                    Loan DSA. Also, they are providing great marketing support and that too for free. I'm
                    earning a great amount, and it is a wonderful experience. I deeply recommend moneyupfinance.com
                </p>
                <span class="p-b-20">Khushi Patel</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/chirag-kukadiya.png') }}"
                    alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="5.00"></div>
                <p>I really liked the concept and work, These guys are providing great assistance on how one can
                    get an approval for any loan. I'm happy to have your service.</p>
                <span class="p-b-20">Chirag Kukadiya</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/anil-bharti.png') }}" alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="5.00"></div>
                <p>Awesome experience in getting fastest loan approval. A great initiative to save applicant's
                    time and money. Also a government certified company, which helps people for fastest loan
                    approvals.</p>
                <span class="p-b-20">Anil Bharti</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/hemant-rathod.png') }}"
                    alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="4.00"></div>
                <p>I want to thank moneyupfinance.com, due to which I got quick approval for a personal loan within a
                    day.</p>
                <span class="p-b-20">Hemant Rathod</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/amrit-giri.jpg') }}" alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="5.00"></div>
                <p>moneyupfinance has been very helpful, They are professionals and the best thing about it is that
                    they are providing timely services.</p>
                <span class="p-b-20">Amrit Giri</span>
            </div>
            <div class="testimonial-item">
                <img src="{{ asset('public/assets/images/customers/manoj-makwana.png') }}"
                    alt="customer img">
                <div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true"
                    data-rateit-readonly="true" data-rateit-value="5.00"></div>
                <p>The processes on moneyupfinance.com are so quick, easy and simple to understand. Superfast
                    services with transparency. I'll surely recommend this to more of my friends and colleagues.
                </p>
                <span class="p-b-20">Manoj Makawana</span>
            </div>
        </div>
    </div>
</section>

@endsection
