@extends('layouts.master')


@section('main')
    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Frequently Asked Questions</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('faq') }}">
                            <span itemprop="name">FAQs</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section id="personalloan">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-text heading-line m-b-0 text-center">
                        <h4 class="text-medium font-weight-500">Personal Loan</h4>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">What is a Personal Loan?</h5>
                            <div class="ac-content">
                                <p>A personal loan is an unsecured loan, which means you don't need to pledge collateral to
                                    receive funds. You can use the money for any personal expense. You can easily apply for
                                    an online personal loan at senction-letter.com. senction-letter offers instant personal loans with
                                    paperless approval and quick disbursal. senction-letter provides loan offers from multiple
                                    banks.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Where can a Personal Loan be used?</h5>
                            <div class="ac-content">
                                <p>Personal Loans can be used for any personal expense, like Shopping, Home Renovation,
                                    Higher Education, Debt Consolidation, Tour Travel, Wedding, Medical emergency, etc.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What is the eligibility for a Personal Loan?</h5>
                            <div class="ac-content">
                                <p>The following people are eligible to apply for an Instant Personal Loan at senction-letter:</p>
                                <ul>
                                    <li>Individuals between 21 and 60 years of age.</li>
                                    <li>Individuals who have had a job for at least 1 year.</li>
                                    <li>Employees of private limited companies, employees from public sector undertakings,
                                        including central, state and local bodies.</li>
                                    <li>Those who earn a minimum of Rs. 15,000/- net income per month.</li>
                                </ul>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What is the EMI for Rs.1 Lakh Personal Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance offers personal loans at the lowest EMIs starting from Rs.1,982/- to Rs.
                                    8,999/-.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Which bank has the lowest interest rate for Personal Loans?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides personal loan offers through top multiple banks in India that offer
                                    Lowest Personal Loan Interest Rates. Loan approval is subjective to the applicant's
                                    documents.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How can I get a low-interest Personal Loan?</h5>
                            <div class="ac-content">
                                <p>Simply by becoming a moneyupfinance member. Get personalised consultation on getting loans at
                                    the lowest rates.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How to apply for a Personal Loan?</h5>
                            <div class="ac-content">
                                <ol>
                                    <li>Visit <a href="index.html" target="_self">https://moneyupfinance.com</a></li>
                                    <li>Quick Registration</li>
                                    <li>Click on Digital Personal Loan.</li>
                                    <li>Register yourself as per bank register name and bank registered mobile number.</li>
                                    <li>If you have already registered then click on the login option.</li>
                                    <li>If you want to calculate loan EMI details then click on the loan calculator. Then,
                                        click on the "Apply Now" button.</li>
                                </ol>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What credit score is required for a Personal Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides personal loan offers if your Credit score is 650 or higher.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What CIBIL Score is required for a Personal Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides a personal loan if your CIBIL Score is 650 or higher.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How long will it take for my Personal Loan to be processed?</h5>
                            <div class="ac-content">
                                <p>Once your application is submitted along with your documents, it can take anywhere
                                    between 1-7 days for your personal loan to get approved and a couple of days after that
                                    for the disbursement. moneyupfinance helps to get instant loan approvals.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">Can my CIBIL Score affect my loan sanction?</h5>
                            <div class="ac-content">
                                <p>As per CIBIL, credit score ranges from 300 to 900 and those with a score of at least 750
                                    points get faster loan approvals from moneyupfinance.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Can I claim tax benefits on personal loans?</h5>
                            <div class="ac-content">
                                <p>Yes, only if you use your personal loan amount for certain purposes like investing in a
                                    business, buying a residential property, investing in other assets, etc.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Can I apply for a personal loan online?</h5>
                            <div class="ac-content">
                                <p>Yes, with moneyupfinance, you can easily apply for an online personal loan with no paperwork
                                    needed and minimal documents.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Can I take a personal loan to repay my education loan?</h5>
                            <div class="ac-content">
                                <p>A personal loan can be taken to effectively pay back the educational loan or any sort of
                                    loan or debt.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How to get a personal loan with the lowest interest rates?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance gives you personal loan offers from multiple banks and NBFCs with the best and
                                    attractive interest rates.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What are 3 things banks consider when giving loans?</h5>
                            <div class="ac-content">
                                <ul>
                                    <li>CIBIL Score</li>
                                    <li>Income Proof & Stability</li>
                                    <li>Age of the loan applicant</li>
                                </ul>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Where would I get an unsecured personal loan for a small business
                                easily?</h5>
                            <div class="ac-content">
                                <p>The best option would be getting a loan easily from moneyupfinance.com with a 100% Online
                                    Process. moneyupfinance gives the best and hassle-free loan experience.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Where can I get the best personal loans in India?</h5>
                            <div class="ac-content">
                                <p>Through moneyupfinance's industry-best loan services, you can get a personal loan from
                                    multiple banks and NBFCs at a single platform, that too in just 30 minutes.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Which bank offers personal loans to the persons even with a very low
                                salary?</h5>
                            <div class="ac-content">
                                <p>Even if you have a salary as low as Rs.15000 per month, moneyupfinance will provide you
                                    instant personal loan offers from multiple banks and NBFCs – 100% Online Loan Process.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="background-grey" id="businessloan">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-text heading-line m-b-0 text-center">
                        <h4 class="text-medium font-weight-500">Business Loan</h4>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">What is a Business Loan?</h5>
                            <div class="ac-content">
                                <p>A business loan is an unsecured credit you can avail to meet your urgent business
                                    requirements. Business loans allow you to usher in funds for your enterprise to expand
                                    your business. You can apply for an online business loan at moneyupfinance.com. moneyupfinance
                                    offers instant Business Loans with paperless approval and quick disbursal. moneyupfinance
                                    provides loans to customers via banks only.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Where can a Business Loan be used?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides Business Loans within 48 hours, you can use that money for any
                                    business expense like business expansion, boost production, buying new machinery, etc.
                                </p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What is the eligibility for a Business Loan?</h5>
                            <div class="ac-content">
                                <p>The following people are eligible to apply for an Instant Business Loan at moneyupfinance:</p>
                                <ul>
                                    <li>Age should be between 21 to 65 years.</li>
                                    <li>CIBIL score must be 700 or more.</li>
                                    <li>The candidate should own a business at least profitable for three successive
                                        financial years.</li>
                                    <li>The business turnover must display an upward trend.</li>
                                    <li>Your balance sheet must be audited by a registered Chartered Accountant (CA).</li>
                                </ul>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What is the EMI for Rs.1 Lakh Business Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance offers business loans at the lowest EMIs starting from Rs.1,929/- to
                                    Rs.8,999/-.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Which bank has the lowest interest rate for Business Loans?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides business loans through top banks in India that offer the lowest
                                    Business Loan Interest Rates. Loan approval is subjective to the applicant's documents.
                                </p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How can I get a low-interest Business Loan?</h5>
                            <div class="ac-content">
                                <p>Simply by becoming a moneyupfinance member. Get personalised consultation on getting loans at
                                    the lowest rates.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">How to apply for a Business Loan?</h5>
                            <div class="ac-content">
                                <ol>
                                    <li>Visit <a href="index.html" target="_self">moneyupfinance.com</a></li>
                                    <li>Quick Registration</li>
                                    <li>Click on Digital Business Loan.</li>
                                    <li>Register with your bank registered name and mobile number.</li>
                                    <li>If you have already registered then click on the login option.</li>
                                    <li>If you want to calculate Business loan EMI details then click on the loan
                                        calculator. Then, click on the "Apply Now" button.</li>
                                </ol>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What credit score is required for a Business Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides business loan offers if your Credit score is 700 or higher.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What CIBIL Score is required for a Business Loan?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance provides business loan offers if your CIBIL score is 700 or higher.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How long will it take for my Business Loan to be processed?</h5>
                            <div class="ac-content">
                                <p>Once your application is submitted along with your documents, it can take anywhere
                                    between 1-7 days for your business loan to get approved and a couple of days after that
                                    for the disbursement. moneyupfinance helps to get instant loan approvals.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Can my CIBIL Score affect my loan sanction?</h5>
                            <div class="ac-content">
                                <p>As per CIBIL, credit score ranges from 300 to 900 and those with a score of at least 750
                                    points, get faster loan approvals from moneyupfinance.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What are the documents required for Instant Business Loan?</h5>
                            <div class="ac-content">
                                <p>Following documents are required for availing a business loan:</p>
                                <ul>
                                    <li>Business proof</li>
                                    <li>KYC documents of the company</li>
                                    <li>KYC documents of the business owners</li>
                                    <li>Photo Identity Proof (Aadhar card/ Driving license/ Voter ID/ Passport)</li>
                                    <li>Last six months company bank statements</li>
                                    <li>GST Certificate</li>
                                    <li>Last two years Income Tax Returns</li>
                                    <li>Last two years Balance sheet and Profit & Loss accounts</li>
                                    <li>A report with detailed information about how the candidate will utilise the business
                                        loan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="channelpartner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-text heading-line m-b-0 text-center">
                        <h4 class="text-medium font-weight-500">Channel Partner</h4>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">What is the advantage of being a moneyupfinance Channel Partner?</h5>
                            <div class="ac-content">
                                <p>With a fantastic earning opportunity with very low investment, moneyupfinance.com provides a
                                    personal dashboard to their Channel Partners where they can get Commission Report,
                                    Sharing Report, Payout Report, Profile Information, etc.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How can we join the moneyupfinance Channel Partner Program?</h5>
                            <div class="ac-content">
                                <p>Go to moneyupfinance.com and visit the Channel Partner section. Then, apply for joining the
                                    program.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Why should I join moneyupfinance Channel Partner Program?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance.com is a rapidly growing company and its Channel Partner Program is the most
                                    rewarding B2B affiliate program in India. As a moneyupfinance.com Channel Partner, you not
                                    only get to earn unbeatable commissions but also you can offer a delightful and
                                    convenient instant personal loan or instant business loan experience to your customers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">How much can I earn with the Channel Partner Program?</h5>
                            <div class="ac-content">
                                <p>With the moneyupfinance.com Channel Partner Program, you get paid on a sales conversion basis,
                                    depending on how many transactions your referrals complete with moneyupfinance.com. The
                                    better they do, the more you earn. There is no cap on how much you can earn.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How can I track my earnings?</h5>
                            <div class="ac-content">
                                <p>You can instantly configure and manage your customers as well as track your earnings
                                    directly from your Channel Partner dashboard on moneyupfinance.com.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">Who can join moneyupfinance Channel Partner Program?</h5>
                            <div class="ac-content">
                                <p>If you're aged between 20 and 60 years, you can join the moneyupfinance Channel Partner
                                    Program. Apart from this, there are no other criteria.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-grey" id="membershipcard">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-text heading-line m-b-0 text-center">
                        <h4 class="text-medium font-weight-500">Membership Card</h4>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">What is moneyupfinance Membership Card?</h5>
                            <div class="ac-content">
                                <p>A Membership Card is an identification of an individual as a member of moneyupfinance.com. The
                                    card entitles a member to use moneyupfinance's portal for loan purposes.</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">How can I buy a Membership Card?</h5>
                            <div class="ac-content">
                                <p>Just after a quick registration, you can choose the convenient Membership Card and
                                    purchase it.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="accordion white accordion-shadow">
                        <div class="ac-item">
                            <h5 class="ac-title">Which Membership Card should I buy?</h5>
                            <div class="ac-content">
                                <p>moneyupfinance.com offers two types of Membership Cards:
                                <p>
                                <p>Premium Membership Card for Personal Loans</p>
                                <p>Platinum Membership Card for Business Loans</p>
                            </div>
                        </div>

                        <div class="ac-item">
                            <h5 class="ac-title">What are the benefits of buying a Membership Card?</h5>
                            <div class="ac-content">
                                <ul>
                                    <li>Get Pre-Approved Loan Offers from Multiple Banks from one platform.</li>
                                    <li>Free Expert Consultancy for 10 Years.</li>
                                    <li>Up to 40% referral payout per membership card.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
