@extends('layouts.master')

@section('main')
    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Privacy Policy</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('privacy-policy') }}">
                            <span itemprop="name">Privacy Policy</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section id="section-privacy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <p>As a user of the website owned by MONEYUPFINANCE, your privacy is important to us. This Privacy
                                Policy discusses the information we collect about you, how we treat it, with whom we share
                                it, and how we protect it.</p>

                            <p>MONEYUPFINANCE.COM is our company&#39;s domain name/brand name and MONEYUPFINANCE SERVICE INDIA PVT
                                LIMITED is our company&#39;s name.</p>

                            <p>In the normal course of business, we collect personal information about you from a variety of
                                sources, including:</p>

                            <ul>
                                <li>Information from you, such as applications or other forms (which includes your name,
                                    address, marital status, employment, assets and income); and</li>
                                <li>Information about you, your accounts, and your holdings and transactions that we receive
                                    from you or others, such as account custodians, brokers, and other financial services
                                    firms, banks, etc.</li>
                            </ul>

                            <p>We&#39;re also serious about protecting our users by addressing potential privacy concerns.
                                Our privacy guidelines apply to all users across the world. This policy applies to all
                                information, in whatever form, relating to MONEYUPFINANCE&#39;s business activities worldwide,
                                and to all information handled by MONEYUPFINANCE, relating to other organizations with whom it
                                deals. It also covers all IT and information communications facilities operated by MONEYUPFINANCE
                                or on its behalf.</p>

                            <p>This Privacy Policy covers the security, information, IT equipment and use of MONEYUPFINANCE SERVICE
                                INDIA Pvt. Ltd., a company incorporated under the laws, presently in force in India and
                                having its registered office at 130, Green Elina, 1st Floor, Anand Mahal Road, Adajan,
                                Surat-395009, Gujarat, India and all its affiliates. It also includes the use of email,
                                internet, voice and mobile IT equipment. This policy applies to all MONEYUPFINANCE Users,
                                Clients, employees, channel partners and associate partners (hereafter referred to as
                                &lsquo;individuals&#39;).</p>

                            <p>Subject to arbitration, only the courts and tribunals of Surat, Gujarat, shall have exclusive
                                jurisdiction with respect to any suit, action or any other proceedings arising out of or in
                                relation to the Loan Documents. Nothing contained in this clause shall limit any right of
                                the Lender to commence any legal action or proceedings arising in relation to the Loan or
                                the Loan Documents in any other court, tribunal or another appropriate forum, competent
                                jurisdiction and the Borrower and/or the Guarantor hereby consent to that jurisdiction.</p>

                            <p><strong>How We Manage and Protect Your Personal Information:</strong></p>

                            <p>We do not sell information about current or former clients to third parties. We may disclose
                                your personal information as necessary to:</p>

                            <ul>
                                <li>Effect, administer, or enforce a transaction that you request or authorize;</li>
                                <li>Process or service a financial product or service that you request or authorize; or</li>
                                <li>Maintain or service your account with us or with another entity.</li>
                            </ul>

                            <p>We may also disclose personal information for everyday business purposes to organizations or
                                firms who provide consulting, technology or other services for us and agree to maintain its
                                confidentiality; others, such as attorneys, trustees, family members, or others who are
                                authorized to represent you, your estate, or a joint or co-owner of your account; regulatory
                                agencies; or as we are otherwise permitted or required by law or process of law.</p>

                            <p>We restrict access to your personal information to our employees and to permitted
                                third-parties who need to know that information to provide products or services for us, or
                                to provide, process, or maintain any security, account, or investment product, service or
                                program for you or your benefit. To protect your personal information from unauthorized
                                access and use, we have adopted administrative, technical, and physical security procedures
                                that comply with the Laws in India. These measures include computer safeguards and secured
                                files and buildings.</p>

                            <p><strong>What we can do with your personal information:</strong></p>

                            <p>We may use your personal information that we collect, or that is provided to us, for the
                                following purposes:</p>

                            <ul>
                                <li>Considering any application for an account or service;</li>
                                <li>Carrying out our business functions and activities;</li>
                                <li>Collecting amounts you owe us, including taking enforcement action;</li>
                                <li>Exercising our rights and fulfilling our obligations under any agreement with you;</li>
                                <li>Exercising our rights and fulfilling our obligations for the purposes of complying with
                                    all applicable laws, including those relating to money laundering, terrorist financing,
                                    bribery, corruption, tax evasion, fraud and similar; and managing all economic and trade
                                    sanction risks;</li>
                                <li>Generally administering and monitoring services provided to you (or any related entity);
                                    and</li>
                                <li>Providing you with information about our other services, or the services of selected
                                    third parties in which we think you may have an interest, including by post, telephone
                                    and electronic message &ndash; you can opt-out of receiving information about our other
                                    services and/or the services of selected third parties by informing us in writing.</li>
                            </ul>

                            <p><strong>Sharing of Personal Information with Third Parties</strong></p>

                            <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable
                                information. This does not include trusted third parties who assist us in operating our
                                website, conducting our business, or servicing you, so long as those parties agree to keep
                                this information confidential. We may also release your information when we believe release
                                is appropriate to comply with the law, enforce our site policies, or protect our or
                                others&#39; rights, property, or safety. However, non-personally identifiable visitor
                                information may be provided to other parties for marketing, advertising, or other uses.</p>

                            <p><strong>Security and Confidentiality</strong></p>

                            <p>The security of your personal information is important to us. We generally follow
                                industry-standard information security tools and measures, as well as internal procedures
                                and strict guidelines to prevent information submitted to us, both during transmission and
                                once we receive it from misuse and data leakage. No method of transmission over the
                                internet, or method of electronic storage, is 100% secure, however. Therefore, while we
                                strive to use commercially acceptable means to protect your personal information, which
                                considerably reduces the risks of data misuse, we cannot guarantee its absolute security. To
                                notify the Company about any security vulnerability or potential data breach, please contact
                                us at: info@moneyupfinance.com and we will take the appropriate measures to address such an
                                incident, as deemed necessary.</p>

                            <p>Our employees and channel partners can access the information on a &quot;need-to-know&quot;
                                basis and are subject to confidentiality obligations.</p>

                            <p><strong>DATA ACCURACY</strong></p>

                            <p>Personal Data must be accurate and, where necessary, kept up to date. It must be corrected or
                                deleted without delay when inaccurate. It is advisable that you ensure that the Personal
                                Data we use and hold is accurate, complete, kept up to date and relevant to the purpose for
                                which we collected it. You must check the accuracy of any Personal Data at the point of
                                collection and at regular intervals afterwards. You must take all reasonable steps to
                                destroy or amend inaccurate or out-of-date Personal Data.</p>

                            <p><strong>LIMIT OF LIABILITY</strong></p>

                            <p>We shall not be liable for any confusion caused as a result of any of your actions or
                                omission of any action, anything as a result of your viewing, reading or listening of any
                                content. Although we will do our best to provide constant, uninterrupted access to our
                                website, we accept no responsibility or liability for any interruption or delay.</p>

                            <p>In no event will our total liability to you for all damages arising from your use of the
                                service or information, materials or products included on or otherwise made available to you
                                through the service exceed the amount you paid for the service related to your claim.</p>

                            <p>We have no liability for any loss, damage or misappropriation of your files under any
                                circumstances or for any consequences related to changes, restrictions, suspension or
                                termination of your service or the agreement. These liabilities shall apply to you even if
                                their remedies shall fail their essential purpose.</p>

                            <p><strong>USAGE OF ADVERTISING ID</strong></p>

                            <p>When you are using our application that incorporates our Services, we may also automatically
                                record your Google and/or any other Advertising ID (if you are using an Android device) or
                                your Advertising Identifier (IDFA - if you are using an IOS device; together with the Google
                                and/or any other Advertising ID-&quot;Mobile Advertising IDs&quot;), for advertising or
                                analytics purposes. The said Advertising ID is an anonymous identifier, provided by Google.
                                If your device has an Advertising ID, we may collect and use it for advertising and user
                                analytics purposes. If your device does not have an Advertising ID, we may use other
                                persistent identifiers. The information collected may also be stored on your device. You can
                                reset your mobile Advertising ID or opt-out of receiving targeted ads through your mobile
                                Advertising IDs which is provided in our settings.</p>

                            <p><strong>COMPLIANCE &amp; COOPERATION WITH REGULATORS</strong></p>

                            <p>We regularly review this Privacy Policy and make sure that we process your personal
                                information in ways that comply with regulations currently in force in India. We firmly
                                comply with legal frameworks including data protection laws relating to the transfer of
                                data.</p>

                            <p><strong>CONSENT</strong></p>

                            <p>By using our website, you consent to our website&#39;s Privacy Policy. The usage of the
                                website shall be construed as an acceptance of the Privacy Policy.</p>

                            <p><strong>GRIEVANCES</strong></p>

                            <p>For any complaints and/or inquiries, you can send us formal written inquiries or complaints
                                at info@moneyupfinance.com. All inquiries and/or complaints shall be examined and will be
                                resolved expeditiously. Our team of experts will respond by contacting the person who made
                                such inquiries and/or complaints. We work with the appropriate regulatory authorities,
                                including local data protection authorities, to resolve any complaints regarding the
                                transfer of your data that we cannot resolve with you directly.</p>

                            <p><strong>MODIFICATION OF THE POLICY</strong></p>

                            <p>We reserve the right to modify this Privacy Policy at our own independent decision at any
                                time. If the changes are significant, the Company shall spare no efforts to apprise its
                                clientele and provide a prominent notice (including, for certain services, email
                                notification of Privacy Policy changes). It is pertinent to remember that it shall be the
                                Clients&#39; responsibility to read the Policy as amended every once in a while.</p>

                            <p><strong>USAGE OF COOKIES/COOKIES POLICY</strong></p>

                            <p>Cookies are small files that a site or its service provider transfers to your computer&#39;s
                                hard drive through your web browser with your permission which enables the site or service
                                provider&#39;s systems to recognize your browser and capture and remember certain
                                information. We use cookies to help us understand and save your preferences for future
                                visits, keep track of advertisements and compile aggregate data about site traffic and site
                                interaction so that we can offer better site experiences and tools in the future.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
