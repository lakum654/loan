@extends('layouts.master')


@section('main')

<section class="p-t-130 p-b-70" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/channel-page.jpg">
	<div class="container">
		<div class="page-title text-center m-b-30">
			<h1 class="text-uppercase text-medium">moneyupfinance - Business Opportunities @ Zero Investment</h1>
			<span>Become Our Channel Partner & Earn Up To Rs 3 Lakhs/month</span>
		</div>
	</div>
</section>

<section class="reservation-form-over no-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-sm-12 center">

				<div class="card">
					<div class="card-body">

					<form action="https://nowofloan.com/partner/sendotpCode" id="submitForm1" class="form-transparent-grey" novalidate="novalidate" method="post" accept-charset="utf-8">
						<div class="row">
							<div class="col-lg-12 text-center m-b-10">
								<h3>Join Channel Partner Program</h3>
								<p>Sell our membership card earn up to 60% pay out per membership card.</p>
							</div>

							<div class="col-lg-6 col-md-6 col-12 form-group">
								<label class="sr-only">First Name</label>
								<input type="text" name="firstname" id="firstname" placeholder="First Name" class="form-control" aria-required="true" required data-validation-regex-regex="^[a-zA-Z ]*$">
								<div class="help-block font-small-3"></div>
							</div>

							<div class="col-lg-6 col-md-6 col-12 form-group">
								<label class="sr-only">Last Name</label>
								<input type="text" name="lastname" id="lastname" placeholder="Last Name" class="form-control" aria-required="true" required data-validation-regex-regex="^[a-zA-Z ]*$">
								<div class="help-block font-small-3"></div>
							</div>

							<div class="col-lg-6 col-md-6 col-12 form-group">
								<label class="sr-only">Email Id</label>
								<input type="email" name="emailid" id="emailid" placeholder="Email Id" class="form-control" aria-required="true" required>
								<div class="help-block font-small-3"></div>
							</div>

							<div class="col-lg-6 col-md-6 col-12 form-group">
								<label class="sr-only">Mobile</label>
								<input type="text" name="mobileno" id="mobileno" placeholder="Mobile" class="form-control" aria-required="true" required maxlength="10" data-validation-regex-regex="^[6789]\d{9}$">
								<div class="help-block font-small-3"></div>
							</div>

							<div class="col-lg-12 col-md-12 col-12 form-group m-b-0">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" aria-required="true" name="conditions" id="conditions" class="custom-control-input" value="1" required>
									<label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the <a href="../terms-conditions.html" target="_blank">Terms of Use</a> and <a href="../privacy-policy.html" target="_blank">Privacy Policy</a> of moneyupfinance.com</small></label>
									<div class="help-block font-small-3"></div>
								</div>
							</div>

							<div class="col-lg-12 col-md-12 col-12 text-center custom-error" id="mobilenoError"></div>

							<div class="col-lg-12 col-md-12 col-12 text-center">
								<button type="submit" id="form-submit1" class="btn btn-secondary">Apply New Registration</button>
							</div>
						</div>
					</form>
						<div class="text-center">
							<hr/>
							<p class="m-t-10 m-b-0"><small>Already have an account? <a href="{{ url('channel-login') }}">Sign in</a></small></p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>


<section class="p-t-10">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 center text-center border">
				<h5 class="text-danger">NOTE</h5>
				<p class="text-danger"><small>Our company is not MLM. So Company's product sale by channel partner, base on that company will provide payout up to 60% per membership Card. If channel partner will not selling any product then company would not be provide any payout to them.</small></p>
			</div>

			<div class="col-lg-12 m-t-20">
				<p>Channel partner is a program for people who wants to earn extra income while doing their regular day to day work. We give commission to channel partner on every loan file that he/she brings. It is a referral program for people who have good network of people wanting loan, but have no connection to any NBFC bank. They join our Channel Partner program and can upload files of other people so that loans can be processed for people with less knowledge or connectivity.</p>
				<p><strong>Below is an example on how our Channel Partner program works:</strong> Rajesh is a money lender doing unorganized and unsecured money lending to locals around him, but people are more acknowledged and are unwilling to take loans from Rajesh and want to take loan from banks but don't know the proper procedure, so they ask Rajesh to process their loans through a bank. Rajesh doesn't have a DSA or any code from NBFC Banks so he can become our channel partner and can work under our company through his channel partner code and earn.</p>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">Eligibility criteria</h4>
		</div>
		<div class="row p-t-20">
			<div class="col-lg-4">
				<div class="icon-box effect small border">
					<div class="icon text-center"><h2><i></i>1</h2></div>
					<h3>Minimum 6 months business experience</h3>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="icon-box effect small border">
					<div class="icon text-center"><h2><i></i>2</h2></div>
					<h3>Must follow company rules and regulations</h3>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="icon-box effect small border">
					<div class="icon text-center"><h2><i></i>3</h2></div>
					<h3>Minimum qualification 10th pass</h3>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">Fast & Easy Application Process</h4>
		</div>
		<div class="row">
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">01</h3>
				<h4>Quick registration</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">02</h3>
				<h4>Submit application form</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">03</h3>
				<h4>Free partner code</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">04</h3>
				<h4>Start business</h4>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">Channel Partner Benefits</h4>
		</div>
		<div class="row p-t-20">
			<div class="col-md-3 col-sm-12">
				<div class="icon-box effect center process border-top-blue w-100">
					<h3>High return, Zero investment</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="icon-box effect center process border-top-pink w-100">
					<h3>Up to 3 Lakhs earning opportunity</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="icon-box effect center process border-top-khaki w-100">
					<h3>Life time career opportunity</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="icon-box effect center process border-top-purple w-100">
					<h3>We provide marketing support</h3>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center p-b-10">
			<h4 class="text-medium font-weight-500">Our Channel Partner Testimonials</h4>
		</div>

		<div class="carousel equalize testimonial testimonial-box" data-margin="20" data-arrows="false" data-items="3" data-items-sm="2" data-items-xxs="1" data-equalize-item=".testimonial-item">
							<div class="testimonial-item">
					<img src="{{ asset('public/assets/images/customers/suresh-lalvani.jpg') }}" alt="customer img">
					<div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
					<p>I am able to fulfill my luxuries one by one, all thanks to extra earning created by moneyupfinance.</p>
					<span class="p-b-20">Suresh Lalvani</span>
				</div>
							<div class="testimonial-item">
					<img src="{{ asset('public/assets/images/customers/satendra-singh.jpg') }}" alt="customer img">
					<div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="5.00"></div>
					<p>Channel Partner program has raised my confidence and made me believe that sky is the limit.</p>
					<span class="p-b-20">Satendra Singh</span>
				</div>
							<div class="testimonial-item">
					<img src="{{ asset('public/assets/images/customers/shahzad-mohd.jpg') }}" alt="customer img">
					<div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.00"></div>
					<p>All my extra expenses are not a burden anymore, thank you for creating extra earning for me.</p>
					<span class="p-b-20">Shahzad Mohd.</span>
				</div>
							<div class="testimonial-item">
					<img src="{{ asset('public/assets/images/customers/dhaval-bhatt.jpg') }}" alt="customer img">
					<div class="rateit" data-rateit-mode="font" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="4.50"></div>
					<p>I am able to earn while I do my regular job, thank you moneyupfinance for giving me this opportunity.</p>
					<span class="p-b-20">Dhaval Bhatt</span>
				</div>
					</div>
	</div>
</section>


<div class="line"></div>

<section id="section-about" class="p-t-10 p-b-20">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <h4 class="text-uppercase">About Company</h4>
        <p>Having served 37,000+ customers and partners with specialized lending, moneyupfinance Service India Private Limited is India's most trusted & reliable loan service provider, which offers Business loans and Personal loans through membership. We have curated a website that allows customers to apply for a loan through multiple banks and NBFCs by purchasing a membership card - that too without impacting your CIBIL or credit score. Also, our membership cardholder can apply for a free loan (without application charges) through the validity of 10 years. We also have a Loan DSA (aka Channel Partner) program where a user can refer us for a loan to earn commission on every successful purchase or loan approval. In pursuit of transforming people's lives and bringing positive changes, rapid response and transparency remain our primary goals. We aid in financial planning and support our customers by providing the loan anytime, anywhere.</p>
      </div>

      <div class="col-md-4 col-sm-12">
        <h4 class="text-uppercase">Contact Us</h4>
        <address>
          <strong>Registered Office Address:</strong>
          <br/>130, Green Elina, 1<sup>st</sup> Floor, Anand Mahal Road, Adajan, Surat, Gujarat, India - 395009<br/><i class='fa fa-gavel m-r-5'></i> CIN No.: U93090GJ2019PTC109257<br/><i class='fa fa-phone-square m-r-5'></i>+91 97237-97077<br/><i class='fa fa-envelope m-r-5'></i>info@moneyupfinance.com<br/><i class='fa fa-envelope m-r-5'></i>partner@moneyupfinance.com<br/><i class='fa fa-clock m-r-5'></i>10 AM to 6 PM (Monday to Saturday)        </address>

        <div class="social-icons social-icons-colored social-icons-rounded float-left">
                <ul>
                  <li class="social-google"><a href="https://www.google.co.in/search?sxsrf=ALeKk01u89nlIqutRFpx7mZQsrbzWPNjSQ%3A1597652951978&amp;source=hp&amp;ei=1z86X4bcOaeD4-EPldqlmAY&amp;q=nowofloan&amp;gs_ssp=eJzj4tVP1zc0TDcoq8xIykgyYLRSNagwTko1MEkxs0i1sDAwMjQwtzKoME0xNzY0tzRONLM0MjEwT_TizMsvz0_LyU_MAwBQxBJt&amp;oq=n&amp;gs_lcp=CgZwc3ktYWIQARgAMg0ILhDHARCvARAnEJMCMgQIIxAnMgQIIxAnMgUIABCRAjIFCAAQkQIyCAgAELEDEIMBMgUIABCxAzIICAAQsQMQgwEyCAgAELEDEIMBMggIABCxAxCDAVDxBVjxBWDuDGgAcAB4AYAB2gWIAbYHkgEHMi0xLjYtMZgBAKABAaoBB2d3cy13aXqwAQA&amp;sclient=psy-ab" target="_blank" rel="nofollow"><i class="fab fa-google-plus-g"></i></a></li>

                  <li class="social-facebook"><a href="https://www.facebook.com/nowofloan.in/" target="_blank" rel="nofollow"><i class="fab fa-facebook-f"></i></a></li>

                  <li class="social-instagram"><a href="https://www.instagram.com/nowofloan.in/" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a></li>

                  <li class="social-twitter"><a href="https://twitter.com/nowofloan/" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a></li>

                  <li class="social-linkedin"><a href="https://www.linkedin.com/company/nowofloan/" target="_blank" rel="nofollow"><i class="fab fa-linkedin"></i></a></li>

                  <li class="social-pinterest"><a href="https://in.pinterest.com/nowofloan/" target="_blank" rel="nofollow"><i class="fab fa-pinterest"></i></a></li>

                  <li class="social-youtube"><a href="https://www.youtube.com/channel/UC-YuwjTXkBlhazMqhqS-m2w" target="_blank" rel="nofollow"><i class="fab fa-youtube"></i></a></li>
                </ul>
              </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modalotp" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center">
				<img src="../assets/images/icons/mobile-otp.png" alt="Mobile OTP" class="m-t-20" />
				<h5 class="text-center text-uppercase" id="modal-label">Mobile</h5>
				<p class="text-dark m-b-10">Please enter the OTP you get on this number:</p>
				<h4 id="modelotpmobileno"></h4>

				<form action="https://nowofloan.com/#" id="submitForm2" class="text-center" novalidate="novalidate" method="post" accept-charset="utf-8">
					<input type="hidden" name="mobileno" id="otpmobileno" value="">
					<input type="hidden" name="firstname" id="otpfirstname" value="">
					<input type="hidden" name="lastname" id="otplastname" value="">
					<input type="hidden" name="emailid" id="otpemailid" value="">

					<div class="form-group">
						<input type="text" name="otpcode" id="otpcode" class="form-control text-center optnumber" required maxlength="4" data-validation-regex-regex="[0-9]+">
						<div class="help-block font-small-3"></div>
					</div>

					<div class="p-countdown" data-delay="5">
						<div class="p-countdown-count">
							<code>New OTP code will generate in <span class="count-number"></span> Sec</code>
						</div>
						<div class="p-countdown-show"><code>Don't received OTP? <a href="javascript:resendotp()">Resend OTP</a></code></div>
						<code id="resend-message"></code>
					</div>

					<div class="custom-error" id="otpcodeError"></div>

					<div class="form-group">
						<button type="button" id="btn-verify" class="btn btn-secondary">VERIFY NOW</button>
						 <a href="channel.html" class="btn btn-light">CANCEL</a>
					</div>
				</form>			</div>
		</div>
	</div>
</div>

@endsection
