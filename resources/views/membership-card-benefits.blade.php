@extends('layouts.master')

@section('title','Apply for Instant Personal Loan Online approvals | moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Membership Card Benefits</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('membership-card-benefits') }}">
			    <span itemprop="name">Membership Card Benefits</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
	</div>
</section>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-12">
				<div class="heading-text heading-line">
					<h2 class="text-medium font-weight-500">How Our Membership Cards Go Beyond Giving You Greater Value-For-Money</h2>
				</div>

				<p><span class="badge badge-dark">EXAMPLE</span></p>

				<p>Let's consider examples of two persons to cite out the differences between the tedious traditional loan process and the modern-day loan process through the moneyupfinance Membership Card.</p>

				<p>Rahul and Rohan are two individuals who are facing a money crunch and are badly in need of financial help.</p>

				<p>Rahul follows the traditional loan process – without knowing with which bank his credit profile (comprising CIBIL Score, etc.) would match, he is exerting himself by making several visits to different banks and going through various iterations of documents submission. Due to the lack of clarity, Rahul is going through an extremely time-consuming process.</p>

				<p>On the other hand, Rohan quickly visits the moneyupfinance online portal, does the quick registration, fills in some basic details, buys the moneyupfinance Membership Card, and receives instant pre-approved loan offers from the banks in which his credit profile matches. This innovative loan service offered by the membership card has made Rohan's loan process super-easy and smooth.</p>

				<p><strong>Just like Rohan, you can get all the below-mentioned benefits at a one-time payment of just Rs.999</strong></p>
			</div>

			<div class="line"></div>

			<div class="col-md-12 col-12 m-b-20">
				<h3>Save Your Time, Money & Effort</h3>

				<p>Let's say, your monthly income is Rs.100000. Now, if you go by the traditional way of the loan process, you will be needing to take one day off from your work to make a visit to a single bank. Imagine making around 5 bank visits like this and losing out almost Rs.15000 per month. But with a moneyupfinance Membership Card, you need not make any bank visit – you get loan offers at the comfort of your home @ just Rs.999.</p>


				<h3>Get Your Personalized Portal</h3>

				<p>In a traditional loan scenario, if your loan application is in process and you want to check the status, you will need to visit and call the banks constantly and get an update. But with our membership card @ just Rs.999, you get your personalized portal where all the updates can be seen by you without going anywhere.</p>


				<h3>10 Years of Free Expert Consultancy</h3>

				<p>Once you get your membership card, you get 10 years of free expert consultancy and on-call assistance where you will be guided by experts on how to improve your loan profile to maximize the chances of loan approval. We will also suggest only those banks for loans whose eligibility criteria will be met by your profile.</p>


				<h3>Apply For Loans in Multiple Banks @ Single Platform</h3>

				<p>Going by the traditional method, when you apply for a loan in a bank, you will only get that bank/agent's support – and if the loan is rejected, you will do the same process in other banks. But with our membership card, you can easily apply for a loan in multiple banks and NBFCs without stepping out of your house – this will increase the chances of loan approval.</p>


				<h3>No Effect On Your CIBIL Score</h3>

				<p>If you apply for a loan the traditional way, for every loan application, your CIBIL score will be decreased by 20 points. But with our membership card @ just Rs.999, your CIBIL won't be affected at all even after multiple loan applications and bank verifications. Our membership card is a great way to maintain your CIBIL score and improve your creditworthiness.</p>


				<h3>Easy Refer & Earn Up To 40% Payout</h3>

				<p>Whatever your profile is – be it a housewife or a working professional – you can generate an extra income through our easy refer and earn program. When you purchase our membership card @ just Rs.999, you will get the unique referral link that you can send to your network of people/loan seekers and you can earn a commission of up to 40% with every membership card sold through your referral link.</p>
			</div>

			<div class="col-md-12 col-12">
				<div class="table-responsive">
					<table class="table table-bordered nobottommargin">
						<thead>
							<tr>
								<th><i class="icon-check text-success"></i> Our Membership Card Benefits (All Solutions Just @ ₹999)</th>
								<th><i class="icon-x text-danger"></i> Loan Seekers Problems With Traditional Loan Process</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><i class="icon-check text-success"></i> Get 100% Online Loan At The Comfort Of Your Home</td>
								<td><i class="icon-x text-danger"></i> More Time & Money Consumption Due To Visiting Multiple Banks</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> Get Your Personalized Portal For Easy Loan Tracking & Solutions</td>
								<td><i class="icon-x text-danger"></i> No Personalized Portal Given For Loan Tracking</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> Loan Application Submitted To Multiple Banks & NBFCs At One Platform</td>
								<td><i class="icon-x text-danger"></i> Only 1 Loan Application Submitted At A Time</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> Get 10 Years Of Free Expert Consultation/On-Call Assistance</td>
								<td><i class="icon-x text-danger"></i> No Free Consultation & Improvement Suggestions Provided</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> We Provide Multiple Banks Support At Single Platform</td>
								<td><i class="icon-x text-danger"></i> You Get Only 1 Bank/Agent Support At A Time</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> Absolutely No Effect On Your CIBIL Score Even After Multiple Loan Application</td>
								<td><i class="icon-x text-danger"></i> Your CIBIL Score Decreases By 20 Points With Every Loan Application</td>
							</tr>

							<tr>
								<td><i class="icon-check text-success"></i> You Can Earn Up To 40% Commission Through Our Easy Refer & Earn Program</td>
								<td><i class="icon-x text-danger"></i> No Scope For You To Earn Extra Money</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
</section>

@endsection
