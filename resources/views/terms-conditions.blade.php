@extends('layouts.master')



@section('main')
    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Terms &amp; Conditions</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('terms-conditions') }}">
                            <span itemprop="name">Terms &amp; Conditions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section id="section-terms">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <p>Here are the terms and conditions for Customers, Channel Partners, Associate
                                Partners,&nbsp;Employees, and every user of our website - www.moneyupfinance.com. So, the terms
                                and conditions are applied as per your role. You must read all the below-mentioned Terms
                                &amp; Conditions carefully.</p>

                            <p>The Company wishes to offer the services under the terms and conditions set forth and the
                                user/customer/channel partner wishes to be associated unconditionally with these terms and
                                conditions.</p>

                            <p>Therefore, in consideration of the agreements contained in this, the parties, intending to be
                                legally bound, agree to the correctness and authenticity of the following details given to
                                the company:</p>

                            <ul>
                                <li>Information from you, such as applications or other forms (which includes your name,
                                    address, marital status, employment, assets and income); and</li>
                                <li>Information about you, your accounts, and your holdings and transactions that we receive
                                    from you or others, such as account custodians, brokers, and other financial services
                                    firms, banks, etc.</li>
                            </ul>

                            <p>If the company by any source finds out anyone bad-mouthing or defaming the company&#39;s
                                reputation or company&#39;s members then strict legal action will be taken against the
                                individual or group.</p>

                            <p>We&#39;re also serious about protecting our users by addressing potential privacy concerns.
                                Our terms and condition guidelines apply to all users across the world. These terms and
                                conditions apply to all information, in whatever form, relating to moneyupfinance&#39;s business
                                activities worldwide, and to all information handled by moneyupfinance, relating to other
                                organizations with whom it deals. It also covers all IT and information communications
                                facilities operated by moneyupfinance or on its behalf.</p>

                            <p><strong>MEMBERSHIP CARD TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>The payment of Membership Card fees is non-refundable under any circumstances.</li>
                                <li>&quot;moneyupfinance.com&quot; Membership Card is not transferable and is only valid up to
                                    its date of expiry (valid up to 10 years) and the card may not be used by any person
                                    other than the card holder.</li>
                                <li>The &quot;moneyupfinance.com&quot; Membership Card is not a payment card or credit card.</li>
                                <li>Renewal terms and conditions are at the discretion of &quot;moneyupfinance.com&quot;.</li>
                                <li>The Membership Card can only be used on/for our website.</li>
                            </ol>

                            <p><strong>CUSTOMER TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>The payment of Membership Card fees is non-refundable under any circumstances.</li>
                                <li>The company only takes the cost of the Membership Card. No other tip of service is
                                    charged.</li>
                                <li>The membership card is not an ATM, DEBIT, or CREDIT CARD. Customers can use that card
                                    only for loan purposes with given benefits. Also, buying a Membership Card lets you
                                    apply for a loan and it doesn&rsquo;t guarantee loan approval as the final loan approval
                                    depends on the banks and the customer profile. If the loan is rejected, you can still
                                    avail other benefits of the Membership Card.</li>
                                <li>If a customer is viewing any advertisement/promotional content of the company and then
                                    approaching the company thinking that he/she will get the loan approval based on the
                                    advertisement, then it must be noted that loan application will only be submitted once
                                    the customer buys moneyupfinance&rsquo;s Membership Card. Even after buying the membership
                                    card, the final loan approval depends on the bank(s) and customer profile. If the
                                    customer&rsquo;s profile doesn&rsquo;t match loan eligibility criteria, he/she
                                    won&rsquo;t be able to get a loan. Still, they can avail other benefits of the
                                    membership card.</li>
                                <li>Membership Cards can be used only by the persons who have purchased them and not by any
                                    other person(s), source or third party.</li>
                                <li>If customer reference does payment through customer own referral link which is provided
                                    by the company and if that shows in customer&#39;s portal then the only company will
                                    give the reference payout of that customer.</li>
                                <li>If the customer loan is approved in our company and he/she denies that loan approval
                                    then also card payment would not be refundable.</li>
                                <li>The documents, cheques, and OTP that the company&#39;s employee asks the customer is for
                                    the processing of the loan only and the company never misuses them. Just for security,
                                    after completing the loan process, the customer can go to the concerned bank and cancel
                                    their cheque. The company is not responsible if any problems/disputes arise in the
                                    future.</li>
                                <li>If you do not give the OTP,&nbsp;documents, or document query for verification to the
                                    company&#39;s employee for the loan process, then your file will be rejected. (According
                                    to the criteria, if your file matches without OTP, your loan will be processed).</li>
                                <li>If our company&#39;s executive asks you for any payment transaction OTP, do not provide
                                    it. If the customer pays any charges other than the charge of the Membership Card, the
                                    company won&rsquo;t be responsible for the same.</li>
                                <li>If a customer has any queries regarding the loan process then he/she would have to
                                    contact that department where their files are in process.</li>
                                <li>We will verify your documents in multiple banks; so whatever documents are submitted by
                                    customers in our company that will match with any bank criteria, in that bank only we
                                    will proceed with the loan process. (For example, if your file will match in 2 banks
                                    then our company&#39;s login department will log in your document only on that 2 banks.
                                    The verification is done by the company&#39;s employee and&nbsp;there is no proof
                                    available for the same.</li>
                                <li>The document will be verified by the company in multiple banks. If your documents match
                                    the criteria of the bank, then the login process will be done in that bank. If your
                                    documents do not match the criteria of a bank, the company will give you a solution. You
                                    can take the solution and reapply after 6 months - and this will be shown on the
                                    customer portal.</li>
                                <li>It is not fixed that the customer file will be logged in only in the banks listed on the
                                    company website. It may be logged in/verified in other banks also, depending on the
                                    customer file.</li>
                                <li>Our company is not taking extra charges other than membership card charges. If any
                                    third-party charges you then our company is not responsible for that.</li>
                                <li>There are no processing or file charges for customer loan approval. There is only one
                                    charge and that&#39;s only for the Membership Card - valid for 10 years.</li>
                                <li>Our company will log in customer files as per their requirements. (Example: If customer
                                    requirement is INR 1 lakh and if some bank criteria is up to INR 50,000 then we will not
                                    log in their file in that bank).</li>
                                <li>Wherever the customer file is logged in by the company, these details will not be given
                                    to any customer in written or digital form.</li>
                                <li>Loan offers and pre-approval loans process depend&nbsp;only on the bank&#39;s rules and
                                    that type of loan is given only on customer behaviour. So, there is so much difference
                                    between that type of process and the company&#39;s process. If that loan is rejected in
                                    our company but gets approved by another company/source then the customer can&#39;t
                                    blame our company.</li>
                                <li>Loan approval depends on your profile so if your documents are perfect and as per the
                                    bank criteria then you will get a loan through our company.</li>
                                <li>The loan information is only given to that person who has applied for the loan.</li>
                                <li>During the loan processing time, if any customer would not be in contact with us for 3
                                    days, then that file will be rejected by our company.</li>
                                <li>If your file is rejected in our company, then the customer has to make sure that they
                                    have to re-submit their documents, with the implemented company-suggested solution, in
                                    our company after 6 months.</li>
                                <li>The company is not responsible if the customer loan is rejected by any queries.</li>
                                <li>If the customer will apply for the first time but his/her loan is rejected in our
                                    company then the company will give them reason and solution for that. So at re-applying
                                    time if the customer will not resubmit a file with the solution implemented&nbsp;then
                                    the file will be again rejected in our company for the same reason. Still, the final
                                    loan approval will depend on the customer profile and the bank&#39;s criteria and rules
                                    &amp; regulations.</li>
                                <li>The company will provide only the reason for rejection to the customer and it would not
                                    be provided in the form of hard or soft copy - it will only be shown in the customer
                                    portal. Some banks only provide general reasons, they don&#39;t give us any specific
                                    reason so the customer should not complain about that.</li>
                                <li>The customer has to give correct information about their CIBIL SCORE and PROFILE. If the
                                    customer gives wrong information, then the company will not be responsible for loan
                                    rejection.</li>
                                <li>The company will not be providing any CIBIL REPORT&nbsp;in digital or hard copy to any
                                    customer in any situation.</li>
                                <li>Bank charges are applicable as per banks&#39; rules and regulations.</li>
                                <li>The company will take legal action against the customer who submitted fake documents.
                                    And the company won&rsquo;t take any responsibility for the loan process in this case.
                                </li>
                                <li>After the customer&rsquo;s file is logged in, the customer has to contact only the login
                                    department and coordinate with them &ndash; not any telecaller or other department of
                                    the company. The further process has to be done according to the login department.</li>
                                <li>During the loan process if the rules of any bank change, then we have to follow those
                                    new rules.</li>
                                <li>The customer has to give their registered phone number for being contacted by the login
                                    department.</li>
                                <li>During the loan processing time, if the company gets any queries and it is not solving
                                    that in the given time, then the company has the authority to take more time to address
                                    the query. So, the customer must not complain about the same.</li>
                                <li>If the customer wants to reapply in our company after file rejection or approval, then
                                    he/she has to re-submit their documents in the customer portal.</li>
                                <li>When you are applying for a loan on our website, we are showing you only your
                                    Eligibility for the loan. So whatever details you enter on the website are accepted by
                                    software only, and that only shows your pre-approval and not your final loan approval.
                                    Approval only depends on your documents and the banks&#39; rules and regulations. We are
                                    not giving you any guarantee for the final loan approval.</li>
                                <li>The Membership Card&nbsp;shown on our websites during the processing time is
                                    for&nbsp;demo purposes only. So, it&#39;s not a real or physical membership card. In
                                    that, whatever details are entered by the customer are accepted by the software and the
                                    system shows you the pre-approval depending on the customer details. The customer will
                                    get the&nbsp;card after completing the entire process&nbsp;on our website.</li>
                                <li>All the detailed criteria, terms, and information behind any of the company&rsquo;s
                                    concise promotional content (social media ads, banners, SMS, advertisements, emails,
                                    etc.) are stated in the Terms &amp; Conditions and Privacy Policy sections of the
                                    website. Any concerned person (customer, channel partner, employee, etc.) must check and
                                    accept all the rules and regulations before availing any of the company&rsquo;s
                                    services.</li>
                                <li>The customer&#39;s payment is executed by third-party payment sources. So whenever
                                    payment would be received by the company then only membership cards or codes will be
                                    generated. If a customer&#39;s payment would be debited from his/her account but we
                                    don&#39;t receive any payment in the company&#39;s account then the company will not be
                                    responsible for that.</li>
                                <li>For any reference customer&#39;s payout, their account verification is compulsory. After
                                    verification, if the payout amount is debited from the company&rsquo;s account and if it
                                    does not credit/reflect in the reference customer&rsquo;s account &ndash; the company
                                    won&rsquo;t be responsible for this issue.</li>
                                <li>Our company is a private limited company and we are tied up with banks and corporate
                                    DSA. We are providing loans through banks only.</li>
                                <li>Multiple partnered banks&#39; logos are shown on our website and our promotional content
                                    across many mediums &ndash; these are shown only for our company&#39;s marketing
                                    purpose. It might be possible that certain banks, whose logos are shown on our
                                    website/promotional content, are not partnered with our company. Also, these should not
                                    be assumed as any bank&#39;s advertisement.</li>
                                <li>If anyone takes any legal action against the company then only our legal advisor would
                                    be dealing with that and Surat, Gujarat will only remain the junction for any legal
                                    procedure. No one would be able to contact any employee or director of our company.</li>
                                <li>A vocal statement would not be acceptable. Only the signed agreements would be
                                    acceptable for any customer. The company&rsquo;s terms and conditions, privacy policies,
                                    and all other rules are to be unconditionally accepted by every concerned person.</li>
                                <li>If any person has a doubt/question regarding any of the company&rsquo;s terms and
                                    conditions, they can contact the company.</li>
                                <li>The eligibility age for buying a membership card is 18 - 62 years. The persons in this
                                    age bracket can avail benefits of the membership card.</li>
                                <li>If a partner would not submit their agreement to the company within 30 days then all
                                    payout&nbsp;would be cancelled.</li>
                                <li>Every reference payout will have a deduction of 5% TDS.</li>
                                <li>For the loan process, the company will only coordinate with the person who has purchased
                                    the membership card and has an ongoing loan process &ndash; the company won&rsquo;t
                                    coordinate with any third party.</li>
                                <li>Every bank payout will have tax deductions as per the bank&rsquo;s rules and
                                    regulations.</li>
                                <li>The Company&#39;s authorized person can change any rules and regulations at any time;
                                    the concerned person must be regularly updated with the company&rsquo;s terms and
                                    conditions and has to accept them unconditionally.</li>
                                <li>The company will provide the appropriate loan services but the responsibility of
                                    customer handling will be of the reference customer.</li>
                                <li>If any bank&#39;s rules or company&#39;s rules are changing during the processing time
                                    of the loan then the customer has to follow those new rules.</li>
                                <li>The office holidays and bank holidays will not be counted as working days/business days.
                                    The company&rsquo;s office work will be done on working days only.</li>
                            </ol>

                            <p><strong>CHANNEL PARTNER TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>Channel Partner code charges are not refundable. It&#39;s the charge for a lifetime
                                    business opportunity (card validation is 10 years).</li>
                                <li>Channel Partner will get payout up to 60% per Membership card only.</li>
                                <li>Payout will be given to the channel partner as per the rules and regulations of the
                                    company. But for payout, the partner must have their invoice generated and all the
                                    payout documents submitted to the company. If there are&nbsp;no invoices or submission
                                    of payout documents, then the company will not be able to provide any payout.</li>
                                <li>If a channel partner&#39;s customer does payment through the channel partner&#39;s
                                    reference link which is provided by the company and if that is shown in the channel
                                    partner&#39;s portal, only then the company will give the payout of that customer.</li>
                                <li>If the channel partner is giving a file of his/her customers in our company for loan
                                    purposes, there are some criteria so the channel partner has to give a file based on
                                    that criteria. The channel partner and channel partner&#39;s customer have to agree with
                                    the decision of the login department about that file.</li>
                                <li>The channel partner&#39;s customer would not be able to visit our company. It&#39;s the
                                    partner&#39;s responsibility to take care of it.&nbsp;</li>
                                <li>Loan processing time might get delayed because of any public holiday, technical
                                    problems, customer issues, etc.</li>
                                <li>The company will take legal action against the channel partner who submitted fake
                                    documents and can charge a penalty. And the company won&rsquo;t take any responsibility
                                    for the loan process in this case.</li>
                                <li>The Customer loan approval only depends on the customer profile and the company is not
                                    giving any guarantee for that.</li>
                                <li>The Company&#39;s authorized person can change any rules and regulations at any time;
                                    the concerned person must be regularly updated with the company&rsquo;s terms and
                                    conditions and has to accept them unconditionally.</li>
                                <li>If any partner will misbehave with the company or customer - or break any of the
                                    company&#39;s rules -&nbsp;then the company will cancel their code.</li>
                                <li>The Company will not be providing any CIBIL REPORT in digital or hard copy to any
                                    customer in any situation.</li>
                                <li>The Company will not be providing any proof for rejection in hard or soft copy.</li>
                                <li>The Customer profile will be logged in where his/her file matches as per the
                                    customer&#39;s requirement. (Example: If customer requirement is INR 1 lakh and if some
                                    bank criteria is up to INR 50,000 then we will not log in their file in that bank.</li>
                                <li>If any third party will ask us for any details of the loan then the company will not be
                                    providing them with any information.</li>
                                <li>If any channel partner will do work against the rules and regulations of our company
                                    then the company will cancel their code.</li>
                                <li>If any bank&#39;s rules or company&#39;s rules are changing during the processing time
                                    of the loan then the channel partner&nbsp;has to follow those new rules.</li>
                                <li>Login department decision is final for the loan process so the customer would not be
                                    able to argue with our company.</li>
                                <li>Payout on approval will be given to the customer when the bank will provide us. But if
                                    that file would be approved through the channel partner&#39;s code then only the company
                                    will provide payout with the deduction of GST and SERVICE TAX.</li>
                                <li>The business done by the channel partner with their customer or the marketing done by
                                    the channel partner is the partner&#39;s responsibility only &ndash; in that, the
                                    company will not take any responsibility.</li>
                                <li>The Company will not take any responsibility for any fake commitments given by any
                                    channel partner to the customers.</li>
                                <li>Channel partner&#39;s customer does their payment through a third-party payment source.
                                    So whenever payment would be received by the company then only the membership card or
                                    code will be generated. If the customer&#39;s payment would be debited from his/her
                                    account but we don&#39;t receive any payment in the company&#39;s account then the
                                    company will not be responsible for that.</li>
                                <li>For any channel partner&#39;s payout, their account verification is compulsory. After
                                    verification, if the payout amount is debited from the company&rsquo;s account and if it
                                    does not credit/reflect in the channel partner&rsquo;s account &ndash; the company
                                    won&rsquo;t be responsible for this issue.</li>
                                <li>The Partner has to submit all required documents (KYC Documents) compulsorily&nbsp;on
                                    the Channel Partner Portal.</li>
                                <li>If the company, for any reason, gives a refund to the channel partner&rsquo;s customer
                                    &ndash; then that amount will be deducted from the channel partner&rsquo;s next payout.
                                </li>
                                <li>If the channel partner would work on/undertake the ad campaign and because of any
                                    technical problem the website or portal would stop then it&#39;s not the company&#39;s
                                    responsibility. In that case, the partner has to keep updated with the company and
                                    co-operate with that technical glitch. Because of this problem if there would be any
                                    loss in partner business then it&#39;s not the company&#39;s responsibility.</li>
                                <li>If customer payment is on hold during the processing time for any reason, then the
                                    channel partner&#39;s payout of that customer would be done after receiving that
                                    customer payment.</li>
                                <li>If a partner would not submit their agreement to the company within 30 days then all
                                    payout would be cancelled.</li>
                                <li>If the channel partner would not work actively for a period of 3 months then their code
                                    will be deactivated.</li>
                                <li>Whatever documents are submitted by partners are secure in our company. If documents
                                    will be misused by any other sources in future then our company is not responsible for
                                    that.</li>
                                <li>Customer terms and conditions are also applicable for channel partners&#39; customers.
                                </li>
                                <li>If anyone takes any legal action against the company then only our legal advisor would
                                    be dealing with that and Surat, Gujarat remains the only junction for any legal
                                    procedure. No one will be able to contact any employee or director of our company.</li>
                                <li>A vocal statement would not be acceptable. Only the signed agreements would be
                                    acceptable for any channel partner. The company&rsquo;s terms and conditions, privacy
                                    policies, and all other rules are to be unconditionally accepted by every concerned
                                    person.</li>
                                <li>If any person has a doubt/question regarding any of the company&rsquo;s terms and
                                    conditions, they can contact the company.</li>
                                <li>Our promotional content may communicate messages like &#39;Earn up to ₹3
                                    Lakhs/month&#39; or &#39;Chance to earn ₹3 Lakhs&#39;, but the actual earnings done by
                                    the Channel Partner depend on the volume of membership cards sold to the customers
                                    through Channel Partner&#39;s referrals. If any customer through Channel Partner&#39;s
                                    reference buys the company&#39;s membership card, then only the payout will be generated
                                    for the concerned Channel Partner.</li>
                                <li>Every reference payout will have a deduction of 5% TDS.</li>
                                <li>If a channel partner&rsquo;s customer&rsquo;s loan will be processed under/with the code
                                    by the bank, then only the payout will be given. Otherwise, the payout shall not be
                                    given.</li>
                                <li>The company will provide the appropriate loan services but the responsibility of
                                    customer handling will be of the channel partner.</li>
                                <li>Every bank payout will have tax deductions as per the bank&rsquo;s rules and
                                    regulations.</li>
                                <li>The office holidays and bank holidays will not be counted as working days/business days.
                                    The company&rsquo;s office work will be done on working days only.</li>
                            </ol>

                            <p><strong>REFERENCE TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>If any person will do an online process for a loan in our company then the membership
                                    card payment of that reference person will not be refundable.</li>
                                <li>Reference payout would be given to them as per rules and regulations of our company.
                                </li>
                                <li>Our company will give payout only on membership cards; it does not depend on reference
                                    customer&#39;s loan approval or rejection.</li>
                                <li>Whether the customer loan will be approved or not&nbsp;depends on the customer profile
                                    and the company does not give any guarantee for that.</li>
                                <li>If customers are giving a reference in our company for loan purposes, for that we have
                                    some criteria. The customer has to give a reference based on that criteria. The company
                                    is not giving you any type of guarantee for the loan approval in any situation at any
                                    cost so customers who give a reference have to agree with the decision of that
                                    file&#39;s login department.</li>
                                <li>The company will take legal action against the reference partner/customer who submitted
                                    fake documents and can charge a penalty. And the company won&rsquo;t take any
                                    responsibility for the loan process in this case.</li>
                                <li>The Customer&#39;s terms and conditions are also applicable for the reference
                                    person&#39;s customers.</li>
                                <li>Reference customer&#39;s payment is done through third-party payment sources. So
                                    whenever payment would be received by the company then only the membership card or code
                                    will be generated. If the customer&#39;s payment is debited from his/her account but the
                                    company doesn&#39;t receive any payment in the company&#39;s account then the company
                                    will not be responsible for any queries.</li>
                                <li>During loan offers, if any reference person will do the online loan process then the
                                    company will give the payout up to 40% per membership card to the reference person. But
                                    before that, invoice generation is most important for any payout process.</li>
                                <li>If the customer of reference will do an online process then the customer&#39;s payout
                                    will be given to the referral partner. But during processing time, if the company would
                                    refund that amount to the customer for any reason, then that customer&#39;s payout will
                                    be cut out from the reference person&#39;s next payout.</li>
                                <li>Whatever documents are submitted by a reference person, they will be secure in our
                                    company. If documents will be misused by any other sources in future then our company is
                                    not responsible for that.</li>
                                <li>If a partner/reference person would not submit their agreement to the company within 30
                                    days then all payout&nbsp;would be cancelled.</li>
                                <li>If anyone takes any legal action against the company then only our legal advisor would
                                    be dealing with that and Surat, Gujarat remains the only junction for any legal
                                    procedure. No one can contact any employee or director of our company.</li>
                                <li>A vocal statement would not be acceptable. Only the signed agreements would be
                                    acceptable for any reference partner. The company&rsquo;s terms and conditions, privacy
                                    policies, and all other rules are to be unconditionally accepted by every concerned
                                    person.</li>
                                <li>If any person has a doubt/question regarding any of the company&rsquo;s terms and
                                    conditions, they can contact the company.</li>
                                <li>Every reference payout will have a deduction of 5% TDS.</li>
                                <li>The office holidays and bank holidays will not be counted as working days/business days.
                                    The company&rsquo;s office work will be done on working days only.</li>
                            </ol>

                            <p><strong>GENERAL TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>If the login department fails to solve the customer queries with accuracy, dedication
                                    and responsibility, then the login department agency will be cancelled by the company.
                                </li>
                                <li>The Customer&#39;s loan process will take more days due to any festival.</li>
                                <li>If anyone has GST then they have to add the GST number in their portal&nbsp;so that the
                                    company can provide a GST RETURN to them.</li>
                                <li>The Company is using Blogs for their advertising, so that content could be of the third
                                    party, so the company doesn&#39;t take guarantee of the information to be correct or
                                    incorrect.</li>
                                <li>If customers, employees, channel partners, any other person, or any other party has a
                                    problem with the company then they have to inform that problem to our company through
                                    notice; so, we can try to give you a solution of that but after that, any of them want
                                    to take a legal action then they have to inform the company through notice. Only then,
                                    the legal process will be started.</li>
                                <li>If&nbsp;customers, employees, channel partners, any other person, any other third party
                                    has a problem/dispute/misunderstanding with the company, the right to take the final
                                    decision over the concerned issue is reserved with the company and the concerned person
                                    will have to accept the solution provided by the company.</li>
                                <li>The documents, cheques, and OTP that the company&#39;s employee asks the customer is for
                                    the processing of the loan, the company is not responsible if any problems/disputes
                                    arise in the future.</li>
                                <li>During any process on the website, if there is any kind of mistake that happens due to
                                    the software or website technical problems, the final decision on such disputes can only
                                    be taken by the company and it has to be accepted by anyone concerned.</li>
                                <li>The Company&#39;s authorized person can change any rules and regulations at any time;
                                    the concerned person must be regularly updated with the company&rsquo;s terms and
                                    conditions and has to accept them unconditionally.</li>
                                <li>All the commitments made by the company&rsquo;s employees (any employee/person from the
                                    company), telecallers, or salespersons, etc. should be cross-checked by any concerned
                                    person (customer/channel partner) from the Terms &amp; Conditions section of
                                    moneyupfinance.com before availing any of the company&rsquo;s services. Only the rules and
                                    regulations stated on the company website will be considered official.</li>
                                <li>All the detailed criteria, terms, and information behind any of the company&rsquo;s
                                    concise promotional content (social media ads, banners, SMS, advertisements, emails,
                                    etc.) are stated in the Terms &amp; Conditions and Privacy Policy sections of the
                                    website. Any concerned person (customer, channel partner, employee, etc.) must check and
                                    accept all the rules and regulations before availing any of the company&rsquo;s
                                    services.</li>
                                <li>If any Channel Partner/Customer Referral&rsquo;s customer gets a refund (due to any
                                    dispute like payment gateway problem or any other issue) then the referral payout will
                                    not be provided (if provided, it would be deducted from the next payout of the channel
                                    partner or customer referral).</li>
                                <li>While generating the payout for Channel Partners or Customer Referrals, the company uses
                                    a third-party payment gateway. So, if the payout is stuck and put on hold due to any
                                    payment gateway issue (or any other issue), then the payout would be delayed and all the
                                    terms and conditions of the third-party payment gateway would be applied. In such cases,
                                    the payout will be released only when the third-party payment gateway releases the stuck
                                    payment. In case of a payout dispute with any bank, the bank&rsquo;s criteria will be
                                    applied and the payout will be released only when the bank approves the payment.</li>
                                <li>If there is any dispute that arises between any concerned user (channel partner,
                                    customer, customer referral, etc.) and the company, their account will be disabled
                                    immediately by the company. In such a case, the user would be needed to contact the
                                    company for any query.</li>
                                <li>All of the promotional content put and shared by the company, either on its website or
                                    any platform, is only for advertisement purposes. Any person should not assume it as the
                                    final loan approval or details of the loan. The final loan approval and specifics of the
                                    loan depend on the rules and regulations of various banks (or the concerned bank) and
                                    the customer profile. Every customer, channel partner, or any other user must accept
                                    this clause and consider the bank&rsquo;s loan processing time only.</li>
                                <li>The loan-related figures, rates, and information used in the promotional content of the
                                    company are general and for promotional purposes. The final nature and specifics of the
                                    loan in terms of the loan amount, interest rate, repayment tenure, loan processing fees,
                                    loan insurance, etc., depends solely on the customer profile and the rules and
                                    regulations stated by the concerned bank. The final loan details depend on the criteria
                                    set by the concerned bank(s).</li>
                                <li>moneyupfinance&#39;s company name, logo, content, business concept, software and system,
                                    pattern, website structure and design, and business process and offers are copyrighted
                                    with the company. If any individual or organization uses/copy any of the above-mentioned
                                    by even 1%, legal action may be taken against them.</li>
                                <li>If any person (customer, channel partner, employee, etc.) is involved in any of the
                                    company&#39;s processes then the company is authorized to record the phone calls with
                                    that person.</li>
                                <li>If any customer applies for a loan in our company and if any other external
                                    person/organization commits fraud with that customer in terms of taking money from you
                                    or in any other way then, it will not be the company&rsquo;s responsibility for any kind
                                    of loss faced by the customer.</li>
                                <li>Whatever loan offer is given to the customer is according to the customer profile. The
                                    customer will have to compulsorily accept the loan offer &ndash; he/she cannot deny the
                                    loan offer.</li>
                                <li>The company has full authority to use the customer&rsquo;s information for purposes such
                                    as testimonials, advertisements, marketing, SMS, etc. The customer agrees that
                                    regulations of Do Not Disturb(DND)/National Do Not Call(NDNC) won&rsquo;t be applied in
                                    such practices.</li>
                                <li>If any person (user, customer, channel partner, etc.) visits our website and indulges in
                                    any activity &ndash; like clicking a button, link, filling forms, or any other activity
                                    on the website, it will clearly mean and express that the person agrees to and
                                    acknowledges all terms &amp; conditions, rules &amp; regulations, and policies of the
                                    company.</li>
                                <li>After the loan approval, the bank charges will be applied as per the bank&rsquo;s rules
                                    and regulations.</li>
                                <li>No customer can contact the bank&rsquo;s employees to inquire about/get any information
                                    on the loan file processes.</li>
                                <li>The customer, whose loan has been approved, must read and understand the bank agreement
                                    and the bank&rsquo;s terms and conditions carefully. After the loan process is done, the
                                    company can&rsquo;t be held responsible or liable for anything.</li>
                                <li>The company will take legal action against the customer, channel partner, reference
                                    customer, or any other person who submitted fake documents. And the company won&rsquo;t
                                    take any responsibility for the loan process in this case.</li>
                                <li>Multiple partnered banks&#39; logos are shown on our website and our promotional content
                                    across many mediums &ndash; these are shown only for our company&#39;s marketing
                                    purpose. It might be possible that certain banks, whose logos are shown on our
                                    website/promotional content, are not partnered with our company. Also, these should not
                                    be assumed as any bank&#39;s advertisement.</li>
                                <li>If anyone takes any legal action against the company then only our legal advisor would
                                    be dealing with that and Surat, Gujarat will only remain the junction for any legal
                                    procedure. No one would be able to contact any employee or director of our company.</li>
                                <li>A vocal statement by any person would not be acceptable. Only the signed agreements
                                    would be acceptable. The company&rsquo;s terms and conditions, privacy policies, and all
                                    other rules are to be unconditionally accepted by every concerned person.</li>
                                <li>If any person has a doubt/question regarding any of the company&rsquo;s terms and
                                    conditions, they can contact the company.</li>
                                <li>The eligibility age for buying a membership card is 18 - 62 years. The persons in this
                                    age bracket can avail benefits of the membership card.</li>
                                <li>The office holidays and bank holidays will not be counted as working days/business days.
                                    The company&rsquo;s office work will be done on working days only.</li>
                                <li>Loan processing time might get delayed because of any public holiday, technical
                                    problems, customer issues, etc.</li>
                                <li>The Company will not be providing any proof for rejection in hard or soft copy.</li>
                            </ol>

                            <p><strong>CANDIDATE TERMS AND CONDITIONS:</strong></p>

                            <ol>
                                <li>The interview time is fixed.</li>
                                <li>The interview can&#39;t be taken any other time than the time decided by the company.
                                </li>
                                <li>The Company can ask any questions in the interview.</li>
                                <li>The candidate will have to appear for the interview as many times as the company asks.
                                </li>
                                <li>A resume (Xerox) will be mandatory for the interview. The resume will not be returned.
                                    &nbsp;There will be no misuse of the resume.</li>
                            </ol>

                            <p><strong>USAGE OF COOKIES / COOKIES POLICY</strong></p>

                            <p>Cookies are small files that a site or its service provider transfers to your computer&#39;s
                                hard drive through your web browser with your permission which enables the site or service
                                provider&#39;s systems to recognize your browser and capture and remember certain
                                information. We use cookies to help us understand and save your preferences for future
                                visits, keep track of advertisements and compile aggregate data about site traffic and site
                                interaction so that we can offer better site experiences and tools in the future.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
