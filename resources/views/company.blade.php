@extends('layouts.master')


@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Company Profile</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('/') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('company') }}">
			    <span itemprop="name">Company</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
	</div>
</section>

<section id="company">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<div class="heading-text heading-line m-b-0">
					<h4 class="text-medium font-weight-500">moneyupfinance.com</h4>
				</div>
			</div>

			<div class="col-lg-8 col-md-8 col-12 m-b-20">
				<div class="text-justify">
					<h4><strong>A Glimpse of moneyupfinance</strong></h4>

					<p>The company was founded in 2019 with the sole purpose of easing the entire loan process - from applying to approval to sanctioning. In the conventional ways of accessing a loan, the loan seeker has to make multiple visits from one bank to another as different banks have their respective loan approving criteria. This is a very tedious process that consumes much of the loan seeker's time and effort. To reduce these iterations and streamline the whole process of the loan - moneyupfinance was commenced - making it a one-stop solution for filing the loan in multiple banks and getting the loan offers through membership for the seekers.</p>

					<p>In just a very small span of time, moneyupfinance has garnered immense customer trust, satisfaction, and reliance. To make it evident with the figures, we boast of having 37,000+ satisfied customers whom we have had the great opportunity to serve with their required loan offers. We are partnered with multiple leading Banks and NBFCs (Non-Banking Financial Company) to give our customers the maximum chance of meeting the various criteria of loan approval. So, when a loan seeker avails the membership of moneyupfinance, they need not make personal visits to the bank, instead, their loan file will be submitted to multiple banks by us.</p>

					<h4><strong>Vision & Mission</strong></h4>

					<p>With an aim to aid people with their financial goals, dreams, and aspirations - moneyupfinance facilitates people with the most sorted way of getting loan offers from multiple banks. Be it a short-term money requirement or an urgent need for financial help, a Personal Loan is the best option to go for. For the ones who seek to access a personal loan, loan seekers can easily get their Premium Membership Card and get their loan files submitted in multiple banks. Once the loan file meets all the requirements stated by the bank, the loan offer would be issued to us and we would be helping you out with the best loan to be taken. For a person who intends to invest money in their business and wants instant money, they can opt for moneyupfinance's Platinum Membership Card and get up to INR 1 Crore of business loan.</p>

					<p>The numerous benefits gained by a person against very reasonable fees of the Membership Cards are the best part of this membership program. With very smooth loan offer processing, the membership card owners will get 10 years of Free Consultancy. So, just by sitting and relaxing at home, a person can get a loan directly into their bank accounts. Also, moneyupfinance's Channel Partner Program is a fantastic opportunity for individuals to refer and earn money through full-time dedication or part-time attention.</p>
				</div>
			</div>


			<div class="sidebar col-lg-4 col-md-4 col-12">
				<div class="sidebar-menu">
					<h4>FACTSHEET</h4>
					<label>Developer:</label>
					<p>
						<a href="{{ url('') }}">moneyupfinance Service India Pvt. Ltd.</a><br/>
						Based in Gujarat, India
					</p>

					<label>Founding date:</label>
					<p>24<sup>rd</sup> JULY 2019</p>

					<label>CIN No.:</label>
					<p>U31101GJ2016PTC813691</p>

					<label>Website:</label>
					<p><a href="{{ url('/') }}">https://moneyupfinance.com</a></p>

					<label>Business Enquiry:</label>
					<p><a href='mailto:info@moneyupfinance.com'>info@moneyupfinance.com</a></p>

					<label>Social:</label>
					<p>
						<a href="#" target="_blank">Facebook</a>
						<br/>
						<a href="#" target="_blank">Instagram</a>
						<br/>
						<a href="#" target="_blank">Linkedin</a>
						<br/>
						<a href="#" target="_blank">Twitter</a>
					</p>

					<label>Address:</label>
					<p>4001,<sup></sup> Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009</p>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line m-b-0 text-center">
			<h4 class="text-medium font-weight-500">Our Membership Cards</h4>
		</div>

		<div class="row m-t-20">
			<div class="col-12 text-center">
				<div class="post-3-columns m-b-30" data-item="post-item">
					<div class="post-item border p-20">
						<div class="post-item-wrap">
							<div class="post-image">
								<img src="{{ asset('public/assets/images/slider/membership-card-premium.png') }}" alt="premium membership card">
							</div>
							<div class="post-item-description">
								<h2>Premium Membership Card</h2>
								<a href="{{ url('premium-membership-card') }}" class="item-link">Learn More <i class="fa fa-arrow-right"></i></a>
							</div>
						</div>
					</div>

					<div class="post-item border p-20">
						<div class="post-item-wrap">
							<div class="post-image">
								<img src="{{ asset('public/assets/images/slider/membership-card-platinum.png') }}" alt="platinum membership card">
							</div>
							<div class="post-item-description">
								<h2>Platinum Membership Card</h2>
								<a href="{{ url('platinum-membership-card') }}" class="item-link">Learn More <i class="fa fa-arrow-right"></i></a>
							</div>
						</div>
					</div>

					<div class="post-item border p-20">
						<div class="post-item-wrap">
							<div class="post-image">
								<img src="{{ asset('public/assets/images/slider/membership-card-channel.png') }}" alt="channel partner membership card">
							</div>
							<div class="post-item-description">
								<h2>Channel Partner Code</h2>
								<a href="{{ url('channel-partner-code') }}" class="item-link">Learn More <i class="fa fa-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row m-t-20">
			<div class="col-12 text-center">
				<h4><strong>CARD BENEFITS</strong></h4>

				<p>Firstly, moneyupfinance asks for your loan related document and then curate a loan file. After that, we send them to our multiple partnered banks and NBFCs, who will check your profile for eligibility and give approval for the loan. If your profile matches in more than one bank or NBFC, moneyupfinance will provide you with the list of approving NBFC/Banks, saving you a great amount of time that otherwise would be spent in going to multiple banks for loans. This is one of the most important benefits of our company's membership card because getting a loan is a time-consuming process. In a daily busy routine, no one has time to visit multiple banks for the loan process so here our company is providing this facility on one platform. Secondly, every time you open your file at a bank for a loan, your CIBIL score gets deducted, but with moneyupfinance your file will only open in those banks where your profile matches, helping you in a better CIBIL score even if you don't take a loan. It should be noted that the Membership Card is not any sort of credit or debit card and the customer should not be of the impression that buying this card means getting money in the bank. Membership Card is limited to our company only, providing certain benefits.</p>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="heading-text heading-line text-center p-b-5">
			<h3 class="text-medium font-weight-500">Our Company Highlights</h3>
		</div>

		<div class="row icon-boxes">
			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-grid"></i>
				<div class="icon-box-content">
					<h3>Partnered With Multiple Banks & NBFCs</h3>
					<p>We're collaborated with leading banks to provide the best loan offers</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-star"></i>
				<div class="icon-box-content">
					<h3>High-Rated Google Reviews</h3>
					<p>Thanks to our quick services, our customers experience high satisfaction</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-users"></i>
				<div class="icon-box-content">
					<h3>Huge Customer Base – 55000+ Customers</h3>
					<p>Our ever-increasing customer base reflects customer trust & appreciation</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-briefcase"></i>
				<div class="icon-box-content">
					<h3>Large Network of 949 Partners</h3>
					<p>Our partners are being benefited by the company's exponential growth</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-film"></i>
				<div class="icon-box-content">
					<h3>Esteemed Media Coverage</h3>
					<p>We're everywhere! We've been covered by prestigious media houses</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-phone-call"></i>
				<div class="icon-box-content">
					<h3>PAN India Service</h3>
					<p>Our loan services are availed by people from all corners of the country</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="background-grey p-b-30">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Our Media Coverage</h4>
				</div>

				<div class="carousel client-logos" data-items="5" data-arrows="false" data-dots="false">
					<div class="icon-box box-type effect center process">
						<img alt="dailyhunt" src="{{ asset('public/assets/images/media/dailyhunt.png') }}">
					</div>
					<div class="icon-box box-type effect center process">
						<img alt="the-print" src="{{ asset('public/assets/images/media/the-print.png') }}">
					</div>
					<div class="icon-box box-type effect center process">
						<img alt="business-standard" src="{{ asset('public/assets/images/media/business-standard.png') }}">
					</div>
					<div class="icon-box box-type effect center process">
						<img alt="aninews" src="{{ asset('public/assets/images/media/ani-news.png') }}">
					</div>
					<div class="icon-box box-type effect center process">
						<img alt="lokmat" src="{{ asset('public/assets/images/media/lokmat-english.png') }}">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
