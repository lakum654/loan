@extends('layouts.master')


@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Important Update</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('important-update') }}">
			    <span itemprop="name">Important Update</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
	</div>
</section>


<section id="update">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<p class='text-center'><strong>No update as of now!</strong></p>
			</div>
		</div>
	</div>
</section>


@endsection
