@extends('layouts.master')


@section('main')


<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Sitemap</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('sitemap') }}">
			    <span itemprop="name">Sitemap</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
	</div>
</section>

<section class="site-map">
	<div class="container">
		<div class="row">

			<div class="col-lg-3 col-md-3 col-12">
				<h4>Company pages</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('company') }}">Company</a></li>
					<li><a href="{{ url('contactus') }}">Contact Us</a></li>
					<li><a href="{{ url('career') }}">Career</a></li>
					<li><a href="{{ url('faq') }}">FAQs</a></li>
					<li><a href="#">Loan Calculator</a></li>
					<li><a href="{{ url('important-update') }}">Important Update</a></li>
					<li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ url('refund-policy') }}">Return & Refund Policy</a></li>
                    <li><a href="{{ url('disclaimer') }}">Disclaimer</a></li>
                    <li><a href="{{ url('terms-conditions') }}">Terms & Conditions</a></li>
				</ul>

				<div class="seperator"></div>
				<h4>Login</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('digital/personal-loan') }}">Customer Login</a></li>
                    {{-- <li><a href="{{ url('channel-login') }}">Channel Partner Login</a></li> --}}
				</ul>
			</div>

			<div class="col-lg-3 col-md-3 col-12">
				<h4>Products</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('premium-membership-card') }}">Premium Membership Card</a></li>
	                <li><a href="{{ url('platinum-membership-card') }}">Platinum Membership Card</a></li>
	                {{-- <li><a href="{{ url('channel-partner-code') }}">Channel Partner Code</a></li> --}}
				</ul>

				<div class="seperator"></div>
				<h4>We Offer</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('personal-loan') }}">Digital Personal Loan</a></li>
                    <li><a href="{{ url('business-loan') }}">Digital Business Loan</a></li>
                    {{-- <li><a href="{{ url('channel-partner') }}">Channel Partner</a></li> --}}
				</ul>

				<div class="seperator"></div>
				<h4>Apply Now</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('digital/personal-loan') }}">Personal Loan</a></li>
                    <li><a href="{{ url('digital/personal-loan') }}">Business Loan</a></li>
                    {{-- <li><a href="{{ url('partner/channel') }}">Channel Partner</a></li> --}}
				</ul>
			</div>

			<div class="col-lg-3 col-md-3 col-12">
				<h4>Topics</h4>

				<ul class="list-icon list-icon-caret">
                    <li><a href="{{ url('personal-loan-for-self-employed') }}">Personal loan for self employed</a></li>
                    <li><a href="{{ url('personal-loan-for-cibil-defaulters') }}">Pesonal loan for cibil defaulters</a></li>
                    <li><a href="{{ url('personal-loan-balance-transfer') }}">Personal loan balance transfer</a></li>
                    <li><a href="{{ url('personal-loan-without-salary-slip') }}">Personal loan without salary slip</a></li>
                    <li><a href="{{ url('personal-loan-private-finance') }}">Personal loan private finance</a></li>
                    <li><a href="{{ url('personal-loan-for-nri') }}">Personal loan for NRI</a></li>
                    <li><a href="{{ url('pre-approved-personal-loan') }}">Pre-approved personal loan</a></li>
                    <li><a href="{{ url('documents-required-for-personal-loan') }}">Documents required for personal loan</a></li>
                    <li><a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a></li>
                    <li><a href="{{ url('top-up-personal-loan') }}">Top Up Personal Loan</a></li>
                    <li><a href="{{ url('business-loan-eligibility') }}">Business loan eligibility</a></li>
                    <li><a href="{{ url('loan-agency-in-india') }}"> Loan agency / loan service provider in India</a></li>
                    <li><a href="{{ url('long-term-loans') }}">Long term loans</a></li>
				</ul>
			</div>

			<div class="col-lg-3 col-md-3 col-12">
				<h4>Personal Loan by City</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi NCR</a></li>
                    <li><a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a></li>
                    <li><a href="{{ url('personal-loan-in-kerala') }}">Kerala</a></li>
                    <li><a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a></li>
                    <li><a href="{{ url('personal-loan-in-pune') }}">Pune</a></li>
                    <li><a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a></li>
                    <li><a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a></li>
                    <li><a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a></li>
				</ul>

				<div class="seperator"></div>
				<h4>Business Loan by City</h4>

				<ul class="list-icon list-icon-caret">
					<li><a href="{{ url('business-loan-in-delhi-ncr') }}">Delhi NCR</a></li>
                    <li><a href="{{ url('business-loan-in-bangalore') }}">Bangalore</a></li>
                    <li><a href="{{ url('business-loan-in-kerala') }}">Kerala</a></li>
                    <li><a href="{{ url('business-loan-in-bihar') }}">Bihar</a></li>
				</ul>
			</div>

		</div>

	</div>
</section>


@endsection
