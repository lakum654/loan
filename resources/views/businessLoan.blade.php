@extends('layouts.master')


@section('main')


<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
	<div class="slide background-gradient-2">
		<div class="container">
			<div class="slide-captions row">
				<div class="col-lg-6 col-md-6 col-12 align-self-center">
					<h1 class="text-medium">Scale-up Your Company's Progress with Instant Business Loan </h1>
					<p>Business loan approvals made easy with moneyupfinance. Apply for a Business loan through multiple banks or NBFCs.</p>

					<a href="{{ url('digital/personal-loan') }}" class="btn btn-dark btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply Now</span><i class="fa fa-arrow-right"></i></a>
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<img src="{{ asset('public/assets/images/slider/bl-offer-img.png') }}" alt="digital business loan" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>


<section class="background-grey">
	<div class="container text-center m-b-20">
		<div class="heading-text heading-line text-center">
			<h3 class="text-medium font-weight-500">Opt For Your Most Convenient Business Loan</h3>
		</div>
		<p>When there are suitable loan repayment tenures, you can always execute your business plans in a more effective manner and right direction. With moneyupfinance, you get a wide range of repayment tenures to choose from - enabling your business to spread its wings and mark a milestone.</p>
	</div>

	<div class="container-fluid">
		<div class="row pricing-table">
			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">1<span>Year</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 70,000 / month</li>
							<li>Min. Loan: ₹ 50,000/-</li>
							<li>Max. Loan: ₹ 5,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">2<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 1,15,000 / month</li>
							<li>Min. Loan: ₹ 50,000/-</li>
							<li>Max. Loan: ₹ 20,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">3<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 2,80,000 / month</li>
							<li>Min. Loan: ₹ 50,000/-</li>
							<li>Max. Loan: ₹ 35,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">4<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 3,50,000 / month</li>
							<li>Min. Loan: ₹ 50,000/-</li>
							<li>Max. Loan: ₹ 75,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">5<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 5,00,000 / month</li>
							<li>Min. Loan: ₹ 50,000/-</li>
							<li>Max. Loan: ₹ 1 Cr.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-12">
				<div class="heading-text heading-line p-b-5">
					<h2 class="text-medium font-weight-500">Instant Business Loans to fulfil all your Business Pursuits!</h2>
				</div>

				<p>As a business owner, we understand the entire range of your financial needs. At any stage of your business flow, monetary requirements can arise and at that instance, the best way to deal with the situation is by going for a Business Loan. You can avail your quick Business Loan with moneyupfinance.com and get loan offers from multiple banks.</p>

				<p>A Business Loan can be used for fulfilling monetary requirements for:</p>

				<ul class="list-icon list-icon-caret">
					<li>Streamlining Healthy Cash Flow</li>
					<li>Make Timely Supplier Payments</li>
					<li>Expand Your Business to New Heights</li>
					<li>Grow Your Business Potential</li>
					<li>To Hire New Talents for Your Business</li>
				</ul>
			</div>

			<div class="col-lg-5 col-md-5 col-12 text-center">
				<img src="{{ asset('public/assets/images/slider/info-banner-5.png') }}" alt="business loan purposes" class="img-fluid">
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line p-b-5 text-center">
			<h3 class="text-medium font-weight-500 text-center">Platinum Membership Card - Studded With Great Perks</h3>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12">
				<p>moneyupfinance.com enables you to access some amazing perks when you buy a Platinum Membership Card. With this awarding membership card, you have the ease of getting business loan offers from multiple banks relying on your eligibility. It should be noted that the Platinum Membership Card is not any sort of credit or debit card and the customer should not be of the impression that buying this card means getting money in the bank. Membership Card is limited to our company only, providing certain benefits:</p>
				<h5>Rewarding Perks of the Platinum Membership Card</h5>
				<p>The Platinum Membership Card provided by moneyupfinance is the best and the easiest option to apply for a business loan in multiple banks and financial firms. The loan seeker can get business loan offers through the complete online process provided by moneyupfinance.com. Besides getting the loan offers, the membership card holder can avail 10 years of free consultancy by the company and get assisted in reapplying for loans. Not just this, you can also earn money through referrals and make a potential side-income.</p>

			</div>

			<div class="col-lg-6 col-md-6 col-12">
				<div>
					<h4><i class="fa fa-certificate"></i> Avail Pre-Approved Loan Offer</h4>
					<p>It's always amazing to receive business loan offers from multiple banks and private financial institutions. And yes, you can select the most appropriate business loan!</p>
				</div>
				<div class="line"></div>
				<div>
					<h4><i class="fa fa-calendar-alt"></i> 10 Years Free Consultancy</h4>
					<p>We are more than just a one-time service provider! With the Platinum Membership Card, we assist you in getting a business loan for 10 years for FREE.</p>
				</div>
				<div class="line"></div>
				<div>
					<h4><i class="fa fa-percent"></i> Earn up to 40% through Referrals</h4>
					<p>While you get your most convenient business loan offers, you can earn money through referrals and fetch in a potential income.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 text-center">
				<div class="heading-text heading-line text-center">
					<h3 class="text-medium font-weight-500">Receive Superfast Business Loan Offers from moneyupfinance</h3>
				</div>

				<p>Whenever you think of applying for a business loan, the first thing that pops up in your mind is making multiple visits to various banks, iterations of documents submission, and giving efforts to an unclear result. But with moneyupfinance, you can receive business loan offers at the comfort of your home. All you need to do is buy the Platinum Membership Card - once this is done, the moneyupfinance team will conduct a quick submission process and come up with all the possible loan offers - depending upon your loan application and eligibility.</p>
				<p>As the monetary requirements for your business may have the nature of urgency, moneyupfinance wastes no time and assists you comprehensively in fetching the best business loan offers for you. This remains the most optimized way of approaching a business loan and helping your business grow and expand to the fullest.</p>

				<a href="{{ url('digital/business-loan') }}" class="btn btn-primary btn-rounded btn-reveal btn-reveal-right"><span>Check Eligibility for Business Loan</span><i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Our NBFC Bank Partners</h4>
				</div>
				<p>We Satisfy Your Business Loan Needs - Backed by the Best Lending Firms!</p>

				<ul class="grid grid-6-columns">
				<li class="p-20"><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/026.png') }}" alt="Bajaj Finserv"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/028.png') }}" alt="Ziploan"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/030.png') }}" alt="Hero Fincorp"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/031.png') }}" alt="Monexo"></li>				</ul>

			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">How to apply for a Business Loan at moneyupfinance?</h4>
		</div>

		<div class="row p-t-40">
			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>1</h2></div>
					<h3>Quick Registration</h3>
					<p>You can start off with an easy and fast process of registration. This 5-minute process will require you to fill in your basic details.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>2</h2></div>
					<h3>Check Eligibility</h3>
					<p>Once you are done with the registration process, you can check your eligibility for a business loan - for better understanding.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>3</h2></div>
					<h3>Buy Membership Card</h3>
					<p>Getting your Platinum Membership Card will enable you to further your process and receive loan offers from multiple banks and NBFCs.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>4</h2></div>
					<h3>Submit Documents</h3>
					<p>After you buy the membership card, you will be asked to submit digital and hard copies of your documents that would take your loan process forward.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>5</h2></div>
					<h3>Bank Verification</h3>
					<p>The next step is the bank verification where your loan application would be evaluated (on the basis of your submitted documents) and a fair sanction would be provided to you.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>6</h2></div>
					<h3>Bank Sanction</h3>
					<p>Once the loan application is approved by the bank, the borrower will receive the sanctioned amount of money in their bank.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="accordion accordion-simple">
					<div class="ac-item">
						<h5 class="ac-title">What is a Business Loan?</h5>
						<div class="ac-content">
							<p>Business, by its true nature of functioning, requires some monetary investment (at initial phase or later phases) in expectation of a return in the form of a profitable amount of money. It would be appropriate to say that money is needed at all the stages of a business - whatever the purpose may be. So, to cater to the financial challenges of a business at any given instance, a business owner can take a business loan to ensure the smooth process of their business.</p>
							<p>To state it in simple definition, a Business Loan is a particular amount of money lent by the investor or business owner from a bank or financial institution, to fulfil their respective business purposes. Taking a business loan can prove to be advantageous for maintaining a healthy cash flow, making undelayable payments to the suppliers, or hiring new talents for expanding the business, etc.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Benefits of a Business Loan</h5>
						<div class="ac-content">
							<p>Conducting any business requires all its aspects to execute flawlessly. And for the same purpose, the flow of money has to be sufficient - to ensure the business is run without avoidable obstacles and challenges. And above all, as business loans fall under the category of unsecured credit, there is no collateral guarantee to be provided by the loan seeker. This reduces the burden of the loan seeker as they may or may not have the required collateral to provide to the bank or the financial firms.</p>
							<p>Taking a business loan may also help the borrower with some tax benefits and if the repayment is done timely and smoothly, it improves the credit score and business credibility. With the capital support that comes with a business loan, every stage of the business can become more efficient and open the prospects of widening the business and its operations.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Business Loan eligibility criteria for Small Business Persons</h5>
						<div class="ac-content">
							<p>For the small business owners who seek to get a business loan offer, it is important to fulfil certain criteria.</p>
							<ul>
								<li>Minimum Age - 21 Years</li>
								<li>Proof of at least 1 year of Income Tax Returns</li>
								<li>1 Year Business Stability</li>
							</ul>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Business Loan eligibility criteria for Audited Report Business Persons</h5>
						<div class="ac-content">
							<p>When an audited report business person wants to apply for a business loan, the following criteria are mandatory to be met:</p>
							<ul>
								<li>Minimum Age - 21 Years</li>
								<li>1 Crore plus Yearly Turnover</li>
								<li>Minimum 2 Years Audited Report</li>
							</ul>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Multiple Options of Loan Tenure</h5>
						<div class="ac-content">
							<p>Depending upon your business nature, history, and stability, a loan seeker can be eligible for business loan offers from multiple banks and financial institutions. The loan lenders often look for businesses that have higher stability and credit score. But that doesn't mean that new business ventures have lower chances of getting a business loan. A business loan can have multiple options of loan tenure, and the borrower can choose the most convenient one from all the available choices.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Collateral Guarantee Not Required</h5>
						<div class="ac-content">
							<p>Business Loans are an option provided by the banks against which no collateral guarantee is asked for. The only form of guarantee to be considered by the bank are the documents submitted (comprising your loan application) by the borrower. Some aspects that are considered for guarantee are your monthly income, business stability, the desired loan amount, etc.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">Why moneyupfinance.com?</h4>
		</div>

		<p class="text-center">moneyupfinance boasts of being India's ace loan providing company - giving its customers amazing benefits in the form of loan offers from multiple banks and NBFCs. With moneyupfinance.com, the loan seekers can easily get business loans directly into the bank account after a very easy and quick registration process and buying the Platinum Membership Card.</p>

		<div class="row p-t-20">
			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-users"></i></div>
					<h5>1000+ Happy Customers Monthly</h5>
					<p>We take pride in giving our customers the best of services and receiving immense customer satisfaction.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-file-alt"></i></div>
					<h5>Minimum Documents Required</h5>
					<p>The loan seeker will have to submit very basic documents (as per the profile) to take the loan process further.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-university"></i></div>
					<h5>Tie-up with Multiple Banks & NBFCs</h5>
					<p>A business loan offer of up to INR 1 Crore could be availed (if the customer profile meets the criteria) from our partnered banks and NBFCs.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-percent"></i></div>
					<h5>Good Range of Annual Percentage Rate</h5>
					<p>Giving its customers the best business loan offer, moneyupfinance offers a range of annual percentage rates from 11.5% to 24%.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-calendar"></i></div>
					<h5>Flexible Repayment Terms</h5>
					<p>Once the loan amount is transferred to your bank, you can repay it through the flexibility of monthly payments for the decided loan tenure.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-hourglass-half"></i></div>
					<h5>Provision of Premium Membership Card</h5>
					<p>With a 100% digital loan process, the customer can avail the membership card and get stunning benefits like loan offers from multiple banks and 10 years of free consultancy.</p>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection
