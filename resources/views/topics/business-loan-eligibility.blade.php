@extends('layouts.master')

@section('title','Check Instant Business Loan Eligibility for free | moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-106.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Business Loan Eligibility</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('/') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('business-loan-eligibility') }}">
			    <span itemprop="name">Business Loan Eligibility</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
		<div class="m-t-20 text-center">
			<a class="btn btn-dark btn-sm" href="{{ url('digital/personal-loan') }}">Apply Now</a>
		</div>
	</div>
</section>

<section class="p-t-30 sidebar-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">

				<div class="card">
	                <div class="card-body">
						<p class="text-justify">Funds are the first requirement for creating a business. In almost all cases, the business owner and partners always bring the initial fund to start a business. But as the business grows, additional funds are a must to grow the business. For example, it may require additional funding to purchase a new machine, hire a servant, run an operation, make inventory, or repair equipment. In any case, you need to find a business loan provider for the necessary funds. However, you may also need to meet the business loan provider's business standards.</p>

						<p class="text-justify">The business loan eligibility criteria usually depend on the type of business, income, business health, profit loss balance sheet, director or partner's CIBIL score, age limit, and more.</p>

						<h3>Basic Documents required for a business loan:</h3>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>ID Proof and Address Proof</li>
							<li>Proof of Business Stability</li>
							<li>Bank Statement of 12 Months</li>
							<li>Board Resolution (If Pvt. Ltd. company is a co-borrower, then a Partnership Authorization Letter is required</li>
							<li>ITR (Income Tax Return) & Statement of Computation of Income – for last 3 Years</li>
							<li>CA Certified or Audited Profit/Loss Account Statement; for loans>INR 40 Lacs cases Audit report is required along with Profit & Loss and Balance Sheet schedules – for the last three years.</li>
							<li>Latest ITR (Income Tax return) of Co-Applicants – For last 3 Years</li>
							<li>Ownership Proof of Residence or Office</li>
						</ul>

						<div class="text-center m-t-20">
							<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-block">Check your Eligibility for Business loan</a>
						</div>
					</div>
				</div>


				<div class="card">
	                <div class="card-body">
						<h3>Types of Business Loans in India</h3>

						<div class="row">
							<div class="col-md-6">
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Working Capital Loan</li>
									<li>Term Loan</li>
									<li>Startup Loan</li>
									<li>Latter or Credit Loan</li>
									<li>Bill/Invoice Discounting</li>
									<li>Overdraft Facility</li>
									<li>Equipment Finance</li>
								</ul>
							</div>

							<div class="col-md-6">
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Loan for Machinery</li>
									<li>Fleet Finance </li>
									<li>Micro Lending</li>
									<li>Trade Credit</li>
									<li>POS Loan / Merchant Cash Advance</li>
									<li>Loan under Govt. Schemes</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>How to get a Business Loan?</h3>

						<p><strong>Step 1:</strong> Check your Loan Eligibility</p>
						<p><strong>Step 2:</strong> Apply Online with the correct Documents</p>
						<p><strong>Step 3:</strong> Sign the Agreement after tenure and interest are agreed upon</p>
						<p><strong>Step 4:</strong> Receive loan amount in as less as 2 Days</p>
						<p><strong>Step 5:</strong> Start operating business with ease</p>

						<p class="text-justify">When you are ready to acquire a loan for the business with the required documents, the bank acts by interviewing the borrower. The lender wants to know some information related to the business. Some possible questions are as follows:</p>

						<h4>Some Questions which Business Loan Provider Banks May Ask You:</h4>

						<ol>
							<li>About your Business plan & Business Model</li>
							<li>Amount of loan required</li>
							<li>About where the borrower intends to use the loan</li>
							<li>Question about personal credit history</li>
							<li>About collaterals being offered</li>
							<li>Potential of business to repay the loan</li>
							<li>Where does the business stand in terms of the present market</li>
							<li>What is the repayment tenure required?</li>
						</ol>

						<h3>Factors that impact Business Loan Interest Rate:</h3>

						<ol>
							<li>Financial Health of business</li>
							<li>Nature of Business</li>
							<li>Business Plan & Model</li>
							<li>Business vintage</li>
							<li>Annual Turnover</li>
							<li>Profitability & Stability</li>
							<li>CIBIL Score & Credit History</li>
							<li>Loan Amount & Repayment Tenure</li>
							<li>Collateral</li>
							<li>Lender Type</li>
						</ol>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>How to Calculate Interest Rate on Business Loan?</h3>

						<p>Business Loan EMI is calculated from the below-mentioned formula:</p>

						<p><strong>E = P x r x (1+r)n / (1+r) n-1</strong><br/>
						Here,<br/>
						E = EMI amount for Repay Loan<br/>
						P = Principal amount<br/>
						r = Rate of interest<br/>
						n = Tenure of Loan</p>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h2>Loan Eligibility Calculator</h2>

						<p>For Instance:<br/>
						Business loan amount = ₹ 5 lakh<br/>
						Interest rate = 20%<br/>
						Tenure = 3 years<br/>
						As per the formula, the loan interest rate per month will be 20/12 = 1.66%<br/>
						Total tenure in months = 3 x 12 = 36 months<br/><br/>
						Therefore:<br/>
						EMI= [5,00,000 x 1.66/100 x (1+1.66/100) ^ 36 / [(1+1.66/100) ^ 36 – 1)<br/>
						You will get your EMI = Rs. 18,582/-</p>

						<p class="text-justify">This method is not only time-consuming but also subject to human error. As a reason, it is safer to utilize an online EMI calculator, which is conveniently accessible via the internet.</p>

						<p class="text-justify">Loan eligibility or repayment capacity is mainly based on the individual's total profit and current loan EMI.</p>

						<p class="text-justify">Many other factors determine loan eligibility, such as age, financial status, other income, credit history, credit score, other financial obligations, and loans.</p>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>How to increase Eligibility for Business Loan?</h3>

						<p>Eligibility for business loans can be increased by:</p>
						<ol>
							<li>Take out a property or other asset-based secured loan such as a gold loan for a business loan that is easily available.</li>
							<li>Take steps to improve your credit / Cibil score (if any). For example, make regular repayment of EMI of the existing loan.</li>
						</ol>
					</div>
				</div>

				<div class="row call-to-action call-to-action-skyblue p-20 m-t-30">
					<div class="col-md-9">
						<h3>Apply for a platinum membership card and get an instant loan offer</h3>
					</div>

					<div class="col-md-3 text-center">
						<a class="btn btn-dark" href="{{ url('digital/personal-loan') }}">Apply Now</a>
					</div>
				</div>

			</div>

			<div class="sidebar sticky-sidebar col-lg-3 col-md-3">
	<div class="widget">
		<h4 class="widget-title">Membership Cards</h4>
		<div><img src="{{ asset('public/assets/images/slider/membership-card-platinum.png') }}" alt="platinum membership card" class="mw-100"></div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Topics</h4>

		<ul class="list list-arrow-icons m-b-0">
            <li> <a href="{{ url('personal-loan-for-self-employed') }}">Personal Loan for Self Employed</a> </li>
			<li> <a href="{{ url('personal-loan-for-cibil-defaulters') }}">Personal Loan for Cibil Defaulters</a> </li>
			<li> <a href="{{ url('personal-loan-balance-transfer') }}">Personal Loan Balance Transfer</a> </li>
			<li> <a href="{{ url('personal-loan-private-finance') }}">Personal Loan Private Finance</a> </li>
			<li> <a href="{{ url('personal-loan-for-nri') }}">Personal Loan for NRI</a> </li>
			<li> <a href="{{ url('pre-approved-personal-loan') }}">Pre-approved Personal Loan</a> </li>
			<li> <a href="{{ url('documents-required-for-personal-loan') }}">Documents Required for Personal Loan</a> </li>
			<li> <a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a> </li>
			<li> <a href="{{ url('top-up-personal-loan') }}">Top Up Personal Loan</a> </li>
			<li> <a href="{{ url('loan-agency-in-india') }}">Loan Agency in India</a> </li>
		</ul>
	</div>

	<div class="widget widget-tags m-b-30">
        <div class="tags">
			<a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi</a>
			<a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a>
			<a href="{{ url('personal-loan-in-kerala') }}">Kerala</a>
			<a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a>
			<a href="{{ url('personal-loan-in-pune') }}">Pune</a>
			<a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a>
			<a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a>
			<a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a>
		</div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Quick Links</h4>

		<ul class="list list-arrow-icons m-b-0">
            <li> <a href="{{ url('digital/personal-loan') }}">Customer Login</a> </li>
			<li> <a href="{{ url('premium-membership-card') }}">Purchase Membership Card</a> </li>
			<li> <a href="{{ url('company') }}">Our Media Coverage</a> </li>
			{{-- <li> <a href="{{ url('') }}">Business Loan EMI Calculator</a> </li> --}}
			<li> <a href="{{ url('/') }}">Earn with moneyupfinance</a> </li>
		</ul>
	</div>

</div>
		</div>
	</div>
</section>

@endsection
