@extends('layouts.master')

@section('title','Basic Documents required for personal loan | moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-106.jpg">
	<div class="container">
		<div class="page-title">
			<h1>What Are The Documents Required For Personal Loan? Find The Entire List With Relevant Information!</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('/') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('documents-required-for-personal-loan') }}">
			    <span itemprop="name">Documents Required for Personal Loan</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
		<div class="m-t-20 text-center">
			<a class="btn btn-dark btn-sm" href="{{ url('digital/personal-loan') }}">Apply Now</a>
		</div>
	</div>
</section>

<section class="p-t-30 sidebar-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">

				<div class="card">
	                <div class="card-body">
						<p class="text-justify">As personal loans are sought by many and its need is surging than ever before, it is important to know the documents required for personal loan application. Though the exact required documents for a personal loan differ from one bank to another, we will look at the basic but mandatory list of documents asked by the banks in pursuit of a personal loan.</p>

						<p class="text-justify"><strong>Get Instant Personal Loan Offer With Minimal Documents & 100% Online Process –</strong></p>

						<div class="text-center m-t-20">
							<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-block">Apply Now</a>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h2>What are the Documents Required by Salaried Individuals for Personal Loan?</h2>

						<p class="text-justify">The need for monetary assistance can arise at any given point in life. And in the cases of salaried people with a limited amount earned every month, it might get difficult to meet an urgent requirement of money – and the best option to deal with such a money crunch is by availing a personal loan. With the help of the list below, you can start gathering the documents that will be needed by the banks to consider your personal loan application:</p>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>Identity Proof (Driving License, Aadhar Card, Voter ID, Passport, etc.)</li>
							<li>PAN Card (Permanent Account Number)</li>
							<li>Residence Proof (Utility Bills (like Electricity Bill, Gas Bill, Voter ID, etc.), Passport, Ration Card, Bank Statement, Rental Agreement, etc.)</li>
							<li>Bank Statement of 3 Months</li>
							<li>Salary Slips/Certificate of 3 Months</li>
							<li>Form 16 (or ITR – Income Tax Returns)</li>
						</ul>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h2>What are the Documents Required by Self-Employed Individuals for Personal Loan?</h2>

						<p class="text-justify">As self-employed persons generally run a business, they can need urgent money for various purposes – they might need to make an urgent payment to the supplier or manufacturer, or they may look to bring in young talents to boost their business, or they would want to invest in the appearance of office through renovation, or they would like to stabilize the business cash flow – and in case of any of the above reasons, the business owner can take a personal loan and fulfil the concerned requirements. The following list depicts the generally required documents for a self-employed person to apply for a personal loan:</p>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>Identity Proof (Driving License, Aadhar Card, Voter ID, Passport, etc.)</li>
							<li>PAN Card (Permanent Account Number)</li>
							<li>Residence Proof (Utility Bills (like Electricity Bill, Gas Bill, Voter ID, etc.), Passport, Ration Card, Bank Statement, Rental Agreement, etc.)</li>
							<li>Bank Statement of 3 Months</li>
							<li>Balance Sheet of 2 Years</li>
							<li>Profit/Loss Account Statement of 2 Years</li>
							<li>Income Computation of 2 Years</li>
							<li>Business Proof – Service Tax Registration, Licence, Registration Certificate</li>
							<li>Clearance Certificate/IT Assessment</li>
							<li>Income Tax Returns</li>
						</ul>

					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>Avail The Quickest Personal Loan Offers Within 48 Hours through moneyupfinance.com</h3>

						<p class="text-justify">Whenever a loan requirement arises, the first thought that might cross the loan seeker's mind is – ‘I might need to visit many banks, pass through the tedious process of multiple iterations of document submission, and then wait for the final judgement on my loan approval' – but with moneyupfinance's super-quick services, all the conventions get broken! With moneyupfinance's 100% online process, you can get personal loans from multiple banks and NBFCs (Non-Banking Financial Companies) at the convenience of your home, just by making some clicks on your smartphone or computer system.</p>

						<h5 class="text-center">Our NBFC Bank Partners</h5>

						<ul class="grid grid-5-columns">
						<li class="p-20"><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/026.png') }}" alt="Bajaj Finserv"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/028.png') }}" alt="Ziploan"></li>						</ul>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>What is the process of getting personal loan offers from moneyupfinance?</h3>

						<p>Just follow the below steps that would hardly take a span of some minutes:</p>

						<div class="row p-t-20">
							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>1.</h2></div>
									<p>Quick Registration</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>2.</h2></div>
									<p>Check Eligibility</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>3.</h2></div>
									<p>Buy Membership Card</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>4.</h2></div>
									<p>Submit Documents</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>5.</h2></div>
									<p>Get Sanctioned</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>6.</h2></div>
									<p>Receive funds</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>Why <a href="{{ url('/') }}">moneyupfinance.com</a>?</h3>

						<p class="text-justify">Being India's number one loan service provider, moneyupfinance is the most trusted, reliable, and acclaimed loan consultant that has a mammoth customer base of over 37000 happy and satisfied customers. As the company is partnered with multiple leading banks and NBFCs, it gives a wide range of loan offers to our customers from which they can choose the most convenient loan offer and satisfy their financial needs, short-term and long-term goals, or monetary aspirations.</p>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>Benefits of buying moneyupfinance's Premium Membership Card</h3>

						<p class="text-justify">Being one of the smartest solutions to all your loan problems, moneyupfinance Premium Membership Card facilitates you with exciting and innovative benefits:</p>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>Get Pre-Approved Loan Offers from Multiple Banks & NBFCs</li>
							<li>Enjoy 10 Years of Free Consultancy</li>
							<li>You can earn up to 40% payout (per membership card) through easy referring and sharing</li>
							<li>No eligibility criteria are needed to get a membership card – an individual belonging from any social stature, financial background, or professional arena can buy a membership card.</li>
						</ul>

						<p class="text-justify">So, if you are someone who is a loan seeker and does not want to go through the exhausting conventional process, the easiest way for you to avail of a personal loan is by visiting <a href="{{ url('/') }}">www.moneyupfinance.com</a> and purchasing the Premium Membership Card.</p>

						<p class="text-justify">Meet all your financial requirements by taking a personal loan at the best rate of interest, loan repayment tenure, and convenient EMIs - only via moneyupfinance.com.</p>
					</div>
				</div>

				<div class="row call-to-action call-to-action-skyblue p-20 m-t-30">
					<div class="col-md-9">
						<h3>Apply for Personal Loan Now</h3>
					</div>

					<div class="col-md-3 text-center">
						<a class="btn btn-dark" href="{{ url('digital/personal-loan') }}">Apply Now</a>
					</div>
				</div>

			</div>

			<div class="sidebar sticky-sidebar col-lg-3 col-md-3">
	<div class="widget">
		<h4 class="widget-title">Membership Cards</h4>
		<div><img src="{{asset('public/assets/images/slider/membership-card-premium.png') }}" alt="premium membership card" class="mw-100"></div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Topics</h4>

		<ul class="list list-arrow-icons m-b-0">
            <li> <a href="{{ url('personal-loan-for-self-employed') }}">Personal Loan for Self Employed</a> </li>
			<li> <a href="{{ url('personal-loan-for-cibil-defaulters') }}">Personal Loan for Cibil Defaulters</a> </li>
			<li> <a href="{{ url('personal-loan-balance-transfer') }}">Personal Loan Balance Transfer</a> </li>
			<li> <a href="{{ url('personal-loan-private-finance') }}">Personal Loan Private Finance</a> </li>
			<li> <a href="{{ url('personal-loan-for-nri') }}">Personal Loan for NRI</a> </li>
			<li> <a href="{{ url('pre-approved-personal-loan') }}">Pre-approved Personal Loan</a> </li>
			<li> <a href="{{ url('documents-required-for-personal-loan') }}">Documents Required for Personal Loan</a> </li>
			<li> <a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a> </li>
			<li> <a href="{{ url('top-up-personal-loan') }}">Top Up Personal Loan</a> </li>
			<li> <a href="{{ url('loan-agency-in-india') }}">Loan Agency in India</a> </li>
		</ul>
	</div>

	<div class="widget widget-tags m-b-30">
		<div class="tags">
            <a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi</a>
			<a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a>
			<a href="{{ url('personal-loan-in-kerala') }}">Kerala</a>
			<a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a>
			<a href="{{ url('personal-loan-in-pune') }}">Pune</a>
			<a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a>
			<a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a>
			<a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a>
		</div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Quick Links</h4>

		<ul class="list list-arrow-icons m-b-0">
            <li> <a href="{{ url('digital/personal-loan') }}">Customer Login</a> </li>
			<li> <a href="{{ url('premium-membership-card') }}">Purchase Membership Card</a> </li>
			<li> <a href="{{ url('company') }}">Our Media Coverage</a> </li>
			{{-- <li> <a href="{{ url('') }}">Business Loan EMI Calculator</a> </li> --}}
			<li> <a href="{{ url('/') }}">Earn with moneyupfinance</a> </li>
		</ul>
	</div>

</div>
		</div>
	</div>
</section>

@endsection
