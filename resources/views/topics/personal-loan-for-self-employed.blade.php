@extends('layouts.master')

@section('title','Personal loan for Self Employed | Apply now - moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-106.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Personal Loan for Self Employed</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('personal-loan-for-self-employed') }}">
			    <span itemprop="name">Personal Loan for Self Employed</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
		<div class="m-t-20 text-center">
			<a class="btn btn-dark btn-sm" href="{{ url('digital/personal-loan') }}">Apply Now</a>
		</div>
	</div>
</section>

<section class="p-t-30 sidebar-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">

				<div class="card">
	                <div class="card-body">
						<p class="text-justify">Self-employed personal loans are mainly for self employed customers, i.e., those running their own independent small/big business. For example, doctors, lawyers, owners of a grocery store, etc.</p>

						<p class="text-justify">Loans can be more helpful to make your dreams come true and meet a temporary financial need. Personal loans are pretty better than all other loans; it makes your life easier. You can use your loan amount in any way. You can shop for auspicious occasions coming home, You can go on vacation, and also buy things. Apply for a personal loan on moneyupfinance.com today for fulfilling your needs. Rest assured, moneyupfinance.com is a company that offers instant personal loans.</p>

						<h3>Advantages of Personal Loan for self employed</h3>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>Instant Loan approved of an amount up to INR 15 lakhs*</li>
							<li>Competitive and lowest interest rates</li>
							<li>Flexible repayment tenure - 12 to 60 months*</li>
							<li>Hassle-free and Quick documentation process</li>
							<li>Quick Online process & Instant loan disbursal</li>
						</ul>

						<div class="text-center m-t-20">
							<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-block">Get a Personal Loan</a>
						</div>
					</div>
				</div>


				<div class="card">
	                <div class="card-body">
						<h3>Loan eligibility for self employed</h3>

						<p class="text-justify">The required eligibility varies from bank to bank and NBFC. Mentioned below are the general eligibility criteria required for approval of the personal loan for the self-employed:</p>

						<ol>
							<li>The applicant should be a resident of India.</li>
							<li>The minimum age of the applicant at the time of application should be more than 21 years.</li>
							<li>The applicant should have a stable income/profit earning to pay the instalments.</li>
						</ol>

						<p>The minimum turnover or profit of the applicant should be as per the requirements of the lender.</p>

						<h4>Documents required for the Loan</h4>

						<p>Here's the list of Documents, required from the applicant's side:</p>

						<ol>
							<li>Proof of ID - Aadhar Card / Passport / Voter ID / PAN Card.</li>
							<li>Proof of age - Aadhaar card / Birth Certificate / Passport.</li>
							<li>Proof of Residence - Aadhar Card / Passport / Utility Bill / Allocation Letter.</li>
							<li>Evidence of Business - Evidence of Shop Establishment License, MSME Registration, Company Registration.</li>
							<li>Evidence of Income - Last Income Tax Return (ITR) with the calculation of income, last 3 years balance sheet, and profit and loss certified by CA.</li>
							<li>Proof of signature verification - Passport / PAN card.</li>
							<li>Bank Statement of last 12 months / Bank passbook of last 12 months.</li>
						</ol>
					</div>
				</div>


				<div class="card">
	                <div class="card-body">
						<p class="text-justify"><strong>Things to keep in mind when applying for personal loans with different banks and NBFCs for the self employed.</strong></p>

						<p class="text-justify"><strong>View interest rates:</strong> Always compare interest rates offered by different banks and NBFCs to suit your individual needs.</p>

						<p class="text-justify"><strong>Instalments / EMI:</strong> The next thing you should consider is the monthly EMI of loans provided by various banks and NBFCs.</p>

						<p class="text-justify"><strong>Time for loan approval:</strong> Normally, a person, can get loan approval within two or three working days.</p>

						<p class="text-justify"><strong>Turnaround time:</strong> Check and compare how much time the bank takes for processing and delivery? The duration of distribution is different in each bank.</p>

						<p class="text-justify"><strong>Processing fee for the Loan:</strong> You might need to pay a one-time fee to the bank. Always compare the banks' processing fees and opt for the lowest one. Typically, the processing charge is between 0.50% - 2.50% of the loan amount.</p>

						<p class="text-justify"><strong>Prepayment Fee:</strong> Generally, some banks charge you a prepayment fee, so you need to take that into consideration.</p>

						<p class="text-justify"><strong>Loan Term:</strong> It is essential to look at the term offered by each bank. Depending on your preference, compare before you choose a term.</p>

						<p><strong>Features and benefits of personal loans for self employed.</strong></p>

						<ul>
							<li>Lowest interest rate</li>
							<li>No collateral is required</li>
							<li>Transparency in loan processing</li>
							<li>Quick processing of loans</li>
							<li>Simple EMIs</li>
							<li>The loan amount can be used in any work</li>
						</ul>
					</div>
				</div>

				<div class="row call-to-action call-to-action-skyblue p-20 m-t-30">
					<div class="col-md-9">
						<h3>Apply for a premium membership card and get an instant loan offer</h3>
					</div>

					<div class="col-md-3 text-center">
						<a class="btn btn-dark" href="{{ url('digital/personal-loan') }}">Apply Now</a>
					</div>
				</div>

			</div>

			<div class="sidebar sticky-sidebar col-lg-3 col-md-3">
	<div class="widget">
		<h4 class="widget-title">Membership Cards</h4>
		<div><img src="{{ asset('public/assets/images/slider/membership-card-premium.png') }}" alt="premium membership card" class="mw-100"></div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Topics</h4>

		<ul class="list list-arrow-icons m-b-0">
			<li> <a href="{{ url('personal-loan-for-self-employed') }}">Personal Loan for Self Employed</a> </li>
			<li> <a href="{{ url('personal-loan-for-cibil-defaulters') }}">Personal Loan for Cibil Defaulters</a> </li>
			<li> <a href="{{ url('personal-loan-balance-transfer') }}">Personal Loan Balance Transfer</a> </li>
			<li> <a href="{{ url('personal-loan-private-finance') }}">Personal Loan Private Finance</a> </li>
			<li> <a href="{{ url('personal-loan-for-nri') }}">Personal Loan for NRI</a> </li>
			<li> <a href="{{ url('pre-approved-personal-loan') }}">Pre-approved Personal Loan</a> </li>
			<li> <a href="{{ url('documents-required-for-personal-loan') }}">Documents Required for Personal Loan</a> </li>
			<li> <a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a> </li>
			<li> <a href="{{ url('top-up-personal-loan') }}">Top Up Personal Loan</a> </li>
			<li> <a href="{{ url('loan-agency-in-india') }}">Loan Agency in India</a> </li>
		</ul>
	</div>

	<div class="widget widget-tags m-b-30">
		<div class="tags">
			<a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi</a>
			<a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a>
			<a href="{{ url('personal-loan-in-kerala') }}">Kerala</a>
			<a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a>
			<a href="{{ url('personal-loan-in-pune') }}">Pune</a>
			<a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a>
			<a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a>
			<a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a>
		</div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Quick Links</h4>

		<ul class="list list-arrow-icons m-b-0">
			<li> <a href="{{ url('digital/personal-loan') }}">Customer Login</a> </li>
			<li> <a href="{{ url('premium-membership-card') }}">Purchase Membership Card</a> </li>
			<li> <a href="{{ url('company') }}">Our Media Coverage</a> </li>
			{{-- <li> <a href="{{ url('') }}">Business Loan EMI Calculator</a> </li> --}}
			<li> <a href="{{ url('/') }}">Earn with moneyupfinance</a> </li>
		</ul>
	</div>

</div>
		</div>
	</div>
</section>

@endsection
