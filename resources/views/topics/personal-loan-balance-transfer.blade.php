@extends('layouts.master')

@section('title','Quick guide on Personal loan for CIBIL defaulters | moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-106.jpg">
	<div class="container">
		<div class="page-title">
			<h1>What is a Personal Loan balance transfer?</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('/') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="personal-loan-balance-transfer">
			    <span itemprop="name">Personal Loan Balance Transfer</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
		<div class="m-t-20 text-center">
			<a class="btn btn-dark btn-sm" href="digital/personal-loan">Apply Now</a>
		</div>
	</div>
</section>

<section class="p-t-30 sidebar-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">

				<div class="card">
	                <div class="card-body">
						<p class="text-justify">Personal balance transfer of loans is a method by which the borrower transfers the entire outstanding personal loan amount from one institution to another. This usually happens when the a bank lowers the interest rate (than the existing bank) on the due loan amount. The aim is to reduce interest rates.</p>

						<p class="text-justify">Your cash flow may be limited when an emergency strikes at any particular time. In such a case of desperation for money, you may take a personal loan at a higher rate.</p>

						<p class="text-justify">Once the immediate need for money is sorted, you can transfer the loan to another bank (offering a lower interest rate on the due loan) to reduce your EMI, to reduce the interest burden.</p>

						<p class="text-justify">Some documents are needed for a personal loan balance transfer. The loan agreement (if Required) will incur foreclosure charges, processing fees and stamp duty at a normal expense.</p>

						<div class="text-center m-t-20">
							<a href="digital/personal-loan" class="btn btn-outline btn-block">Get a Personal Loan</a>
						</div>
					</div>
				</div>


				<div class="card">
	                <div class="card-body">
						<h4>How Does a Personal Loan Balance Transfer Work?</h4>

						<p class="text-justify">In a personal loan transfer from one bank to another, your new bank pays off your existing loan. If your current loan comes with a prepayment clause, you may have to take that charge in such a case. Alternatively, you may have to pay a processing fee for your new loan. However, having a new loan will increase your savings and offset these costs with a lower interest rate.</p>

						<h4>Benefits of Loan Balance Transfer:</h4>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li><strong>Low-interest rates:</strong> The primary benefit of the balance transfer feature is that the interest rate is decreased, which decreases how much interest is charged to the borrower via EMI. In general, new lenders will offer lower interest rates on loan transfers.</li>
							<li><strong>Time of loan repayment:</strong> When a personal loan is transferred from one bank to another, the term of the existing personal loan can be reduced or extended. You can extend or shorten the loan repayment period depending on the requirements. The instalments are to be set accordingly.</li>
						</ul>

						<p><strong>Other features:</strong></p>
						<ul class="list-icon list-icon-check list-icon-colored">
							<li>This benefit records your previous payments</li>
							<li>Depending on your CIBIL score and constantly changing revenue dynamics</li>
							<li>Some lenders charge zero processing fees</li>
							<li>Low-interest rate</li>
							<li>It can a provide great feature like waiver of certain instalments. The personal loan balance transfer facility can reduce the interest on the personal loan and give the applicant better loan facilities in the deal.</li>
						</ul>

						<p><strong>Increase new loan facility:</strong></p>
						<ul>
							<li>Many banks offer top-up loans with personal loan transfers.</li>
							<li>Banks and Financial Institutions (NBFCs) offer new and top-up loans for competitive and low-interest rate personal loans.</li>
						</ul>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3>How does moneyupfinance help to get a long term loan?</h3>

						<ul class="list-icon list-icon-check list-icon-colored">
							<li>Personal loan of up to Rs. 15 lakhs in 30 minutes</li>
							<li>Get Instant personal loan offer from multiple banks</li>
							<li>Low annual percentage rate starting from 12.5%</li>
						</ul>

						<div class="row">
							<div class="col-md-6">
								<h5>Eligibility Criteria for Salaried Person</h5>
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Minimum salary - Rs. 15,000/- per month</li>
									<li>1 year job stability</li>
									<li>Minimum age - 21 years</li>
								</ul>
							</div>

							<div class="col-md-6">
								<h5>Eligibility Criteria for Self-Employed Person</h5>
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Minimum 1 year IT return</li>
									<li>1 year of business stability</li>
									<li>Minimum age - 21 years</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">How to proceed?</h3>

						<div class="row p-t-20">
							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>1.</h2></div>
									<p>Quick Registration</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>2.</h2></div>
									<p>Check Eligibility</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>3.</h2></div>
									<p>Buy Membership Card</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>4.</h2></div>
									<p>Submit Documents</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>5.</h2></div>
									<p>Get Sanctioned</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>6.</h2></div>
									<p>Receive funds</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">Why Apply for Instant Personal Loan Online at <a href="{{ url('/') }}">moneyupfinance.com</a>?</h3>

						<div class="row p-t-20">
							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-users"></i></div>
									<p>1000+ happy customers monthly</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-file-alt"></i></div>
									<p>Minimum documents required as per customer profile</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-university"></i></div>
									<p>Get Loan Offers from multiple banks - Loan up to Rs. 15,00,000/-*</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-percent"></i></div>
									<p>Annual percentage rate - minimum 11.5% and maximum 24%*</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-calendar"></i></div>
									<p>Flexible repayment terms of 2 to 6 years</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-hourglass-half"></i></div>
									<p>Low Processing fees - Depends on customer profile & banks' regulations</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">Membership Card Benefits</h3>

						<div class="row p-t-20">
							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>1.</h2></div>
									<p>Get pre approval loan offer in multiple banks from one platform</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>2.</h2></div>
									<p>Free loan consultancy & customer service for 10 years</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>3.</h2></div>
									<p>Up to 40% referral payout bonus</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>4.</h2></div>
									<p>Loan offers from multiple banks anytime - anywhere</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>5.</h2></div>
									<p>No effect on CIBIL even after multiple bank verification</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>6.</h2></div>
									<p>On-call Assistance on all your doubts</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row call-to-action call-to-action-skyblue p-20 m-t-30">
					<div class="col-md-9">
						<h3>Apply for a premium membership card and get an instant loan offer</h3>
					</div>

					<div class="col-md-3 text-center">
						<a class="btn btn-dark" href="digital/personal-loan">Apply Now</a>
					</div>
				</div>

				<h6>Note : The details of bank interest rates and requirements may vary.</h6>

			</div>

            <div class="sidebar sticky-sidebar col-lg-3 col-md-3">
                <div class="widget">
                    <h4 class="widget-title">Membership Cards</h4>
                    <div><img src="{{ asset('public/assets/images/slider/membership-card-premium.png') }}" alt="premium membership card" class="mw-100"></div>
                </div>

                <div class="widget clearfix widget-categories">
                    <h4 class="widget-title">Topics</h4>

                    <ul class="list list-arrow-icons m-b-0">
                        <li> <a href="{{ url('personal-loan-for-self-employed') }}">Personal Loan for Self Employed</a> </li>
                        <li> <a href="{{ url('personal-loan-for-cibil-defaulters') }}">Personal Loan for Cibil Defaulters</a> </li>
                        <li> <a href="{{ url('personal-loan-balance-transfer') }}">Personal Loan Balance Transfer</a> </li>
                        <li> <a href="{{ url('personal-loan-private-finance') }}">Personal Loan Private Finance</a> </li>
                        <li> <a href="{{ url('personal-loan-for-nri') }}">Personal Loan for NRI</a> </li>
                        <li> <a href="{{ url('pre-approved-personal-loan') }}">Pre-approved Personal Loan</a> </li>
                        <li> <a href="{{ url('documents-required-for-personal-loan') }}">Documents Required for Personal Loan</a> </li>
                        <li> <a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a> </li>
                        <li> <a href="{{ url('top-up-personal-loan.') }}">Top Up Personal Loan</a> </li>
                        <li> <a href="{{ url('loan-agency-in-india') }}">Loan Agency in India</a> </li>
                    </ul>
                </div>

                <div class="widget widget-tags m-b-30">
                    <div class="tags">
                        <a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi</a>
                        <a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a>
                        <a href="{{ url('personal-loan-in-kerala') }}">Kerala</a>
                        <a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a>
                        <a href="{{ url('personal-loan-in-pune') }}">Pune</a>
                        <a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a>
                        <a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a>
                        <a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a>
                    </div>
                </div>

                <div class="widget clearfix widget-categories">
                    <h4 class="widget-title">Quick Links</h4>

                    <ul class="list list-arrow-icons m-b-0">
                        <li> <a href="{{ url('customer-login') }}">Customer Login</a> </li>
                        <li> <a href="{{ url('premium-membership-card') }}">Purchase Membership Card</a> </li>
                        <li> <a href="{{ url('company') }}">Our Media Coverage</a> </li>
                        {{-- <li> <a href="{{ url('loan/calculator') }}">Business Loan EMI Calculator</a> </li> --}}
                        <li> <a href="{{ url('channel-partner-code') }}">Earn with moneyupfinance</a> </li>
                    </ul>
                </div>

            </div>
		</div>
	</div>
</section>

@endsection
