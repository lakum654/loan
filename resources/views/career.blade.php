@extends('layouts.master')


@section('main')
<section class="p-t-130 p-b-100" id="page-title"
            data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
            <div class="container">
                <div class="page-title">
                    <h1>Current Openings</h1>
                </div>
                <div class="breadcrumb">
                    <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a itemprop="item" href="{{ url('') }}">
                                <span itemprop="name">Home</span></a>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                            <a itemprop="item" href="{{ url('career') }}">
                                <span itemprop="name">Career</span></a>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section id="career">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-10">
                                    <h3 class="card-title text-primary">We are Hiring Google Ads Expert | Adajan Surat |
                                    </h3>
                                    <p><strong>We&rsquo;re Hiring Google Ads Expert |</strong></p>

                                    <p><strong>&nbsp;Adajan Surat&nbsp;</strong></p>

                                    <p>&nbsp;</p>

                                    <p>✅ &nbsp; <strong>Eligibility:</strong></p>

                                    <ul>
                                        <li>Experience: Minimum 1 Year</li>
                                        <li>Minimum Qualification &ndash; Graduate</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;✅ &nbsp; <strong>Job Role:</strong></p>

                                    <ul>
                                        <li>Plan, create and manage PPC campaigns.</li>
                                        <li>Be involved in keyword selection and audience targeting.</li>
                                        <li>Generating leads and sales using Google Adwords.</li>
                                        <li>Manage the strategy and setup of all paid campaigns.</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p><strong>✅ &nbsp; Note:</strong></p>

                                    <ul>
                                        <li>Final Selection depends on the candidate&rsquo;s skill &ndash; judged by the
                                            company once the interview is done.</li>
                                        <li>Salary Bar &ndash; Rs.20,000 To Rs.60,000&nbsp;per Month</li>
                                        <li>Job Timing &ndash; 10:00 AM To 7:00 PM (Monday to Saturday).</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p><strong>✅&nbsp; Company Location:</strong></p>

                                    <ul>
                                        <li>4001, Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p><strong>✅&nbsp; Interview Timing:</strong></p>

                                    <ul>
                                        <li>10:00 AM to 10:30 AM &nbsp;(Monday to Saturday).</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <ul>
                                    </ul>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ url('career/form-1') }}" class="btn btn-sm">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-10">
                                    <h3 class="card-title text-primary">We are Hiring | Telecaller | Adajan Surat</h3>
                                    <p><strong>We&rsquo;re Hiring | Telecaller | Adajan Surat</strong></p>

                                    <p>&nbsp;</p>

                                    <p>✅<strong> Telecaller Eligibility:</strong></p>

                                    <ul>
                                        <li>Clear Speech</li>
                                        <li>Good Communication Skills</li>
                                        <li>Minimum Qualification &ndash; 10th Pass</li>
                                        <li>Minimum Experience &ndash; Freshers Can Apply</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p>✅ <strong>Job Role:</strong></p>

                                    <ul>
                                        <li>Calling Customers and Informing them about the Company&#39;s
                                            Products/Services.</li>
                                        <li>Receive Calls and Solve Queries.</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p>✅ <strong>Note:</strong></p>

                                    <ul>
                                        <li>Final Selection and Incentive depend on the candidate&rsquo;s skill &ndash;
                                            judged by the company once the interview is done.</li>
                                        <li>Job Timing - 09:30 AM To 06:30 PM (Monday to Saturday).</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p><strong>✅ Company Location:</strong></p>

                                    <ul>
                                        <li>4001, Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009</li>
                                    </ul>

                                    <p>&nbsp;</p>

                                    <p>✅<strong> Interview Time:</strong></p>

                                    <ul>
                                        <li>10:00 AM To 10:30 AM&nbsp;&amp;&nbsp;5:00 PM&nbsp;to 5:30 PM&nbsp; (Monday
                                            To Saturday).</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ url('career/form-2') }}" class="btn btn-sm">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-10">
                                    <h3 class="card-title text-primary">We are Hiring | SEO Expert |</h3>
                                    <p><span style="font-size:14px"><strong>|| We are Hiring ||</strong></span></p>

                                    <p><br />
                                        <strong>1.&nbsp;&nbsp; &nbsp;SEO Expert</strong>
                                    </p>

                                    <p>✅ Roles &amp; Responsibilities :-<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Strong understanding of Search Engine Marketing (SEM)
                                        and Search Engine Optimization (SEO) process<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Knowledge of popular tools (Google analytics, google
                                        adwords, google search console)<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Using Google Analytics to conduct performance reports
                                        regularly.<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Creating high-quality SEO content.<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Creating Backlink, article and directory posting, press
                                        release&nbsp;<br />
                                        &bull;&nbsp;&nbsp; &nbsp;Knowledge of PPC programs and optimizing data gathered
                                        from both organic and paid sources.</p>

                                    <p>✅ Skills :-<br />
                                        &bull;&nbsp;&nbsp; &nbsp;2+ years experience in Search Engine Marketing (SEM)
                                        and Search Engine Optimization (SEO)</p>

                                    <p>✅ &nbsp; NOTE :-<br />
                                        &bull; &nbsp; &nbsp;Final Selection Depends on your Skills After appearing for
                                        an interview&nbsp;<br />
                                        &bull; &nbsp; &nbsp;Salary - 20,000/- To 30,000/- Per Month<br />
                                        &bull; &nbsp; &nbsp;Job Time - 10:00 AM TO 07:00 PM- Monday to Saturday.<br />
                                        &nbsp;<br />
                                        ✅ &nbsp;Interview Address :-<br />
                                        &bull; &nbsp; &nbsp; moneyupfinance - 4001, Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009<br />
                                        &nbsp;<br />
                                        ✅ &nbsp;Interview Time :-<br />
                                        &bull; &nbsp; &nbsp;10:00AM to 10:30AM &nbsp;( Monday to Saturday )</p>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ url('career/form-1') }}" class="btn btn-sm">Apply Now</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>
@endsection
