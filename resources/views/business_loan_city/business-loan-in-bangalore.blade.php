@extends('layouts.master')

@section('title','Business loan in Bangalore | Low interest rate - moneyupfinance')

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-107.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Unsecured Business Loan in Bangalore</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('business-loan-in-bangalore') }}">
			    <span itemprop="name">Business Loan in Bangalore</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
		<div class="m-t-20 text-center">
			<a class="btn btn-dark btn-sm" href="{{ url('digital/personal-loan') }}">Apply Now</a>
		</div>
	</div>
</section>

<section class="p-t-30 sidebar-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">

				<div class="card">
	                <div class="card-body">
						<p class="text-justify">The city of Bangalore is known as the "Silicon Valley of India" or the "IT capital of India" as most of the startups and Small IT businesses are registered in Bangalore. Many of these businesses often require a loan to run their operations, Acquiring startup costs, Working capital, Marketing expenses, etc.</p>

						<p class="text-justify">moneyupfinance provides customized small business loan offers in Bangalore which are collateral-free. Although many large companies or businesses can avail of collateral loans, most small-sized businesses cannot get these loans. So, there arises the need for an unsecured business loan, demanding no security. If you are looking for a business loan in Bangalore, then moneyupfinance is the best choice to go with as they are India's ace loan service provider.</p>

						<img src="{{ asset('public/assets/images/slider/city-bangalore-bg.png') }}" alt="Business Loan in Bangalore" class="img-fluid img-thumbnail m-b-20" />

						<h3>Documents & Eligibility</h3>

						<p>Below provided is a list of documents to apply for a business loan:</p>

						<div class="row">
							<div class="col-md-6">
								<h5>Small Business Person Eligibility Criteria</h5>
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Minimum age - 21 years</li>
									<li>Minimum 1 year IT return</li>
									<li>1 year business stability</li>
									<li>Identity proof and Address proof</li>
								</ul>
							</div>

							<div class="col-md-6">
								<h5>Company Audit Report Person Eligibility Criteria</h5>
								<ul class="list-icon list-icon-check list-icon-colored">
									<li>Minimum age - 21 years</li>
									<li>INR 1 crore plus yearly turnover</li>
									<li>Minimum 2 years audited report</li>
									<li>Identity proof and Address proof</li>
								</ul>
							</div>
						</div>

						<div class="text-center m-t-20">
							<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-block">Check your eligiblity</a>
						</div>

						<h3 class="m-t-40">Benefits of taking a Business Loan in Bangalore</h3>

						<ol class="text-justify">
							<li><strong>Lower Interest Rates:</strong><br/>
							Taking loan offers from moneyupfinance can prove advantageous as banks provide business loans with lower interest rates (depending upon the loan application), which can benefit the applicant in the future.</li>

							<li><strong>Get Loan Offer From Multiple Banks & NBFCs:</strong><br/>
							Applicants have various certified banks to apply for a personal or Business Loan, and it gives the applicant liberty to choose a convenient and easy bank option.</li>
						</ol>

						<div class="row m-t-30">
							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Interest Rate</h6>
									<h3>11.5% - 24%</h3>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Processing Fee</h6>
									<h3>1% - 1.5%</h3>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Loan Tenure</h6>
									<h3>2 to 6 years</h3>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Instalments</h6>
									<h3>Monthly</h3>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Insurance Rate</h6>
									<h3>1% - 2.5%</h3>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="icon-box center process p-10 m-5 w-100">
									<h6>Loan Amount</h6>
									<h3>Up to 1 Cr.</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">How it works?</h3>

						<div class="row p-t-20">
							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>1.</h2></div>
									<p>Quick Registration</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>2.</h2></div>
									<p>Check Eligibility</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>3.</h2></div>
									<p>Buy Membership Card</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>4.</h2></div>
									<p>Submit Documents</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>5.</h2></div>
									<p>Bank Verification</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>6.</h2></div>
									<p>Get Sanctioned</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">Why moneyupfinance.com?</h3>

						<div class="row p-t-20">
							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-users"></i></div>
									<p>Per month 1000+ happy customers</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-file-alt"></i></div>
									<p>Minimum documents - depending on customer profile</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-university"></i></div>
									<p>User can compare and select loan offers from multiple banks</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-percent"></i></div>
									<p>Lowest annual interest rate</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-calendar"></i></div>
									<p>Flexible repayment terms ranging from 1 to 6 years</p>
								</div>
							</div>

							<div class="col-lg-4 col-sm-12">
								<div class="icon-box effect small m-b-20">
									<div class="icon"><i class="fa fa-hourglass-half"></i></div>
									<p>Processing fees - Depends on customer profile & banks</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
	                <div class="card-body">
						<h3 class="text-center">Membership Card Benefits</h3>

						<div class="row p-t-20">
							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>1.</h2></div>
									<p>Get pre-approved loan offer from multiple banks at one platform</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>2.</h2></div>
									<p>Free loan consultancy & customer service for 10 years</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>3.</h2></div>
									<p>Up to 40% referral payout bonus</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>4.</h2></div>
									<p>Loan offers from multiple banks anytime - anywhere</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>5.</h2></div>
									<p>No effect on CIBIL even after multiple bank verification</p>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="icon-box small">
									<div class="icon text-center"><h2><i></i>6.</h2></div>
									<p>On-call Assistance on all your doubts</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row call-to-action call-to-action-olive p-20 m-t-30">
					<div class="col-md-9">
						<h3>Apply for a platinum membership card and get an instant loan offer</h3>
					</div>

					<div class="col-md-3 text-center">
						<a class="btn btn-dark" href="{{ url('digital/personal-loan') }}">Apply Now</a>
					</div>
				</div>

				<h6>FAQ for Business Loan in Bangalore - <a href="{{ url('faq') }}" target="_blank">Click here</a></h6>

			</div>

			<div class="sidebar sticky-sidebar col-lg-3 col-md-3">
	<div class="widget">
		<h4 class="widget-title">Membership Cards</h4>
		<div><img src="{{ asset('public/assets/images/slider/membership-card-platinum.png') }}" alt="platinum membership card" class="mw-100"></div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Topics</h4>

		<ul class="list list-arrow-icons m-b-0">
			<li> <a href="{{ url('personal-loan-for-self-employed') }}">Personal Loan for Self Employed</a> </li>
			<li> <a href="{{ url('personal-loan-for-cibil-defaulters') }}">Personal Loan for Cibil Defaulters</a> </li>
			<li> <a href="{{ url('personal-loan-balance-transfer') }}">Personal Loan Balance Transfer</a> </li>
			<li> <a href="{{ url('personal-loan-private-finance') }}">Personal Loan Private Finance</a> </li>
			<li> <a href="{{ url('personal-loan-for-nri') }}">Personal Loan for NRI</a> </li>
			<li> <a href="{{ url('pre-approved-personal-loan') }}">Pre-approved Personal Loan</a> </li>
			<li> <a href="{{ url('documents-required-for-personal-loan') }}">Documents Required for Personal Loan</a> </li>
			<li> <a href="{{ url('required-cibil-score-for-personal-loan') }}">Required Cibil Score for Personal Loan</a> </li>
			<li> <a href="{{ url('top-up-personal-loan.') }}">Top Up Personal Loan</a> </li>
			<li> <a href="{{ url('loan-agency-in-india') }}">Loan Agency in India</a> </li>
		</ul>
	</div>

	<div class="widget widget-tags m-b-30">
		<div class="tags">
			<a href="{{ url('personal-loan-in-delhi-ncr') }}">Delhi</a>
			<a href="{{ url('personal-loan-in-bangalore') }}">Bangalore</a>
			<a href="{{ url('personal-loan-in-kerala') }}">Kerala</a>
			<a href="{{ url('online-personal-loan-mumbai') }}">Mumbai</a>
			<a href="{{ url('personal-loan-in-pune') }}">Pune</a>
			<a href="{{ url('personal-loan-in-coimbatore') }}">Coimbatore</a>
			<a href="{{ url('personal-loan-in-kolkata') }}">Kolkata</a>
			<a href="{{ url('personal-loan-in-jaipur') }}">Jaipur</a>
		</div>
	</div>

	<div class="widget clearfix widget-categories">
		<h4 class="widget-title">Quick Links</h4>

		<ul class="list list-arrow-icons m-b-0">
			<li> <a href="{{ url('customer-login') }}">Customer Login</a> </li>
			<li> <a href="{{ url('premium-membership-card') }}">Purchase Membership Card</a> </li>
			<li> <a href="{{ url('company') }}">Our Media Coverage</a> </li>
			{{-- <li> <a href="{{ url('loan/calculator') }}">Business Loan EMI Calculator</a> </li> --}}
			<li> <a href="{{ url('channel-partner-code') }}">Earn with moneyupfinance</a> </li>
		</ul>
	</div>

</div>
		</div>
	</div>
</section>

@endsection
