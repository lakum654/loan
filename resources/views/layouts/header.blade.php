<header id="header" data-transparent="true" data-fullwidth="true" class="light submenu-light">
    <div class="header-inner">
        <div class="container">

            <div id="logo">
                <a href="{{ url('/') }}">
                    <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance"
                            width="120"></span>
                    <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}"
                            alt="moneyupfinance" width="120"></span>
                </a>
            </div>

            <div id="mainMenu-trigger">
                <a class="lines-button x"><span class="lines"></span></a>
            </div>

            <div id="mainMenu">
                <div class="container">
                    <nav>
                        <ul>
                            <li id="102" class=""><a href="{{ url('company') }}">Company</a></li>

                            <li id="103" class="dropdown"><a href="#">Our Products</a>
                                <ul class="dropdown-menu">
                                    <li id="1031" class=""><a href="{{ url('premium-membership-card') }}"><i
                                                class="fa fa-credit-credit"></i> Premium Membership Card</a>
                                    </li>
                                    <li id="1032" class=""><a href="{{ url('platinum-membership-card') }}"><i
                                                class="fa fa-credit-credit"></i> Platinum Membership Card</a>
                                    </li>
                                    {{-- <li id="1033" class=""><a href="{{ url('channel-partner-code') }}"><i
                                                class="fa fa-credit-credit"></i> Channel Partner Code</a></li> --}}
                                </ul>
                            </li>

                            <li id="109" class="dropdown"><a href="#">We Offer</a>
                                <ul class="dropdown-menu">
                                    <li id="1091" class=""><a href="{{ url('personal-loan') }}"><i
                                                class="fa fa-home"></i> Digital Personal Loan</a></li>
                                    <li id="1092" class=""><a href="{{ url('business-loan') }}"><i
                                                class="fa fa-building"></i> Digital Business Loan</a></li>
                                    {{-- <li id="1093" class=""><a href="{{ url('channel-partner') }}"><i
                                                class="fa fa-hands-helping"></i> Channel Partner</a></li> --}}
                                </ul>
                            </li>

                            <li id="109" class="dropdown"><a href="#">Apply Now</a>
                                <ul class="dropdown-menu">
                                    <li id="1091" class=""><a href="{{ url('digital/personal-loan') }}"><i
                                                class="fa fa-rupee-sign"></i> Personal Loan</a></li>
                                    <li id="1092" class=""><a href="{{ url('digital/personal-loan') }}"><i
                                                class="fa fa-rupee-sign"></i> Business Loan</a></li>
                                </ul>
                            </li>

                            <li id="108" class="dropdown"><a href="#">Login</a>
                                <ul class="dropdown-menu">
                                    <li id="1081" class=""><a href="{{ url('digital/personal-loan') }}"><i
                                                class="fa fa-sign-in-alt"></i> Customer Login</a></li>
                                    {{-- <li id="1082" class=""><a href="{{ url('channel-login') }}"><i
                                                class="fa fa-sign-in-alt"></i> Channel Partner Login</a></li> --}}
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</header>
