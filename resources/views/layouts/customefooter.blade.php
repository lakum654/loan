<footer id="footer" style="z-index:10;">
	<div class="copyright-content background-dark">
	    <div class="container">
	      <div class="copyright-text text-center text-light">
	          2022 &copy; moneyupfinance Service India Pvt. Ltd. All rights reserved.
	      </div>
	    </div>
	</div>
</footer>
