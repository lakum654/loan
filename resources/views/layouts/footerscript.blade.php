    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/60e6a10ed6e7610a49aa3344/1fa2ea8el';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    {{-- <script src="{{ asset('public/assets/js/jquery.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('public/assets/js/plugins.js') }}" type="text/javascript"></script>

    <script src="{{ asset('public/assets/js/functions.js ') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>

    <script src="{{ asset('public/assets/plugins/validate/form-validation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/plugins/validate/form-validation.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/plugins/bootstrap-switch/bootstrap-switch.min.js') }}" type="text/javascript">
    </script>

    <script src="{{ asset('public/assets/plugins/pageloader/pageloader.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/plugins/pageloader/pageloader.init.js') }}" type="text/javascript"></script>

    <script src="{{ asset('public/assets/plugins/rateit/jquery.rateit.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('public/assets/checkout/checkout.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $('#submitSubscribeForm').on('submit', function(e) {
                e.preventDefault();

                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#form-submit-subscribe').html(
                            "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>"
                        );
                        $('#form-submit-subscribe').attr('disabled', true);
                    },
                    success: function(response) {
                        if (response['success'] == true) {
                            document.getElementById("submitSubscribeForm").reset();
                            $.notify({
                                message: response['message']
                            }, {
                                type: 'success'
                            });
                        } else {
                            $.notify({
                                message: response['message']
                            }, {
                                type: 'danger'
                            });
                        }
                        $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                        $('#form-submit-subscribe').attr('disabled', false);
                    },
                    error: function(jXHR, textStatus, errorThrown) {
                        $.notify({
                            message: errorThrown
                        }, {
                            type: 'danger'
                        });
                        $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                        $('#form-submit-subscribe').attr('disabled', false);
                    }
                });
            });
        });
    </script>
