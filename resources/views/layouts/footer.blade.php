<footer id="footer" class="background-light">
    <div class="footer-content">
        <div class="container">
            <div class="row gap-y">
                <div class="col-md-4 col-sm-12">
                    <img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan"
                        class="m-t-20 m-b-30" style="max-width: 140px;">

                    <p class="m-b-20">Partnered with leading multiple banks & NBFCs, moneyupfinance is
                        India's #1 Personal & Business Loan Service Provider through Membership Cards. With
                        37,000+ Satisfied Customers & 949 Happy Partners, we strive to cater to people's
                        financial goals & aspirations.</p>

                    <h5><i class="fa fa-envelope"></i> SUBSCRIBE TO OUR LATEST UPDATES</h5>
                    <form action="" id="submitSubscribeForm"
                        class="row col-12" novalidate="novalidate" method="post" accept-charset="utf-8">
                        <div class="form-group col-10 p-0">
                            <input type="email" aria-required="true" name="subscribeemail" id="subscribeemail"
                                class="form-control" placeholder="Enter your Email" required>
                            <div class="help-block font-small-3"></div>
                        </div>

                        <div class="form-group col-2 p-0">
                            <button type="submit" id="form-submit-subscribe"
                                class="btn btn-block btn-secondary"><i class="fa fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>

                <div class="col-md-5 col-sm-12">
                    <h5 class="text-uppercase m-b-15">Useful Links</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-inline">
                                <li><a href="{{ url('company') }}"><i class="fa fa-info-circle m-r-5"></i> Company</a>
                                </li>
                                <li><a href="{{ url('personal-loan') }}"><i class="fa fa-desktop m-r-5"></i> Digital
                                        Personal Loan</a></li>
                                <li><a href="{{ url('business-loan') }}"><i class="fa fa-desktop m-r-5"></i> Digital
                                        Business Loan</a></li>
                                {{-- <li><a href="{{ url('partner/channel') }}"><i class="fa fa-user-tie m-r-5"></i> Channel
                                        Partner</a></li> --}}
                                <li><a href="{{ url('career') }}"><i class="fa fa-chart-line m-r-5"></i> Career</a>
                                </li>
                                <li><a href="" target="_blank"><i
                                            class="fa fa-table m-r-5"></i> Blog</a></li>
                                <li><a href="{{ url('faq') }}"><i class="fa fa-question-circle m-r-5"></i> FAQs</a>
                                </li>
                                <li><a href="{{ url('contactus') }}"><i class="fa fa-map m-r-5"></i> Contact Us</a></li>
                            </ul>
                        </div>

                        <div class="col-md-6">
                            <ul class="list-inline">
                                <li><a href="{{ url('digital/personal-loan') }}"><i class="fa fa-lock m-r-5"></i> Customer Login</a>
                                </li>
                                {{-- <li><a href="{{ url('channel-login') }}"><i class="fa fa-lock m-r-5"></i> Channel Partner
                                        Login</a></li> --}}
                                <li><a href="{{ url('important-update') }}"><i
                                            class="fa fa-exclamation-triangle m-r-5"></i> Important Update</a>
                                </li>
                                <li><a href="{{ url('sitemap') }}"><i class="fa fa-sitemap m-r-5"></i> Sitemap</a></li>
                                <li><a href="{{ url('privacy-policy') }}"><i class="fa fa-shield-alt m-r-5"></i>
                                        Privacy Policy</a></li>
                                <li><a href="{{ url('refund-policy') }}"><i class="fa fa-shield-alt m-r-5"></i> Return
                                        & Refund Policy</a></li>
                                <li><a href="{{ url('disclaimer') }}"><i class="fa fa-shield-alt m-r-5"></i>
                                        Disclaimer</a></li>
                                <li><a href="{{ url('terms-conditions') }}"><i class="fa fa-shield-alt m-r-5"></i>
                                        Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-12">
                    <h5 class="text-uppercase m-b-15">Contact Us</h5>
                    <div>
                        <p><a href="tel:+91 97237-97077"><i class="fa fa-phone m-r-5"></i> +91 97237-97077</a>
                        </p>
                        <p><a href="mailto:info@moneyupfinance.com"><i class="fa fa-envelope m-r-5"></i>
                                info@moneyupfinance.com</a></p>
                        <p><a href="mailto:partner@moneyupfinance.com"><i class="fa fa-envelope m-r-5"></i>
                                partner@moneyupfinance.com</a></p>
                        <p><i class="fa fa-gavel m-r-5"></i> CIN No.: U31101GJ2016PTC813691</p>
                        <p><i class="fa fa-map-marker m-r-5"></i> Registered Office: <br />4001,
                            <sup></sup>Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009</p>
                    </div>

                    <div class="social-icons social-icons-colored social-icons-rounded float-left">
                        <!-- <h6>Follow us:</h6> -->
                        <ul>
                            <li class="social-google"><a
                                    href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-google-plus-g"></i></a>
                            </li>

                            <li class="social-facebook"><a href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-facebook-f"></i></a></li>

                            <li class="social-instagram"><a href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a></li>

                            <li class="social-twitter"><a href="" target="_blank"
                                    rel="nofollow"><i class="fab fa-twitter"></i></a></li>

                            <li class="social-linkedin"><a href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-linkedin"></i></a></li>

                            <li class="social-pinterest"><a href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-pinterest"></i></a></li>

                            <li class="social-youtube"><a
                                    href=""
                                    target="_blank" rel="nofollow"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-content background-dark">
        <div class="container">
            <div class="copyright-text text-center text-light">
                2022 &copy; moneyupfinance Service India Pvt. Ltd. All rights reserved.
            </div>
        </div>
    </div>
</footer>
