<head>
    <link href="{{ asset('public/assets/plugins/bootstrap-switch/bootstrap-switch.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('public/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/css/validation/form-validation.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/plugins/rateit/rateit.css') }}" rel="stylesheet" type="text/css" />
  <link rel="icon" type="image/x-icon" href="{{ asset('public/assets/logo/logo-f.png') }}">

    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans&amp;family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161757010-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-161757010-1');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 418840398 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-418840398"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-418840398');
    </script>

    <!-- Facebook Domain + Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            '../connect.facebook.net/en_US/fbevents.js');
        fbq('init', '490578892301965');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=490578892301965&amp;ev=PageView&amp;noscript=1" /></noscript>
    <!-- End Facebook Domain + Pixel Code -->

    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "moneyupfinance.com",
            "alternateName": "moneyupfinance Service India Pvt. Ltd.",
            "url": "https://nowofloan.com/",
            "logo": "https://nowofloan.com/assets/images/logo-large.png",
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+919081545972",
                "contactType": "customer service",
                "areaServed": "IN",
                "availableLanguage": ["en", "Hindi", "Gujarati"]
            },
            "sameAs": [
                "https://www.facebook.com/nowofloan.in/",
                "https://twitter.com/nowofloan/",
                "https://www.instagram.com/nowofloan.in/",
                "https://www.linkedin.com/company/nowofloan/",
                "https://in.pinterest.com/nowofloan/",
                "https://www.youtube.com/channel/UC-YuwjTXkBlhazMqhqS-m2w"
            ]
        }
    </script>

    <script id="mcjs">
        ! function(c, h, i, m, p) {
            m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m,
                p)
        }(document, "script",
            "../chimpstatic.com/mcjs-connected/js/users/c7b98423fac62ff5c0187aa80/68ff10d3e4181a0f8dee72111.js");
    </script>
</head>
