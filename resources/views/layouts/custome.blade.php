
<header id="header" data-transparent="true" data-fullwidth="true" class="submenu-light header-disable-fixed">
    <div class="header-inner">
        <div class="container">

            <div id="logo">
                <a href="{{ url('/') }}">
                    <span class="logo-default"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan"
                            width="120"></span>
                    <span class="logo-dark"><img src="{{ asset('public/assets/logo/logo-f.png') }}" alt="moneyupfinance Loan"
                            width="120"></span>
                </a>
            </div>

            <div class="header-extras">
                <div class="p-dropdown">
                    <a class="x"><span class="lines"></span></a>
                    <ul class="p-dropdown-content">
                        <li><a href="tel:+91 97237-97077"><i class="icon-phone-call"></i>+91 97237-97077</a></li>
                        <li><a href="mailto:info@moneyupfinance.com"><i class="icon-mail"></i>info@moneyupfinance.com</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</header>
