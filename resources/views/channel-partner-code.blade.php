@extends('layouts.master')

@section('title', '')
@section('main')
    <div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
        <div class="slide background-columbia-blue">
            <div class="container">
                <div class="slide-captions row">
                    <div class="col-lg-6 col-md-6 col-12 align-self-center">
                        <h1 class="text-medium">Become a Loan Agent (Loan DSA) - Refer & Earn with moneyupfinance Channel
                            Partner</h1>
                        <p>Generate a great source of potential income through the easy sharing of your unique referral
                            links.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <img src="{{ asset('public/assets/images/slider/partner-code-banner.png') }}" alt="channel partner membership card"
                            class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="heading-text heading-line text-center p-b-5">
                        <h2 class="text-medium font-weight-500">About Channel Partner Code</h2>
                    </div>

                    <p>Meant for everyone who intends to build a good income source, moneyupfinance's Channel Partner Program
                        enables you to work from anywhere independently - all you need to do is sharing of your unique
                        referral link (provided by the company) to your network of people. Through this, you can earn a
                        handsome commission of up to 60% per membership card.</p>

                    <a href="{{ url('partner/channel') }}"
                        class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Register Now</span><i
                            class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>


    <section class="background-grey">
        <div class="container">
            <div class="heading-text heading-line text-center p-b-5">
                <h2 class="text-medium font-weight-500">Channel Partner Code - Become a Loan Agent or Loan DSA</h2>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-12 text-justify">
                    <p>Presented by moneyupfinance, the Channel Partner Code is a very benefitting program for people who want to
                        generate a very handsome side-income or a potentially sound full-time income just through referrals
                        - without leaving your current job or business. By becoming our Channel Partner or Loan DSA, all you
                        would need to do is help us provide our membership cards to people through your unique referral
                        link. We provide a 60% payout for every membership card sale, and you will also be paid the decided
                        commissions as per the banks or NBFCs on every loan approval. A Loan DSA can earn an average of INR
                        3 Lakhs per month through our Channel Partner Program. </p>
                </div>

                <div class="col-lg-6 col-md-6 col-12 text-justify">
                    <p>We pay commissions to our Channel Partners on each loan file that they send successfully. This is a
                        referral program for those who have an extensive network of individuals who want to get a loan but
                        are not connected to any bank or NBFC. You can join our Channel Partner program and upload files of
                        the loan seekers of your network despite you being less knowledgeable or having limited
                        connectivity.</p>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="heading-text heading-line text-center p-b-5">
                <h2 class="text-medium font-weight-500">Fast & Easy Application Process</h2>
            </div>

            <div class="row">
                <div class="col-md-3 text-center m-b-20">
                    <h3 class="text-lg text-grey">01</h3>
                    <h4>Quick registration</h4>
                </div>
                <div class="col-md-3 text-center m-b-20">
                    <h3 class="text-lg text-grey">02</h3>
                    <h4>Submit application form</h4>
                </div>
                <div class="col-md-3 text-center m-b-20">
                    <h3 class="text-lg text-grey">03</h3>
                    <h4>Free partner code</h4>
                </div>
                <div class="col-md-3 text-center m-b-20">
                    <h3 class="text-lg text-grey">04</h3>
                    <h4>Start business</h4>
                </div>
            </div>

            <div class="row p-t-30">
                <div class="col-12 text-center">
                    <h5>Below is an example of how the Channel Partner's Refer & Earn Program works:</h5>
                    <p>Rajesh is a moneylender that provides unorganized and unsecured money lending to people living within
                        his vicinity. However, the people around him are more well-known and inclined to accept loans from
                        banks, instead of moneylenders like Rajesh. A thing stopping them is they don't know the proper
                        procedure, so they ask Rajesh how to process their loans through a bank. If Rajesh doesn't have a
                        DSA or any code from NBFC Banks, he can become a moneyupfinance Channel Partner and work under our
                        company through his Channel Partner Code to earn extra income via refer and earn.</p>
                </div>
            </div>
        </div>
    </section>


    <section class="background-grey">
        <div class="container">
            <div class="heading-text heading-line text-center p-b-5">
                <h2 class="text-medium font-weight-500">Why moneyupfinance.com?</h2>
            </div>
            <p class="text-center">The Channel Partner Program is a very impressive and beneficial idea for the organic
                development of an individual and their group. This loan-partner program provides an opportunity for those
                looking to make additional income with Zero investment over a long time. It's a life-long opportunity with
                backup support from moneyupfinance. If you're looking to earn up to INR 3 Lakhs more per month, then becoming a
                moneyupfinance's Loan DSA is the ideal choice.</p>

            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="icon-box effect center process border-top-orange w-100">
                        <h3>High Return, Zero Investment</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="icon-box effect center process border-top-orange w-100">
                        <h3>Up to INR 3 Lakhs Earning Opportunity</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="icon-box effect center process border-top-orange w-100">
                        <h3>Lifetime Career Opportunity</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="icon-box effect center process border-top-orange w-100">
                        <h3>Free Marketing Support</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
