@extends('layouts.master')


@section('main')


<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Contact Us</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('contactus') }}">
			    <span itemprop="name">Contact</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			</ul>
		</div>
	</div>
</section>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 m-b-20">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Get in touch</h4>
				</div>
			</div>

			<div class="col-lg-5 m-b-30">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29761.216022640227!2d72.76891717389438!3d21.18611981357355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04dc8d31c5bbd%3A0x1e7ba3645758135a!2smoney%20up%20finance!5e0!3m2!1sen!2sin!4v1647886400923!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>

			<div class="col-lg-7">
				<form action="https://nowofloan.com/infopage/contactsubmission" id="contactForm" class="form-horizontal" novalidate="novalidate" method="post" accept-charset="utf-8">

					<div class="row">
						<div class="form-group col-md-6">
							<label class="text-dark" for="fullname">Full Name *</label>
							<input type="text" aria-required="true" name="fullname" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>
						<div class="form-group col-md-6">
							<label class="text-dark" for="email">Mobile no *</label>
							<input type="text" aria-required="true" name="mobile" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label class="text-dark" for="subject">Subject</label>
							<input type="text" name="subject" class="form-control">
						</div>
						<div class="form-group col-md-6">
							<label class="text-dark" for="email">Email id *</label>
							<input type="email" aria-required="true" name="email" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="text-dark" for="message">Message *</label>
						<textarea name="message" rows="5" aria-required="true" class="form-control" maxlength="500" required></textarea>
						<div class="help-block font-small-3"></div>
					</div>

					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" aria-required="true" name="conditions" id="conditions" class="custom-control-input" value="1" required>
							<label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the <a href="terms-conditions.html" target="_blank">Terms of Use</a>, <a href="privacy-policy.html" target="_blank">Privacy Policy</a> and this consent will override any registration by me for DNC / NDNC.</small></label>
							<div class="help-block font-small-3"></div>
						</div>
					</div>

					<button type="submit" id="submit-btn" class="btn btn-primary">Send Message</button>
				</form>
			</div>
		</div>
	</div>
</section>

@endsection
