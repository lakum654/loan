@extends('layouts.master')
@section('title','Apply for an Instant personal loan | Premium Membership - moneyupfinance')
@section('main')
<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
	<div class="slide background-eggshell">
		<div class="container">
			<div class="slide-captions row">
				<div class="col-lg-6 col-md-6 col-12 align-self-center">
					<h1 class="text-medium">Instant Personal Loan through Premium Membership Card</h1>
					<h4>Digital Personal Loan straight from your mobile - Easy Loan Approvals.</h4>
					<p>Get Premium Membership and apply for a personal loan from our Certified NBFCs for free.</p>
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<img src="{{ asset('public/assets/images/slider/premium-card-banner.png') }}" alt="premium membership card" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-12 text-center">
				<div class="heading-text heading-line text-center p-b-5">
					<h2 class="text-medium font-weight-500">About Premium Membership Card</h2>
				</div>

				<p>Through Premium Membership Cards, you can apply for an Instant Personal Loan for free from this portal itself - without making multiple visits to the banks. On moneyupfinance, customers can apply for a digital personal loan from multiple banks. We run your documents through various certified banks and NBFCs to provide you with attractive Interest rates with minimum paperwork. It leads the customer to get a quick approval within 30 minutes*.</p>

				<h3>Rs. <del class="text-danger">3999.00</del> <span class="text-success">999.00</span> only</h3>
				<a href="{{ url('digital/personal-loan') }}" class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Buy Now</span><i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center p-b-5">
			<h2 class="text-medium font-weight-500">Premium Membership Card in applying for Instant Personal Loan</h2>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>The loan documents you have submitted are attached to your profile and shared with our partnered banks and NBFCs. They will check your profile and verify whether your loan application meets their eligibility requirements for loan approval or not. After this, moneyupfinance will provide you with the list of NBFCs & Banks that have approved your loan application. It will help you save a significant amount of time, which otherwise would have been wasted in visiting multiple banks to apply for a loan. That's one of the main benefits of our Membership Card since taking out a loan is an extremely time-consuming procedure. In the daily rat race, people do not have time to go to multiple banks for loan processing; therefore, we offer this imperative facility - all in one place.</p>
			</div>

			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>Moreover, If you go by the conventional way and submit your loan file directly to the bank, the CIBIL score is affected each time. But with moneyupfinance, you can make sure that your account will be accessible to the banks in which your profile is compatible, assisting you to get a higher CIBIL score, even if you are not eligible to get the loan. moneyupfinance helps to apply for a Personal Loan whether you're self-employed or salaried person. We also help in applying for a loan with or without documents. moneyupfinance also helps in loans for CIBIL defaulters.</p>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="heading-text heading-line text-center p-b-5">
			<h3 class="text-medium font-weight-500">Benefits of premium membership card in applying for a personal loan</h3>
		</div>

		<div class="row icon-boxes">
			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-clock"></i>
				<div class="icon-box-content">
					<h3>Save Your Time, Money & Effort</h3>
					<p>You don’t need to waste time & money in visiting several banks for loans. Apply in multiple banks easily @ just ₹999</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-calendar"></i>
				<div class="icon-box-content">
					<h3>10 Years of Free Expert Consultancy</h3>
					<p>We'll help you out in improving your loan application and suggest the best steps for easy loan approval through on-call assistance</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-server"></i>
				<div class="icon-box-content">
					<h3>Apply For Loans in Multiple Banks @ Single Platform</h3>
					<p>Instead of getting 1 bank/agent support at a time, you get multiple banks' support with us – letting you get loan offers from multiple banks</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-file-text"></i>
				<div class="icon-box-content">
					<h3>No Effect On Your CIBIL Score</h3>
					<p>We submit your loan application only to those banks whose loan eligibility criteria matches your application, not impacting your CIBIL</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-grid"></i>
				<div class="icon-box-content">
					<h3>Get Your Personalized Portal</h3>
					<p>You can track your loan status and notifications easily while sitting at your home and get timely updates</p>
				</div>
			</div>

			<div class="icon-boxx col-md-4 col-12">
				<i class="icon-percent"></i>
				<div class="icon-box-content">
					<h3>Easy Refer & Earn Up To 40% Payout</h3>
					<p>Start earning easy income through our refer and earn program. Get up to 40% commission for every card sold through your referral link</p>
				</div>
			</div>
		</div>

		<div class="row m-t-20">
			<div class="col-md-12 col-12 text-center m-t-20">
				<a href="{{ url('membership-card-benefits') }}" class="btn btn-primary btn-sm">Know Detailed Benefits</a>
			</div>
		</div>
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center p-b-5">
			<h4 class="text-medium font-weight-500">How it works?</h4>
		</div>

		<div class="row">
			<div class="col-md-12 col-12">
				<div class="tabs tabs-vertical">
					<div class="row">
						<div class="col-md-4">
							<ul class="nav flex-column nav-tabs" id="myTab4" role="tablist" aria-orientation="vertical">
								<li class="nav-item"> <a class="nav-link active" id="point1-tab" data-toggle="tab" href="#point1" role="tab" aria-controls="home" aria-selected="true"><span class="badge badge-dark">1</span> Quick Registration</a> </li>

								<li class="nav-item"> <a class="nav-link" id="point2-tab" data-toggle="tab" href="#point2" role="tab" aria-controls="profile" aria-selected="false"><span class="badge badge-dark">2</span> Check Eligibility</a> </li>

								<li class="nav-item"> <a class="nav-link" id="point3-tab" data-toggle="tab" href="#point3" role="tab" aria-controls="profile" aria-selected="false"><span class="badge badge-dark">3</span> Buy Membership Card</a> </li>

								<li class="nav-item"> <a class="nav-link" id="point4-tab" data-toggle="tab" href="#point4" role="tab" aria-controls="profile" aria-selected="false"><span class="badge badge-dark">4</span> Submit Document</a> </li>

								<li class="nav-item"> <a class="nav-link" id="point5-tab" data-toggle="tab" href="#point5" role="tab" aria-controls="profile" aria-selected="false"><span class="badge badge-dark">5</span> Bank Verification</a> </li>

								<li class="nav-item"> <a class="nav-link" id="point6-tab" data-toggle="tab" href="#point6" role="tab" aria-controls="profile" aria-selected="false"><span class="badge badge-dark">6</span> Bank Sanction</a> </li>
							</ul>
						</div>

						<div class="col-md-8">
							<div class="tab-content p-t-40" id="myTabContent4">
								<div class="tab-pane fade show active" id="point1" role="tabpanel" aria-labelledby="point1-tab">
									<h4>Quick Registration</h4>
									<ul class="list-icon list-icon-check">
										<li>Search on google moneyupfinance.COM or visit a website <a href="{{ url('') }}" target="_blank">https://moneyupfinance.com</a></li>
										<li>Click on Digital Personal Loan.</li>
										<li>Register your bank register name and mobile number.</li>
										<li>If you have already registered then click on the Login option.</li>
										<li>If you want to calculate loan EMI and get its details then click on the Loan Calculator. After that click on Apply Now.</li>
									</ul>
								</div>

								<div class="tab-pane fade show" id="point2" role="tabpanel" aria-labelledby="point2-tab">
									<h4>Check Eligibility</h4>
									<ul class="list-icon list-icon-check">
										<li>Fill up the given details such as CIBIL score, city, loan purpose, income, monthly EMI (in case of an existing loan), any bounce EMI in the last 6 months. Then click on Check Eligibility.</li>
										<li>Our automated system will show you the eligibility based on your details and income. Here, only the pre-approval will be shown based on your details, it will not be your final approval.</li>
										<li>Now after checking your eligibility, select the tenure and EMI and click on Get Offer.</li>
									</ul>
								</div>

								<div class="tab-pane fade show" id="point3" role="tabpanel" aria-labelledby="point3-tab">
									<h4>Buy Membership Card</h4>
									<ul class="list-icon list-icon-check">
										<li>Our portal will show you the membership card with customer name with unique code. Click on Buy Now for getting a loan offer and membership card.</li>
										<li>Enter your phone number and email-id; then click on Proceed.</li>
										<li>In the next step, the portal will show you multiple payment options. You can pay with any suitable payment option from there.</li>
									</ul>
								</div>

								<div class="tab-pane fade show" id="point4" role="tabpanel" aria-labelledby="point4-tab">
									<h4>Submit Document</h4>
									<ul class="list-icon list-icon-check">
										<li>After successful payment, you'll receive a receipt and pasword on your email id.</li>
										<li>You will get a call from the login department in 24-48 hours after payment for document verification.</li>
										<li>Customer has to submit their documents within 3 days through whatsapp number or email-id, on the provided number or E-mail.</li>
										<li>Document verification is compulsory as our company is not able to proces loan without it.</li>
									</ul>
								</div>

								<div class="tab-pane fade show" id="point5" role="tabpanel" aria-labelledby="point5-tab">
									<h4>Bank Verification</h4>
									<ul class="list-icon list-icon-check">
										<li>Your documents will be sent to multiple NBFC banks for verification.</li>
										<li>Loan approval directly depends on the documents you provide.</li>
										<li>Then, NBFC banks will match the documents as per the required parameters.</li>
									</ul>
								</div>

								<div class="tab-pane fade show" id="point6" role="tabpanel" aria-labelledby="point6-tab">
									<h4>Bank Sanction</h4>
									<ul class="list-icon list-icon-check">
										<li>Bank will provide loan sanction only when your profile matches the required parameters.</li>
										<li>After a sanction letter, the customer needs to sign an agreement letter and then the funds will be credited to the customer's bank account in 3 to 4 working days.</li>
										<li>After final loan approval, the amount will be directly credited to the provided customer bank account.</li>
									</ul>
								</div>

							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

		<div class="row m-t-50">
			<div class="col-md-12 col-12">
				<p><small><strong>Disclaimer:</strong> The Premium Membership Card is not any sort of credit or debit card and the customer should not be under the impression that buying this card means getting money in the bank.  Membership Card is limited to our company only, providing certain benefits. T&C applied*.</small></p>
			</div>
		</div>
	</div>
</section>

@endsection
