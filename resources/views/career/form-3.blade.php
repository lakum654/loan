@extends('layouts.master')

@section('title', 'Kickstart your Career at moneyupfinance.com');

@section('main')

    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Career</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('career') }}">
                            <span itemprop="name">Career</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('career/form-3') }}">
                            <span itemprop="name">Current Opening</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <div class="heading-text heading-line">
                        <h4>We are Hiring | SEO Expert |</h4>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-12">
                    <form action="https://nowofloan.com/apply/careerSubmission" id="submitForm2" class="p-cb"
                        enctype="multipart/form-data" novalidate="novalidate" method="post" accept-charset="utf-8">
                        <input type="hidden" name="id" value="9" class="form-control" required>
                        <input type="hidden" name="slug" value="038912" class="form-control" required>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="text-dark" for="firstname">First Name</label>
                                <input type="text" aria-required="true" name="firstname" id="firstname"
                                    class="form-control" required>
                                <div class="help-block font-small-3"></div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="text-dark" for="lastname">Last Name</label>
                                <input type="text" name="lastname" id="lastname" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="text-dark" for="mobile">Mobile</label>
                                <input type="text" aria-required="true" name="mobile" id="mobile" class="form-control"
                                    required maxlength="10" data-validation-regex-regex="^[6789]\d{9}$">
                                <div class="help-block font-small-3"></div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="text-dark" for="emailid">Email Id</label>
                                <input type="text" aria-required="true" name="emailid" id="emailid" class="form-control"
                                    required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="text-dark" for="city">City</label>
                                <input type="text" aria-required="true" name="city" id="city" class="form-control"
                                    required>
                                <div class="help-block font-small-3"></div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="text-dark" for="qualifications">Qualifications</label>
                                <input type="text" aria-required="true" name="qualifications" id="qualifications"
                                    class="form-control" required>
                                <div class="help-block font-small-3"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="text-dark" for="experience">Experience</label>
                            <textarea aria-required="true" name="experience" id="experience" class="form-control"
                                required></textarea>
                            <div class="help-block font-small-3"></div>
                        </div>

                        <div class="form-group">
                            <label class="text-dark" for="keyskills">Key Skills</label>
                            <textarea aria-required="true" name="keyskills" id="keyskills" class="form-control"
                                required></textarea>
                            <div class="help-block font-small-3"></div>
                        </div>

                        <div class="form-group">
                            <label class="text-dark" for="resume">Upload Resume</label>
                            <input type="file" aria-required="true" name="resume" id="resume" class="form-control"
                                required accept=".doc,.docx,.txt,.pdf">
                            <div class="help-block font-small-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" aria-required="true" name="conditions" id="conditions"
                                    class="custom-control-input" value="1" required>
                                <label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the
                                        <a href="../../terms-conditions.html" target="_blank">Terms of Use</a>, <a
                                            href="../../privacy-policy.html" target="_blank">Privacy Policy</a> and this
                                        consent will override any registration by me for DNC / NDNC.</small></label>
                                <div class="help-block font-small-3"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" id="submit-btn2" class="btn btn-secondary">APPLY NOW</button>
                        </div>

                        <div class="form-group" id="applymessage"></div>
                    </form>
                </div>

                <div class="col-lg-5 col-md-5 col-12 pt-4">
                    <p><span style="font-size:14px"><strong>|| We are Hiring ||</strong></span></p>

                    <p><br />
                        <strong>1.&nbsp;&nbsp; &nbsp;SEO Expert</strong>
                    </p>

                    <p>✅ Roles &amp; Responsibilities :-<br />
                        &bull;&nbsp;&nbsp; &nbsp;Strong understanding of Search Engine Marketing (SEM) and Search Engine
                        Optimization (SEO) process<br />
                        &bull;&nbsp;&nbsp; &nbsp;Knowledge of popular tools (Google analytics, google adwords, google search
                        console)<br />
                        &bull;&nbsp;&nbsp; &nbsp;Using Google Analytics to conduct performance reports regularly.<br />
                        &bull;&nbsp;&nbsp; &nbsp;Creating high-quality SEO content.<br />
                        &bull;&nbsp;&nbsp; &nbsp;Creating Backlink, article and directory posting, press release&nbsp;<br />
                        &bull;&nbsp;&nbsp; &nbsp;Knowledge of PPC programs and optimizing data gathered from both organic
                        and paid sources.</p>

                    <p>✅ Skills :-<br />
                        &bull;&nbsp;&nbsp; &nbsp;2+ years experience in Search Engine Marketing (SEM) and Search Engine
                        Optimization (SEO)</p>

                    <p>✅ &nbsp; NOTE :-<br />
                        &bull; &nbsp; &nbsp;Final Selection Depends on your Skills After appearing for an
                        interview&nbsp;<br />
                        &bull; &nbsp; &nbsp;Salary - 20,000/- To 30,000/- Per Month<br />
                        &bull; &nbsp; &nbsp;Job Time - 10:00 AM TO 07:00 PM- Monday to Saturday.<br />
                        &nbsp;<br />
                        ✅ &nbsp;Interview Address :-<br />
                        &bull; &nbsp; &nbsp; Nowofloan - 4001, Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009<br />
                        &nbsp;<br />
                        ✅ &nbsp;Interview Time :-<br />
                        &bull; &nbsp; &nbsp;10:00AM to 10:30AM &nbsp;( Monday to Saturday )</p>
                </div>

            </div>
        </div>
    </section>


    <script type="text/javascript">
        $(function(){
            $('#submitSubscribeForm').on('submit', function(e) {
                e.preventDefault();

                $.ajax({
                    url : $(this).attr('action'),
                    type: "POST",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    cache: false,
                    processData: false,
                    beforeSend: function(){
                        $('#form-submit-subscribe').html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>");
                                $('#form-submit-subscribe').attr('disabled', true);
                    },
                    success: function (response) {
                      if(response['success'] == true) {
                          document.getElementById("submitSubscribeForm").reset();
                          $.notify({ message: response['message'] },{ type: 'success' });
                      } else {
                        $.notify({ message: response['message'] },{ type: 'danger' });
                      }
                      $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                      $('#form-submit-subscribe').attr('disabled', false);
                    },
                    error: function (jXHR, textStatus, errorThrown) {
                      $.notify({ message: errorThrown },{ type: 'danger' });
                      $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                      $('#form-submit-subscribe').attr('disabled', false);
                    }
                });
            });
        });
      </script>
      <script type="text/javascript">
        $(function(){
            $('#submitForm2').on('submit', function(e) {
                  e.preventDefault();
                  var formData = new FormData($(this)[0]);

                  $.ajax({
                         url : $(this).attr('action'),
                      type: "POST",
                      data: formData,
                      async: true,
                      dataType: "JSON",
                      cache: false,
                      contentType: false,
                      processData: false,
                      beforeSend: function(){
                          $('#submit-btn2').html('Document uploading... <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                          $('#submit-btn2').attr('disabled', true);
                      },
                      success: function (response) {
                        if(response['success'] == true) {
                            document.getElementById("submitForm2").reset();
                        }

                        document.getElementById("applymessage").innerHTML = response['message'];
                        $.notify({ message: response['message'] },{ type: 'success' });
                        $('#submit-btn2').html('APPLY NOW');
                        $('#submit-btn2').attr('disabled', false);
                      },
                      error: function (jXHR, textStatus, errorThrown) {
                          $('#submit-btn2').html('APPLY NOW');
                          $('#submit-btn2').attr('disabled', false);
                          $.notify({ message: errorThrown },{ type: 'danger' });
                      }
                  });
              });
        });
      </script>
@endsection
