@extends('layouts.master')

@section('title','Kickstart your Career at moneyupfinance.com');

@section('main')

<section class="p-t-130 p-b-100" id="page-title" data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-104.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Career</h1>
		</div>
		<div class="breadcrumb">
			<ul itemscope itemtype="https://schema.org/BreadcrumbList">
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('') }}">
			    <span itemprop="name">Home</span></a>
			    <meta itemprop="position" content="1" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('career') }}">
			    <span itemprop="name">Career</span></a>
			    <meta itemprop="position" content="2" />
			  </li>
			  <li itemprop="itemListElement" itemscope
			      itemtype="https://schema.org/ListItem">
			    <a itemprop="item" href="{{ url('career/form-2') }}">
			    <span itemprop="name">Current Opening</span></a>
			    <meta itemprop="position" content="3" />
			  </li>
			</ul>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">

			<div class="col-lg-12 col-md-12">
				<div class="heading-text heading-line">
					<h4>We are Hiring | Telecaller | Adajan Surat</h4>
				</div>
			</div>

			<div class="col-lg-7 col-md-7 col-12">
				<form action="https://nowofloan.com/apply/careerSubmission" id="submitForm2" class="p-cb" enctype="multipart/form-data" novalidate="novalidate" method="post" accept-charset="utf-8">
					<input type="hidden" name="id" value="1" class="form-control" required>
					<input type="hidden" name="slug" value="sdv54a" class="form-control" required>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="text-dark" for="firstname">First Name</label>
							<input type="text" aria-required="true" name="firstname" id="firstname" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>

						<div class="form-group col-md-6">
							<label class="text-dark" for="lastname">Last Name</label>
							<input type="text" name="lastname" id="lastname" class="form-control">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="text-dark" for="mobile">Mobile</label>
							<input type="text" aria-required="true" name="mobile" id="mobile" class="form-control" required maxlength="10" data-validation-regex-regex="^[6789]\d{9}$">
							<div class="help-block font-small-3"></div>
						</div>

						<div class="form-group col-md-6">
							<label class="text-dark" for="emailid">Email Id</label>
							<input type="text" aria-required="true" name="emailid" id="emailid" class="form-control" required>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="text-dark" for="city">City</label>
							<input type="text" aria-required="true" name="city" id="city" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>

						<div class="form-group col-md-6">
							<label class="text-dark" for="qualifications">Qualifications</label>
							<input type="text" aria-required="true" name="qualifications" id="qualifications" class="form-control" required>
							<div class="help-block font-small-3"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="text-dark" for="experience">Experience</label>
						<textarea aria-required="true" name="experience" id="experience" class="form-control" required></textarea>
						<div class="help-block font-small-3"></div>
					</div>

					<div class="form-group">
						<label class="text-dark" for="keyskills">Key Skills</label>
						<textarea aria-required="true" name="keyskills" id="keyskills" class="form-control" required></textarea>
						<div class="help-block font-small-3"></div>
					</div>

					<div class="form-group">
						<label class="text-dark" for="resume">Upload Resume</label>
						<input type="file" aria-required="true" name="resume" id="resume" class="form-control" required accept=".doc,.docx,.txt,.pdf">
						<div class="help-block font-small-3"></div>
					</div>

					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" aria-required="true" name="conditions" id="conditions" class="custom-control-input" value="1" required>
							<label class="custom-control-label" for="conditions"><small>By proceeding, you agree to the <a href="../../terms-conditions.html" target="_blank">Terms of Use</a>, <a href="../../privacy-policy.html" target="_blank">Privacy Policy</a> and this consent will override any registration by me for DNC / NDNC.</small></label>
							<div class="help-block font-small-3"></div>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" id="submit-btn2" class="btn btn-secondary">APPLY NOW</button>
					</div>

					<div class="form-group" id="applymessage"></div>
				</form>			</div>

			<div class="col-lg-5 col-md-5 col-12 pt-4">
				<p><strong>We&rsquo;re Hiring | Telecaller | Adajan Surat</strong></p>

<p>&nbsp;</p>

<p>✅<strong> Telecaller Eligibility:</strong></p>

<ul>
	<li>Clear Speech</li>
	<li>Good Communication Skills</li>
	<li>Minimum Qualification &ndash; 10th Pass</li>
	<li>Minimum Experience &ndash; Freshers Can Apply</li>
</ul>

<p>&nbsp;</p>

<p>✅ <strong>Job Role:</strong></p>

<ul>
	<li>Calling Customers and Informing them about the Company&#39;s Products/Services.</li>
	<li>Receive Calls and Solve Queries.</li>
</ul>

<p>&nbsp;</p>

<p>✅ <strong>Note:</strong></p>

<ul>
	<li>Final Selection and Incentive depend on the candidate&rsquo;s skill &ndash; judged by the company once the interview is done.</li>
	<li>Job Timing - 09:30 AM To 06:30 PM (Monday to Saturday).</li>
</ul>

<p>&nbsp;</p>

<p><strong>✅ Company Location:</strong></p>

<ul>
	<li>4001, Marvella Business Hub, Annapurna Temple, Adajan Gam, Adajan, Surat, Gujarat. 395009</li>
</ul>

<p>&nbsp;</p>

<p>✅<strong> Interview Time:</strong></p>

<ul>
	<li>10:00 AM To 10:30 AM&nbsp;&amp;&nbsp;5:00 PM&nbsp;to 5:30 PM&nbsp; (Monday To Saturday).</li>
</ul>
			</div>

		</div>
	</div>
</section>

<script type="text/javascript">
  $(function(){
      $('#submitSubscribeForm').on('submit', function(e) {
          e.preventDefault();

          $.ajax({
              url : $(this).attr('action'),
              type: "POST",
              data: $(this).serialize(),
              dataType: "JSON",
              cache: false,
              processData: false,
              beforeSend: function(){
                  $('#form-submit-subscribe').html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>");
				          $('#form-submit-subscribe').attr('disabled', true);
              },
              success: function (response) {
                if(response['success'] == true) {
                    document.getElementById("submitSubscribeForm").reset();
                    $.notify({ message: response['message'] },{ type: 'success' });
                } else {
                  $.notify({ message: response['message'] },{ type: 'danger' });
                }
                $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                $('#form-submit-subscribe').attr('disabled', false);
              },
              error: function (jXHR, textStatus, errorThrown) {
                $.notify({ message: errorThrown },{ type: 'danger' });
                $('#form-submit-subscribe').html('<i class="fa fa-paper-plane"></i>');
                $('#form-submit-subscribe').attr('disabled', false);
              }
          });
      });
  });
</script>
<script type="text/javascript">
  $(function(){
      $('#submitForm2').on('submit', function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);

            $.ajax({
               	url : $(this).attr('action'),
                type: "POST",
                data: formData,
                async: true,
                dataType: "JSON",
                cache: false,
                contentType: false,
        		processData: false,
                beforeSend: function(){
                    $('#submit-btn2').html('Document uploading... <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
					$('#submit-btn2').attr('disabled', true);
                },
                success: function (response) {
                  if(response['success'] == true) {
                      document.getElementById("submitForm2").reset();
                  }

                  document.getElementById("applymessage").innerHTML = response['message'];
				  $.notify({ message: response['message'] },{ type: 'success' });
                  $('#submit-btn2').html('APPLY NOW');
				  $('#submit-btn2').attr('disabled', false);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    $('#submit-btn2').html('APPLY NOW');
					$('#submit-btn2').attr('disabled', false);
                    $.notify({ message: errorThrown },{ type: 'danger' });
                }
            });
        });
  });
</script>
@endsection
