@extends('layouts.master')


@section('main')


<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
	<div class="slide background-gradient-1">
		<div class="container">
			<div class="slide-captions row">
				<div class="col-lg-6 col-md-6 col-12 align-self-center">
					<h1 class="text-medium">Your Quickest Way To Get Instant Personal Loan Online</h1>
					<p>Get personal loan offers from multiple banks and NBFCs within a few minutes - that too with an easy and rewarding membership.<br/>
					Fulfil all your financial dreams and requirements with the super-smooth and instant process at moneyupfinance.</p>

					<a href="{{ url('digital/personal-loan') }}" class="btn btn-dark btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply Now</span><i class="fa fa-arrow-right"></i></a>
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<img src="{{ asset('public/assets/images/slider/pl-offer-img.png') }}" alt="digital personal loan" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>

<section class="background-grey">
	<div class="container text-center m-b-20">
		<div class="heading-text heading-line text-center">
			<h3 class="text-medium font-weight-500">A Fantastic Range of Personal Loans!</h3>
		</div>
		<p>Whatever your personal loan requirement is, we've always got the best option for you. From varied options of loan tenure, you can opt for the most convenient one and materialize your dreams to garnish your life.</p>
	</div>

	<div class="container-fluid">
		<div class="row pricing-table">
			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">1<span>Year</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 15,000 / month</li>
							<li>Min. Loan: ₹ 25,000/-</li>
							<li>Max. Loan: ₹ 1,10,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">2<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 22,000 / month</li>
							<li>Min. Loan: ₹ 25,000/-</li>
							<li>Max. Loan: ₹ 4,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">3<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 27,000 / month</li>
							<li>Min. Loan: ₹ 25,000/-</li>
							<li>Max. Loan: ₹ 7,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">4<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 32,000 / month</li>
							<li>Min. Loan: ₹ 25,000/-</li>
							<li>Max. Loan: ₹ 12,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="plan">
					<div class="plan-header">
						<div class="plan-price p-b-0">5<span>Years</span></div>
					</div>
					<div class="plan-list">
						<ul>
							<li>Min. Salary Required: ₹ 40,000 / month</li>
							<li>Min. Loan: ₹ 25,000/-</li>
							<li>Max. Loan: ₹ 15,00,000/-</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-12">
				<div class="heading-text heading-line p-b-5">
					<h2 class="text-medium font-weight-500">A Quick Personal Loan for All Purposes!</h2>
				</div>

				<p>You may need some urgent money to hit the path of your dream vacation, or re-colour your faded house walls, or even pay some emergency medical bills. Whatever the reason for the need for urgent money is, you're just some minutes away from availing your Instant Personal Loan through moneyupfinance.</p>

				<p>A Personal Loan can be used to meet financial requirements for:</p>

				<ul class="list-icon list-icon-caret text-dark">
					<li>Marriage Purposes</li>
					<li>Medical Emergencies</li>
					<li>Vacation Trips</li>
					<li>Rent Deposits</li>
					<li>Buying Vehicles</li>
					<li>Buying Essential Gadgets</li>
				</ul>
			</div>

			<div class="col-lg-5 col-md-5 col-12 text-center">
				<img src="{{ asset('public/assets/images/slider/info-banner-4.png') }}" alt="personal loan purposes" class="img-fluid">
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center p-b-5">
			<h3 class="text-medium font-weight-500">Premium Membership Card for Unlimited Benefits</h3>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12">
				<p>Once you get moneyupfinance's Premium Membership Card, there's no looking back! You can get personal loan offers from multiple banks - all at the comfort of your home. Just make some clicks and get instant loan offers depending upon your eligibility. It should be noted that the Premium Membership Card is not any sort of credit or debit card and the customer should not be of the impression that buying this card means getting money in the bank. Membership Card is limited to our company only, providing certain benefits. Buying a Premium Membership Card has a wide range of benefits that can help the loan seeker in easing the entire process of applying for a personal loan. Let's look at the specific perks of the membership card. You can avail of your pre-approval loan offer from multiple banks through just a single platform - moneyupfinance. The company provides its customers with a decade-long free consultancy - assisting at every stage of the loan process. With the membership card, you can even earn a payout of up to 40% through referrals.</p>
			</div>

			<div class="col-lg-6 col-md-6 col-12">
				<div>
					<h4><i class="fa fa-certificate"></i> Get Pre-Approved Loan Offer</h4>
					<p>You can get loan offers from multiple banks and NBFCs - allowing you to choose the best loan offer as per your requirement and convenience.</p>
				</div>
				<div class="line"></div>
				<div>
					<h4><i class="fa fa-calendar-alt"></i> 10 Years Free Consultancy</h4>
					<p>This is a very helpful feature of the Premium Membership Card that assists you in getting your personal loan offer for a span of 10 years.</p>
				</div>
				<div class="line"></div>
				<div>
					<h4><i class="fa fa-percent"></i> Refer & Earn up to 40%</h4>
					<p>You can even make good money through referrals. This means that in addition to getting loan offers from multiple banks, you can earn some extra money!</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 text-center">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Avail Quick & Instant Personal Loan from moneyupfinance</h4>
				</div>

				<p>moneyupfinance is completely dedicated to providing all its customers with wholehearted assistance and helping them with their personal loan requirements. For the same purpose, we have a streamlined and transparent process of giving you personal loan offers from multiple banks and NBFCs. This facility provided by moneyupfinance will let you get a personal loan depending upon your eligibility.</p>
				<p>Just after a very quick registration and once you get the Premium Membership Card, the moneyupfinance team will submit your loan file to multiple banks and the loan offers will be sent to you from the banks under which your eligibility matches.</p>

				<a href="{{ url('digital/personal-loan') }}" class="btn btn-primary btn-rounded btn-reveal btn-reveal-right"><span>Check Eligibility for Personal Loan</span><i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Our NBFC Bank Partners</h4>
				</div>
				<p>A Spectrum of Solid Collaborations for Best Services!</p>

				<ul class="grid grid-6-columns">
				<li class="p-20"><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/026.png') }}" alt="Bajaj Finserv"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/028.png') }}" alt="Ziploan"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/030.png') }}" alt="Hero Fincorp"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/031.png') }}" alt="Monexo"></li>				</ul>

			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">How to apply for a Personal Loan at moneyupfinance?</h4>
		</div>

		<div class="row p-t-40">
			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>1</h2></div>
					<h3>Quick Registration</h3>
					<p>The first and foremost step remains the filling of a very simple form to be filled by the personal loan seeker. The process of filling this registration form can hardly take 5 minutes.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>2</h2></div>
					<h3>Check Eligibility</h3>
					<p>You will be able to check for your eligibility for the personal loan - just after the quick registration process.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>3</h2></div>
					<h3>Buy Membership Card</h3>
					<p>This is the best feature of moneyupfinance through which you can get your Premium Membership Card and receive loan offers from multiple banks.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>4</h2></div>
					<h3>Submit Documents</h3>
					<p>The next step would be the submission of your documents - digital copy and hard copy - which would be used for further loan processing.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>5</h2></div>
					<h3>Bank Verification</h3>
					<p>In this, the bank will evaluate your application and come up with a fair sanction - depending upon the submitted documents.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box light small border">
					<div class="icon text-center"><h2><i></i>6</h2></div>
					<h3>Bank Sanction</h3>
					<p>The final step is receiving the sanctioned money into your account after the bank successfully approves your loan application.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="accordion accordion-simple">
					<div class="ac-item">
						<h5 class="ac-title">What is a Personal Loan?</h5>
						<div class="ac-content">
							<p>A personal loan is an official lent money to an individual by banks or NBFCs (private financial firms) after the person's loan file is approved by the concerned firm. A personal loan falls under the category of unsecured credit - meaning that the loan is provided against no collaterals. As personal loans need no security, the rate of interest is comparatively higher than the loans that include collateral.</p>
							<p>An individual can opt for a personal loan in case of any financial urgency like - a medical emergency, unplanned vacation trips, urgent rental deposits, domestic needs, buying a vehicle, or any other. A personal loan can easily be taken by salaried people by submitting their income proof and documents of professional stability.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Advantages of a Personal Loan</h5>
						<div class="ac-content">
							<p>Urgent money can be needed at any stage of life, at any given instance of life. And everyone cannot be financially sound to fulfil that urgent need of money. Here, the best option is to get a personal loan because it can be accessed in less time - only after the loan file is approved by the bank or financial company. Once the loan is taken by the person, he/she can repay the loan through monthly EMIs (Equated Monthly Instalments). The EMI is dependent upon the loan amount and the decided tenure.</p>
							<p>Also, taking a personal loan is very advantageous when it comes to meeting urgent financial challenges. Whenever a person is short of money and has some financial commitments, he/she can easily avail of a personal loan (if the loan application is approved) and fulfil the monetary requirements.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Personal loan eligibility criteria for Salaried Persons</h5>
						<div class="ac-content">
							<p>If you are a salaried person and if you seek a personal loan, there are some standard eligibility criteria that are to be met:</p>
							<ul>
								<li>Minimum Age - 21 Years</li>
								<li>Minimum Salary - Rs. 15,000 / month</li>
								<li>1 Year Job Stability</li>
							</ul>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Personal loan eligibility criteria for Self-Employed Persons</h5>
						<div class="ac-content">
							<p>There are some specified criteria for the loan seekers who are self-employed. Let's have a look at them:</p>
							<ul>
								<li>Minimum Age - 21 Years</li>
								<li>Income Tax Return of Minimum 1 Year</li>
								<li>1 Year Business Stability</li>
							</ul>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">Convenient Loan Tenure</h5>
						<div class="ac-content">
							<p>moneyupfinance understands that a personal loan is a thing that can't embrace a stringent one-fits-all approach. And for the same reason, a loan seeker can find an excellent range of loan tenure options to choose from. Depending upon your convenience, you can opt for a loan tenure of 1 year, 2 years, 3 years, 4 years, and 5 years. It must be noted that for opting for a particular loan, the loan seeker's loan file shall meet all the requirements and criteria of the concerned bank or NBFC. Some of the factors that decide your loan approval are income proof, professional stability, loan amount, CIBIL score, etc.</p>
						</div>
					</div>

					<div class="ac-item">
						<h5 class="ac-title">No Requirement of Collateral</h5>
						<div class="ac-content">
							<p>As a personal loan is categorized as unsecured credit, no bank would ask for any collateral security against the personal loan. This is a great feature for the personal loan seekers as they have to give no guarantee; having said that, the documents presented during the loan process are considered as the guarantee for loan repayment.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h4 class="text-medium font-weight-500">Why moneyupfinance.com?</h4>
		</div>

		<p class="text-center">Being India's #1 Digital Loan Provider, moneyupfinance is partnered with multiple banks and NBFCs to provide you with the best personal loan that suits you and fulfils your financial goals or immediate monetary needs. With some amazing features and provisions, moneyupfinance is a one-stop digital solution for catering to your personal loan needs.</p>

		<div class="row p-t-20">
			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-users"></i></div>
					<h5>1000+ Happy Customers Monthly</h5>
					<p>Our humongous customer base is our pride, who are super-satisfied with us and continue our association.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-file-alt"></i></div>
					<h5>Minimum Documents Required</h5>
					<p>Depending upon the customer profile, the concerned documents can be easily submitted.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-university"></i></div>
					<h5>Tie-up with Multiple Banks & NBFCs</h5>
					<p>A personal loan offer of up to INR 15,00,000 (depending upon customer profile) can be offered from multiple banks.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-percent"></i></div>
					<h5>Good Range of Annual Percentage Rate</h5>
					<p>The range of annual percentage rates, starting from 12.5% to 24%, enables the customer to opt for the most convenient loan offer.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-calendar"></i></div>
					<h5>Flexible Repayment Terms</h5>
					<p>Depending upon the loan offer, the customer is facilitated with flexible repayment terms with loan tenures ranging from 1 Year to 5 Years.</p>
				</div>
			</div>

			<div class="col-lg-4 col-sm-12">
				<div class="icon-box effect small m-b-20">
					<div class="icon"><i class="fa fa-hourglass-half"></i></div>
					<h5>Provision of Premium Membership Card</h5>
					<p>The customer can avail of loan offers from multiple banks through a 100% online loan process. Also, 10 years of free consultancy is a great plus.</p>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection
