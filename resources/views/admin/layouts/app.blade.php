<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Admin | @yield('title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/adminassets/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('public/adminassets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/adminassets/dist/css/adminlte.min.css')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/assets/logo/logo-f.png') }}">
  <link rel="icon" type="image/x-icon" href="{{ asset('public/assets/logo/logo-f.png') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
          <a href="{{url('/')}}" class="h1" style="font-weight: 200"><b>{{env('APP_NAME')}}</b></a>
        </div>
  <!-- /.login-logo -->
  @yield('content')
  </div
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('public/adminassets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('public/adminassets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminassets/dist/js/adminlte.min.js')}}"></script>

@yield('script')
</body>
</html>