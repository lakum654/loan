<nav class="main-header navbar navbar-expand navbar-dark">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


    <ul class="navbar-nav ml-auto">

	  <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
		  <span class="fa fa-angle-down"></span> {{ auth()->user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

          <div class="dropdown-divider"></div>
          <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="dropdown-item">
            Logout <i class="fa fa-times float-right"></i>
          </a>
		    <form id="logout-form" action="{{ route('admin.logout') }}"
                method="POST" style="display: none;">
          		@csrf
        	</form>
			    {{-- <a href="{{ url('admin/changePassword') }}" class="dropdown-item">
            Change Password <i class="fa fa-key float-right"></i> 
          </a> --}}

        </div>
      </li>
    </ul>
  </nav>
