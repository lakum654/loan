<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('public/adminassets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{ asset('public/adminassets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('public/adminassets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/adminassets/dist/js/adminlte.js')}}"></script>

<!-- Select2 -->
<script src="{{ asset('public/adminassets/plugins/select2/js/select2.min.js')}}"></script>
<!-- Datatables -->
<script src="{{ asset('public/adminassets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/adminassets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- Jquery Validation -->
<script src="{{ asset('public/adminassets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('public/adminassets/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<!-- sweetalert -->
<script src="{{ asset('public/adminassets/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>

<!-- moment -->
<script src="{{ asset('public/adminassets/plugins/moment/moment.min.js')}}"></script>
<!-- datepicker -->
<script src="{{ asset('public/adminassets/plugins/datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
  $(document).ready(function() {

    @if(Session::has('message'))
    Swal.fire(
      '{{ $moduleName }}',
      '{!! session('message') !!}',
      'success'
    );
    @elseif(Session::has('failmessage'))
    Swal.fire(
      '{{ $moduleName }}',
      '{!! session('failmessage ') !!}',
      'error'
    );
    @endif

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4',
      placeholder: 'Select',
      allowClear: true
    });

    $('.date').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });
  });
</script>