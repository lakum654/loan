 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="{{ route('admin.dashboard') }}" class="brand-link">
    <img src="{{ asset('public/assets/logo/logo-f.png')}}" alt="AdminLTE Logo" class="brand-image elevation-3 w-50">
  </a>

   <!-- Sidebar -->
   <div class="sidebar">
     <!-- Sidebar Menu -->
     <nav class="mt-2">
       <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <li class="nav-item">
           <a href="{{route('admin.dashboard')}}" class="nav-link {{ (\Request::segment(2) == '') ? 'active' : '' }}">
             <i class="nav-icon fas fa-tachometer-alt"></i>
             <p>Dashboard</p>
           </a>
         </li>


         <li class="nav-item">
            <a href="{{route('users')}}" class="nav-link {{ (\Request::segment(2) == 'users') ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>App Users</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('applications')}}" class="nav-link {{ (\Request::segment(2) == 'applications') ? 'active' : '' }}">
              <i class="nav-icon fa fa-book"></i>
              <p>Loan Applications</p>
            </a>
          </li>
       </ul>
     </nav>
     <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
 </aside>
