@extends('admin.admin.layouts.master')
@section('title')
    {{ $moduleName ?? '' }}
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $moduleName ?? '' }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $moduleName ?? '' }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $moduleName ?? '' }} Details</h3>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Document Name</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @if ($document != null)
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Profile Photo</td>
                                        <td><img src="{{ $document->profile_photo }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->profile_photo }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->profile_photo }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Aadhar Card</td>
                                        <td><img src="{{ $document->aadhar_card }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->aadhar_card }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->aadhar_card }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Pan Card</td>
                                        <td><img src="{{ $document->pan_card }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->pan_card }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->pan_card }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Cancel Cheque</td>
                                        <td><img src="{{ $document->cancel_cheque }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->cancel_cheque }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->cancel_cheque }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Address Proof</td>
                                        <td><img src="{{ $document->address_proof }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->address_proof }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->address_proof }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Form_16</td>
                                        <td><img src="{{ $document->form_16 }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->form_16 }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->form_16 }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Bank Statement</td>
                                        <td><img src="{{ $document->bank_statement }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->bank_statement }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->bank_statement }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Salary Slip</td>
                                        <td><img src="{{ $document->salary_slip }}" alt="" width="100px"></td>
                                        <td>
                                            <a href="{{ $document->salary_slip }}" class="btn btn-success" target="_blank">View</a>
                                            <a href="{{ $document->salary_slip }}" class="btn btn-danger" target="_blank" download>Download</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @elseif($document == null)
                                <h2 class="card-title">No Document Uploaded</h2>
                            @endif

                        </div>

                    </div>
                </div>
                <!--/. container-fluid -->
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection


@section('script')
    <script>

    </script>
@endsection
