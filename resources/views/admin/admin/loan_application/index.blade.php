@extends('admin.admin.layouts.master')
@section('title')
    {{ $moduleName ?? '' }}
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $moduleName ?? '' }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $moduleName ?? '' }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $moduleName ?? '' }} Details</h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive table-sm">
                            <table id="datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>User Name</th>
                                        <th>Cibil Score</th>
                                        <th>Montly Income</th>
                                        <th>Current Monthly EMI</th>
                                        <th>Loan Purpose</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Eligibility Amount</th>
                                        <th>Pre Approval Offer</th>
                                        {{-- <th>EMI</th> --}}
                                        <th>Payment Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <!-- /.card-body -->
                        <div class="card-footer">

                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/. container-fluid -->
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection


@section('script')
    <script>
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{{ route('applications.getData') }}",
                "dataType": "json",
                "type": "GET",
            },
            lengthMenu: [
                [50, 100, 200, -1],
                [50, 100, 200, "All"]
            ],
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'user.name'
                }, {
                    data: 'cibil_score'
                },
                {
                    data: 'monthly_income'
                },
                {
                    data: 'current_monthly_emi'
                },
                {
                    data: 'loan_purpose'
                },
                {
                    data: 'city'
                },
                {
                    data: 'state'
                },
                {
                    data: 'eligibility_amount'
                },
                {
                    data: 'pre_approval_offer'
                },
                // {
                //     data: 'amount_paid'
                // },
                {
                    'data': 'payment_status',
                    'render':function(data,type,row) {
                        if(data == 'pending') {
                            return `<span class="badge bg-warning">${data}</span>`;
                        } else if(data == 'success') {
                            return `<span class="badge bg-success">${data}</span>`;
                        } else if(data == 'failed') {
                            return `<span class="badge bg-danger">${data}</span>`;
                        }
                    }
                },
                {
                    'data': 'action',
                },
            ],
        });

        // $('body').on('click', '.delete', function(e) {
        //     let delId = $(this).data('id');
        //     if (delId != '') {
        //         Swal.fire({
        //             title: 'Are you sure?',
        //             text: "You won't be able to revert this!",
        //             icon: 'warning',
        //             showCancelButton: true,
        //             confirmButtonColor: '#3085d6',
        //             cancelButtonColor: '#d33',
        //             confirmButtonText: 'Yes, delete it!'
        //         }).then((result) => {
        //             if (result.isConfirmed) {
        //                 $.ajax({
        //                     url: "{{ route('users.delete') }}",
        //                     type: "POST",
        //                     data: {
        //                         id: delId
        //                     },
        //                     success: function(response) {
        //                         if (response) {
        //                             location.reload();
        //                         }
        //                     },
        //                 });

        //             }
        //         });
        //     }

        // });

        $(document).on('click', '#activate', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to Activate?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });

        $(document).on('click', '#deactivate', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to De-Activate?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });

        $(document).on('click', '#approve', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to Approve?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });

        $(document).on('click', '#disapprove', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to Dis-Approve?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });
    </script>
@endsection
