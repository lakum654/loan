@extends('layouts.master')


@section('main')

<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
	<div class="slide background-gradient-3">
		<div class="container">
			<div class="slide-captions row">
				<div class="col-lg-6 col-md-6 col-12 align-self-center">
					<h1 class="text-medium">Your Fastest Earning Opportunity - Become Loan DSA/Loan Agent</h1>
					<p>If you're someone who is in search of some smart and quick ways to make money, moneyupfinance's Channel Partner is just meant for you. When you become our Channel Partner, with easy sharing of your referral links to your network of loan seekers, you can generate a very handy amount of part-time income or even a handsome full-time amount.</p>

					<a href="{{ url('partner/channel') }}" class="btn btn-dark btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply Now</span><i class="fa fa-arrow-right"></i></a>
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<img src="{{ asset('public/assets/images/slider/cp-offer-img.png') }}" alt="channel partner" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line text-center">
			<h3 class="text-medium font-weight-500">Why Work For Someone Else? Be Your Own Boss!</h3>
		</div>
		<p>Instead of working under someone else and contributing to someone else's growth, you always wanted to do something for your own, by your own! So, make your smartest decision and join moneyupfinance's Channel Partner Program now for FREE (limited time offer). All you need to do is follow the fast and easy application process.</p>

		<div class="row">
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">01</h3>
				<h4>Quick registration</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">02</h3>
				<h4>Submit application form</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">03</h3>
				<h4>Free partner code</h4>
			</div>
			<div class="col-md-3 text-center m-b-20">
				<h3 class="text-lg text-grey">04</h3>
				<h4>Start business</h4>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-12">
				<div class="heading-text heading-line p-b-5">
					<h2 class="text-medium font-weight-500">Rewarding Benefits of moneyupfinance's Channel Partner Program</h2>
				</div>

				<p>moneyupfinance's Channel Partner is a comprehensive idea of enabling its partners with a remote professional opportunity through which the Channel Partners can refer and earn money at the comfort of their home, without going out anywhere. Joining this Channel Partner will facilitate you with several solid advantages - from earning potential to building your network - this program is a sure-shot stepping stone to your success and financial independence.</p>

				<ul class="list-icon list-icon-caret">
					<li>High Return, Zero Investment</li>
					<li>Up to ₹ 3 Lakhs Earning Chance</li>
					<li>Lifetime Career Opportunity</li>
					<li>Marketing Support of moneyupfinance</li>
				</ul>
			</div>

			<div class="col-lg-5 col-md-5 col-12 text-center">
				<img src="{{ asset('public/assets/images/slider/info-banner-6.png') }}" alt="channel partner" class="img-fluid">
			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line p-b-5 text-center">
			<h3 class="text-medium font-weight-500 text-center">Whatever your background is, Channel Partner Program is for YOU!</h3>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>The best part about moneyupfinance's Channel Partner Program is that it can be joined by people from all the faculties and fields. If you are a student who is looking to earn a good amount of money without going to any office and contributing to their household or want to buy something desirable, they can easily become moneyupfinance's Channel Partner and start earning through easy sharing of referral links. All the Channel Partners are provided with unique referral links that can be shared by them to their network of people and specifically to the loan seekers. If you are a housewife who is mostly consumed with domestic commitments but want to contribute economically to your family, here is the best chance for you to make your mark by becoming moneyupfinance's Channel Partner. In your spare time, you can easily share your unique referral link (provided by moneyupfinance.com) to your known ones and through this, you can efficiently create a stronger network where the loan seekers can be targeted for you to leverage this fantastic opportunity of generating a potential income.</p>
			</div>

			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>And the best part is, you can start this one-of-its-kind business at your home - without going out anywhere. If you are a salaried professional or a business owner and want to generate a side-income or even a full-time income - moneyupfinance's Channel Partner Program is the ideal choice for you. By becoming moneyupfinance's Loan Agent, you can make your earning quotient limitless as the volume of loan seekers is very hefty, all you need to do is share your unique referral link with them. For every purchase of the membership card done through your referral link, you will receive a payout (commission) of up to 60%. This is a very astounding business opportunity for the ones who seek to start something of their own with zero investment. Joining the Channel Partner Program is very easy - you can complete the entire process in a matter of just a few minutes. Start your business right now by becoming moneyupfinance's Loan DSA/ Loan Agent.</p>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="heading-text heading-line text-center">
					<h4 class="text-medium font-weight-500">Our NBFC Bank Partners</h4>
				</div>
				<p>We Satisfy Your Business Loan Needs - Backed by the Best Lending Firms!</p>

				<ul class="grid grid-6-columns">
				<li class="p-20"><img src="{{ asset('public/assets/images/bank/006.png') }}" alt="TATA Capital"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/015.png') }}" alt="Faircent.com"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/016.png') }}" alt="Fullertor India"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/023.png') }}" alt="Lendingkart"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/021.png') }}" alt="Indifi"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/022.png') }}" alt="Money View"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/024.png') }}" alt="Moneytap"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/025.png') }}" alt="IDFC First Bank"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/026.png') }}" alt="Bajaj Finserv"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/028.png') }}" alt="Ziploan"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/030.png') }}" alt="Hero Fincorp"></li><li class="p-20"><img src="{{ asset('public/assets/images/bank/031.png') }}" alt="Monexo"></li>				</ul>

			</div>
		</div>
	</div>
</section>


<section class="background-grey">
	<div class="container">
		<div class="heading-text heading-line p-b-5 text-center">
			<h3 class="text-medium font-weight-500 text-center">Make The Most of moneyupfinance's Free Marketing Support for Channel Partners</h3>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>When you become a Channel Partner/Loan DSA/Loan Agent with moneyupfinance.com, you are facilitated with numerous provisions that will help you boost your network of people and eventually, grow your business. As social media has emerged as the most beneficial source of marketing through which a mass audience can be targeted for respective services and increase client conversion, moneyupfinance leverages all the social platforms like Facebook, Instagram, LinkedIn, etc., and has a commendable amount of followers who provide good engagement. With the Channel Partner Program, you will be able to leverage this mammoth follower base of moneyupfinance and boost your prospects of earning a handsome income through easy refer and earn facilitation.</p>
			</div>

			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>Considering the high standards of professionalism practised by moneyupfinance pertaining to the Channel Partner Program, we understand the need for effective marketing for our Channel Partners to function in the most efficient manner. We strongly believe in creating a win-win situation for our Loan DSAs/Loan Agents and the Channel Partner Program is just the classic instance of this thought - because when you grow your network and start generating loan files for submission to the company, we take over and lead the customers to their final desirable service - which is the fetching Personal and Business Loan Offers from multiple banks and NBFCs (Non-Banking Financial Companies).</p>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="heading-text heading-line p-b-5 text-center">
			<h3 class="text-medium font-weight-500 text-center">Take Pride in Becoming a Part of India's #1 Loan Providing Company - moneyupfinance.com</h3>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>moneyupfinance has helped and served 37000+ customers and the number seems to be ever-increasing! This humongous customer base is the evident byproduct of the company's excellent services, customer satisfaction, and professional attributes. By becoming a Loan DSA/Loan Agent with moneyupfinance, you will be able to add a golden feather in the hat of your professional pursuits. moneyupfinance proudly boasts of being India's most reliable, trustworthy, and value-for-money service provider that enables its customers to avail Personal and Business Loan Offers from multiple partnered banks and NBFCs - facilitating its clients to choose the most convenient loan offers to meet their financial goals, requirements, and plans.</p>
			</div>

			<div class="col-lg-6 col-md-6 col-12 text-justify">
				<p>As a Channel Partner for moneyupfinance, you can upscale your earning potential through easy sharing of your unique link (provided by the company once you complete the entire process of registration) and when your referred customers will purchase our Premium Membership Card for a personal loan or the Platinum Membership Card for a business loan, you can earn a payout commission of up to 60%.</p>
			</div>
		</div>

		<div class="p-t-20 text-center">
			<h3>Let your earnings soar high with moneyupfinance's Channel Partner Program </h3>
			<a href="{{ url('partner/channel') }}" class="btn btn-outline btn-rounded btn-reveal btn-reveal-right"><span>Apply Now</span><i class="fa fa-arrow-right"></i></a>
		</div>
	</div>
</section>



@endsection
