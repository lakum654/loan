@extends('layouts.master')


@section('main')
    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Return &amp; Refund Policy</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('refund-policy') }}">
                            <span itemprop="name">Return &amp; Refund Policy</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section id="section-privacy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <p><span style="font-size:16px">Thank you for giving us the opportunity to serve you!</span></p>

                            <p>&nbsp;</p>

                            <p><strong>Is the Membership Card fees refundable?</strong></p>

                            <p>●&nbsp;&nbsp; &nbsp;Membership Fees are not refundable under any circumstances. And the same
                                clause had been approved by you by agreeing to our Privacy Policy and Terms and Conditions.
                            </p>

                            <p><strong>Why is the Membership Card charge is non-refundable?</strong></p>

                            <p>●&nbsp;&nbsp; &nbsp;When you buy a Membership Card, the company has to pay some
                                non-refundable amounts like marketing and promotional charges, other various charges,
                                etc.<br />
                                ●&nbsp;&nbsp; &nbsp;Upon buying a Membership Card, the company gives you some facilities
                                &amp; services; and if you have any issue regarding the loan process, we will try our best
                                to solve your issue in the most satisfactory manner. We are always there to serve you and
                                provide the best experience!</p>

                            <p><strong>Contact Us</strong></p>

                            <p>If you have any questions about our Returns and Refunds Policy, please contact us:</p>

                            <p>●&nbsp;&nbsp; &nbsp;By email: info@moneyupfinance.com<br />
                                ●&nbsp;&nbsp; &nbsp;By call: +91 97237-97077</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
