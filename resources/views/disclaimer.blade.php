@extends('layouts.master')

@section('main')
    <section class="p-t-130 p-b-100" id="page-title"
        data-bg-parallax="https://nowofloan.com/assets/images/slider/header-bg-105.jpg">
        <div class="container">
            <div class="page-title">
                <h1>Disclaimer</h1>
            </div>
            <div class="breadcrumb">
                <ul itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('') }}">
                            <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ url('disclaimer') }}">
                            <span itemprop="name">Disclaimer</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section id="section-privacy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <p>moneyupfinance Service India Pvt. Ltd. and its customers, employees, channel partners, associate
                                partners use and present www.moneyupfinance.com (the &ldquo;Website&rdquo;) for personal and
                                informational purposes only. We further expressly disclaim any warranties or representations
                                (expressed or implied) in respect of quality, suitability, accuracy, reliability,
                                completeness, timeliness, performance for a particular purpose or legality of the services
                                listed or displayed or transacted or the content on the website. You should not construe any
                                such information or other material as legal, tax, investment, financial, or other advice.
                                You acknowledge and undertake that you are accessing the services on the moneyupfinance website
                                and transacting at your own risk and are using your best and prudent judgement before
                                entering into any transactions through the website. You alone assume the sole responsibility
                                of evaluating the merits and risks associated with the use of any information or other
                                Content contained on the moneyupfinance Website before making any decisions based on such
                                information or other Content.</p>

                            <p>Nothing contained on our Website constitutes a solicitation, recommendation, endorsement, or
                                offer by moneyupfinance to buy or sell any securities or other financial instruments in this or
                                in any other jurisdiction in which such solicitation or offer would be unlawful under the
                                securities laws of such jurisdiction. You further acknowledge that at no time shall any
                                right, title or interest in the services sold through or displayed on the website vest with
                                moneyupfinance nor shall moneyupfinance have any obligations or liabilities in respect of any
                                transactions on the website.</p>

                            <p>After you enter your details on our website for any purpose, the company takes no
                                responsibility in case you come across instances of data misusage of any form.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
